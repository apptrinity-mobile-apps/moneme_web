import 'package:mone_me/models/add_amount_to_wallet_model.dart';
import 'package:mone_me/models/add_bank_account_model.dart';
import 'package:mone_me/models/add_card_model.dart';
import 'package:mone_me/models/add_transaction_model.dart';
import 'package:mone_me/models/forgot_secure_pin_model.dart';
import 'package:mone_me/models/update_profile_model.dart';
import 'package:mone_me/models/user_login_model.dart';
import 'package:mone_me/models/user_sign_up_model.dart';
import 'package:mone_me/models/wallet_to_bank_model.dart';

abstract class BaseService {
  // staging
  // final String baseUrl = "http://3.16.193.190/api/frontend";

  // final String baseUrl = "https://moneeme.com/api/frontend";
  final String baseUrl = "https://app.moneeme.com/api/frontend";

  // live
  // final String baseUrl = "";

  Future<dynamic> getAllCountries();

  Future<dynamic> getAllLanguages();

  // login & signup
  Future<dynamic> verifyMobileNumber(String phoneNumber, String email);

  Future<dynamic> createUser(UserSignUpModel signUpModel);

  Future<dynamic> updateProfile(UpdateProfileModel profileModel);

  Future<dynamic> login(UserLoginModel loginModel);

  Future<dynamic> logout(String userId);

  Future<dynamic> passwordReset(String phoneCode, String phoneNumber, String email);

  Future<dynamic> forgotPasswordReset(
      String phoneCode, String phoneNumber, String email, String forgotPasswordOTP, String newPassword, String confirmNewPassword);

  // user data
  Future<dynamic> viewDashboard(String userId, String timezone);

  Future<dynamic> getAllBanks(String userId);

  Future<dynamic> addBankAccount(AddBankAccountModel model);

  Future<dynamic> deleteBankAccount(String userId, String bankAccountId);

  Future<dynamic> updateCustomerStripe(String userId, String stripeId);

  Future<dynamic> viewCustomerStripe(String userId);

  // transactions
  Future<dynamic> geAllTransactions(String userId, String startTime, String endTime);

  Future<dynamic> getAllTransferredTransactions(String userId, String startTime, String endTime);

  Future<dynamic> getAllReceivedTransactions(String userId, String startTime, String endTime);

  Future<dynamic> getAllRequestsTransactions(String userId, String startTime, String endTime);

  Future<dynamic> walletToBank(WalletToBankModel walletToBankModel);

  Future<dynamic> getAllFriends(String userId);

  // user to user transactions
  Future<dynamic> getFriendRecentTransactions(String userId, String friendId);

  Future<dynamic> blockUser(String userId, String blockUserId);

  Future<dynamic> unBlockUser(String userId, String blockUserId);

  Future<dynamic> getSavedCards(String userId);

  Future<dynamic> addCard(AddCardModel cardModel);

  Future<dynamic> addAmountToWallet(AddAmountToWalletModel amountToWalletModel);

  Future<dynamic> getSingleTransaction(String userId, String transactionId);

  Future<dynamic> searchUserByPhone(String userId, String searchNumber);

  Future<dynamic> createSecurePin(String userId, String pin);

  Future<dynamic> resetSecurePin(String email);

  Future<dynamic> forgotSecurePin(ForgotSecurePinModel forgotSecurePinModel);

  Future<dynamic> addTransaction(AddTransactionModel addTransactionModel);

  Future<dynamic> getSocialMediaLinks();

  Future<dynamic> getTransactionFees();
}
