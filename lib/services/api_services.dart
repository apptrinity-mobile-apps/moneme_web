import 'dart:convert';
import 'dart:io';
import 'package:mone_me/models/add_amount_to_wallet_model.dart';
import 'package:mone_me/models/add_bank_account_model.dart';
import 'package:mone_me/models/add_card_model.dart';
import 'package:mone_me/models/add_transaction_model.dart';
import 'package:mone_me/models/forgot_secure_pin_model.dart';
import 'package:mone_me/models/update_profile_model.dart';
import 'package:mone_me/models/user_login_model.dart';
import 'package:mone_me/models/user_sign_up_model.dart';
import 'package:mone_me/models/wallet_to_bank_model.dart';
import 'package:mone_me/services/app_exceptions.dart';
import 'package:mone_me/services/base_services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ApiServices extends BaseService {
  @visibleForTesting
  dynamic returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        dynamic responseJson = jsonDecode(response.body);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
        throw UnauthorisedException(response.body.toString());
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
        throw InternalServerException(response.body.toString());
      default:
        throw FetchDataException('Error occurred while communication with server with status code : ${response.statusCode}');
    }
  }

  @override
  Future getAllCountries() async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_countries");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getAllLanguages() async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_languages");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  // login & signup
  @override
  Future verifyMobileNumber(String phoneNumber, String email) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/mobile_otp");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"phoneNumber": phoneNumber, "email": email}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future createUser(UserSignUpModel signUpModel) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/createUser");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: jsonEncode(signUpModel));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateProfile(UpdateProfileModel profileModel) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/user_update");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: jsonEncode(profileModel));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future login(UserLoginModel loginModel) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/user_login");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: jsonEncode(loginModel));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future logout(String userId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/user_logout");
    var body = json.encode({
      "userId": userId,
    });
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future passwordReset(String phoneCode, String phoneNumber, String email) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/user_password_reset");
    var body = json.encode({
      "phoneCode": phoneCode,
      "phoneNumber": phoneNumber,
      "email": email,
    });
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future forgotPasswordReset(
      String phoneCode, String phoneNumber, String email, String forgotPasswordOTP, String newPassword, String confirmNewPassword) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/user_forgot_password");
    var body = json.encode({
      "phoneCode": phoneCode,
      "phoneNumber": phoneNumber,
      "email": email,
      "forgotPasswordOTP": forgotPasswordOTP,
      "newPassword": newPassword,
      "confirmNewPassword": confirmNewPassword,
    });
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  // user data
  @override
  Future viewDashboard(String userId, String timezone) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_dashboard");
    var body = json.encode({"userId": userId, "timezone": timezone});
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getAllBanks(String userId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_user_bank_accounts");
    var body = json.encode({"userId": userId});
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future addBankAccount(AddBankAccountModel model) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_user_bank_account");
    var body = jsonEncode(model);
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deleteBankAccount(String userId, String bankAccountId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_user_bank_account");
    var body = json.encode({"userId": userId, "bankAccountId": bankAccountId});
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewCustomerStripe(String userId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_customer_stripe");
    var body = json.encode({"userId": userId});
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateCustomerStripe(String userId, String stripeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_customer_stripe");
    var body = json.encode({"userId": userId, "stripeId":stripeId});
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  // transactions
  @override
  Future geAllTransactions(String userId, String startTime, String endTime) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_transactions");
    var body = json.encode({"userId": userId, "startTime": startTime, "endTime": endTime});
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getAllTransferredTransactions(String userId, String startTime, String endTime) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_transfered_transactions");
    var body = json.encode({"userId": userId, "startTime": startTime, "endTime": endTime});
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getAllReceivedTransactions(String userId, String startTime, String endTime) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_received_transactions");
    var body = json.encode({"userId": userId, "startTime": startTime, "endTime": endTime});
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getAllRequestsTransactions(String userId, String startTime, String endTime) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_fulfilled_request_transactions");
    var body = json.encode({"userId": userId, "startTime": startTime, "endTime": endTime});
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future walletToBank(WalletToBankModel walletToBankModel) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/transfer_wallet_amount_to_bank_account");
    var body = jsonEncode(walletToBankModel);
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getAllFriends(String userId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/unique_friends_transactions");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"userId": userId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  // user to user transactions
  @override
  Future getFriendRecentTransactions(String userId, String friendId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/user_profile_list_of_transactions");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"userId": userId, "friendId": friendId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future blockUser(String userId, String blockUserId)async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/block_user");
    var body = json.encode({"userId": userId, "blockUserId": blockUserId});
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future unBlockUser(String userId, String blockUserId)async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/unblock_user");
    var body = json.encode({"userId": userId, "blockUserId": blockUserId});
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getSavedCards(String userId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_creditcards");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"userId": userId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future addCard(AddCardModel cardModel) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/createCreditCard");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: jsonEncode(cardModel));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future addAmountToWallet(AddAmountToWalletModel amountToWalletModel) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_creditAmount_to_wallet");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: jsonEncode(amountToWalletModel));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getSingleTransaction(String userId, String transactionId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/single_view_transaction");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"userId": userId, "transactionId": transactionId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future searchUserByPhone(String userId, String searchNumber) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/search_phonenumber");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"userId": userId, "searchNumber": searchNumber}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future createSecurePin(String userId, String pin) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/create_securePin");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"userId": userId, "transferSecurePin": pin}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future resetSecurePin(String email) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/securePin_reset");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"email": email}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future forgotSecurePin(ForgotSecurePinModel forgotSecurePinModel) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/secure_forgot_password");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: jsonEncode(forgotSecurePinModel));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future addTransaction(AddTransactionModel addTransactionModel) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/wallet_to_wallet_transaction");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: jsonEncode(addTransactionModel));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getSocialMediaLinks() async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/fetch_social_media_links");
    try {
      final response = await http.get(url, headers: {"Content-type": "application/json; charset=UTF-8"});
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getTransactionFees() async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_transaction_fees");
    try {
      final response = await http.get(url, headers: {"Content-type": "application/json; charset=UTF-8"});
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }
}
