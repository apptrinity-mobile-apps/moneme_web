import 'package:mone_me/models/add_amount_to_wallet_model.dart';
import 'package:mone_me/models/add_bank_account_model.dart';
import 'package:mone_me/models/add_card_model.dart';
import 'package:mone_me/models/add_transaction_model.dart';
import 'package:mone_me/models/forgot_secure_pin_model.dart';
import 'package:mone_me/models/update_profile_model.dart';
import 'package:mone_me/models/user_login_model.dart';
import 'package:mone_me/models/user_sign_up_model.dart';
import 'package:mone_me/models/wallet_to_bank_model.dart';
import 'package:mone_me/services/api_services.dart';
import 'package:mone_me/services/base_services.dart';

class Repository {
  final BaseService _service = ApiServices();

  Future<dynamic> getAllCountries() async {
    dynamic response = await _service.getAllCountries();
    return response;
  }

  Future<dynamic> getAllLanguages() async {
    dynamic response = await _service.getAllLanguages();
    return response;
  }

  // login & signup
  Future<dynamic> verifyMobileNumber(String phoneNumber, String email) async {
    dynamic response = await _service.verifyMobileNumber(phoneNumber, email);
    return response;
  }

  Future<dynamic> createUser(UserSignUpModel signUpModel) async {
    dynamic response = await _service.createUser(signUpModel);
    return response;
  }

  Future<dynamic> updateProfile(UpdateProfileModel profileModel) async {
    dynamic response = await _service.updateProfile(profileModel);
    return response;
  }

  Future<dynamic> login(UserLoginModel loginModel) async {
    dynamic response = await _service.login(loginModel);
    return response;
  }

  Future<dynamic> logout(String userId) async {
    dynamic response = await _service.logout(userId);
    return response;
  }

  Future<dynamic> passwordReset(String phoneCode, String phoneNumber, String email) async {
    dynamic response = await _service.passwordReset(phoneCode, phoneNumber, email);
    return response;
  }

  Future<dynamic> forgotPasswordReset(
      String phoneCode, String phoneNumber, String email, String forgotPasswordOTP, String newPassword, String confirmNewPassword) async {
    dynamic response = await _service.forgotPasswordReset(phoneCode, phoneNumber, email, forgotPasswordOTP, newPassword, confirmNewPassword);
    return response;
  }

  //user data
  Future<dynamic> viewDashboard(String userId, String timezone) async {
    dynamic response = await _service.viewDashboard(userId, timezone);
    return response;
  }

  Future<dynamic> getAllBanks(String userId) async {
    dynamic response = await _service.getAllBanks(userId);
    return response;
  }

  Future<dynamic> addBankAccount(AddBankAccountModel model) async {
    dynamic response = await _service.addBankAccount(model);
    return response;
  }

  Future<dynamic> deleteBankAccount(String userId, String bankAccountId) async {
    dynamic response = await _service.deleteBankAccount(userId, bankAccountId);
    return response;
  }

  Future<dynamic> viewCustomerStripe(String userId) async {
    dynamic response = await _service.viewCustomerStripe(userId);
    return response;
  }

  Future<dynamic> updateCustomerStripe(String userId, String stripeId) async {
    dynamic response = await _service.updateCustomerStripe(userId, stripeId);
    return response;
  }

  // transactions
  Future<dynamic> geAllTransactions(String userId, String startTime, String endTime) async {
    dynamic response = await _service.geAllTransactions(userId, startTime, endTime);
    return response;
  }

  Future<dynamic> getAllTransferredTransactions(String userId, String startTime, String endTime) async {
    dynamic response = await _service.getAllTransferredTransactions(userId, startTime, endTime);
    return response;
  }

  Future<dynamic> getAllReceivedTransactions(String userId, String startTime, String endTime) async {
    dynamic response = await _service.getAllReceivedTransactions(userId, startTime, endTime);
    return response;
  }

  Future<dynamic> getAllRequestsTransactions(String userId, String startTime, String endTime) async {
    dynamic response = await _service.getAllRequestsTransactions(userId, startTime, endTime);
    return response;
  }

  Future<dynamic> walletToBank(WalletToBankModel walletToBankModel) async {
    dynamic response = await _service.walletToBank(walletToBankModel);
    return response;
  }

  Future<dynamic> getAllFriends(String userId) async {
    dynamic response = await _service.getAllFriends(userId);
    return response;
  }

  // user to user transactions
  Future<dynamic> getFriendRecentTransactions(String userId, String friendId) async {
    dynamic response = await _service.getFriendRecentTransactions(userId, friendId);
    return response;
  }

  Future<dynamic> blockUser(String userId, String blockUserId)async {
    dynamic response = await _service.blockUser(userId, blockUserId);
    return response;
  }

  Future<dynamic> unBlockUser(String userId, String blockUserId)async {
    dynamic response = await _service.unBlockUser(userId, blockUserId);
    return response;
  }

  Future<dynamic> getSavedCards(String userId) async {
    dynamic response = await _service.getSavedCards(userId);
    return response;
  }

  Future<dynamic> addCard(AddCardModel cardModel) async {
    dynamic response = await _service.addCard(cardModel);
    return response;
  }

  Future<dynamic> addAmountToWallet(AddAmountToWalletModel amountToWalletModel) async {
    dynamic response = await _service.addAmountToWallet(amountToWalletModel);
    return response;
  }

  Future<dynamic> getSingleTransaction(String userId, String transactionId) async {
    dynamic response = await _service.getSingleTransaction(userId, transactionId);
    return response;
  }

  Future<dynamic> searchUserByPhone(String userId, String searchNumber) async {
    dynamic response = await _service.searchUserByPhone(userId, searchNumber);
    return response;
  }

  Future<dynamic> createSecurePin(String userId, String pin) async {
    dynamic response = await _service.createSecurePin(userId, pin);
    return response;
  }

  Future<dynamic> resetSecurePin(String email) async {
    dynamic response = await _service.resetSecurePin(email);
    return response;
  }

  Future<dynamic> forgotSecurePin(ForgotSecurePinModel forgotSecurePinModel) async {
    dynamic response = await _service.forgotSecurePin(forgotSecurePinModel);
    return response;
  }

  Future<dynamic> addTransaction(AddTransactionModel addTransactionModel) async {
    dynamic response = await _service.addTransaction(addTransactionModel);
    return response;
  }

  Future<dynamic> getSocialMediaLinks() async {
    dynamic response = await _service.getSocialMediaLinks();
    return response;
  }

  Future<dynamic> getTransactionFees() async {
    dynamic response = await _service.getTransactionFees();
    return response;
  }
}
