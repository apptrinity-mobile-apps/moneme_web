enum TransactionTypes { debit, credit }

extension InnerPagesExtension on TransactionTypes {
  String? get name {
    switch (this) {
      case TransactionTypes.debit:
        return "debit";
      case TransactionTypes.credit:
        return "credit";
      default:
        return "debit";
    }
  }
}
