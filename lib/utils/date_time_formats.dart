import 'package:intl/intl.dart';

const inputDateFormat = "E, dd MMM yyyy HH:mm:ss ZZZ";

String dateFormatDMYHyphen(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  var outputFormat = DateFormat('dd-MM-yyyy');
  var output = outputFormat.format(tempDate);
  return output;
}

String dateFormatDMYSlash(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  var outputFormat = DateFormat('dd/MM/yyyy');
  var output = outputFormat.format(tempDate);
  return output;
}

String dateFormatDMY(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  var outputFormat = DateFormat('dd MMM yyyy');
  var output = outputFormat.format(tempDate);
  return output;
}

String dateFormatMDY(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  var outputFormat = DateFormat('MMMM dd, yyyy');
  var output = outputFormat.format(tempDate);
  return output;
}

String dateFormatMD(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  var outputFormat = DateFormat('MMM dd');
  var output = outputFormat.format(tempDate);
  return output;
}

String dateFormatYMDHyphen(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  var outputFormat = DateFormat('yyyy-MM-dd');
  var output = outputFormat.format(tempDate);
  return output;
}

String timeFormatHMS12hr(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  var outputFormat = DateFormat('hh:mma');
  var output = outputFormat.format(tempDate).toLowerCase();
  return output;
}

String timeFormatHMS24hr(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  var outputFormat = DateFormat('HH:mm');
  var output = outputFormat.format(tempDate).toLowerCase();
  return output;
}

String dayDate(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  var outputFormat = DateFormat('E, MMM dd');
  var outputDate = outputFormat.format(tempDate);
  return outputDate;
}

String dateFormatHMDMY(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  var outputFormat = DateFormat('hh:mm a, dd MMM yyyy');
  var outputDate = outputFormat.format(tempDate);
  return outputDate;
}

String getTodayDate() {
  String date = "";
  var dateNow = DateTime.now();
  var outputFormat = DateFormat('yyyy-MM-dd');
  date = outputFormat.format(dateNow);
  return date;
}

String dayAndTime(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  String time = timeFormatHMS12hr(inputDate);
  String date1 = dateFormatDMYSlash(inputDate);
  var outputDate = "";
  if (daysDifference(tempDate) == -1) {
    // yesterday
    outputDate = "Yesterday, $time";
  } else if (daysDifference(tempDate) == 0) {
    // today
    outputDate = "Today, $time";
  } else {
    outputDate = "$date1, $time";
  }
  return outputDate;
}

int daysDifference(DateTime date) {
  DateTime now = DateTime.now();
  return DateTime(date.year, date.month, date.day).difference(DateTime(now.year, now.month, now.day)).inDays;
}
