import 'package:mone_me/models/friends_response_model.dart';

class Globals {
  // UniqueFriendsList item for viewing & passing data to "profileTransactionDetailsWidget"
  static UniqueFriendsList? unqFriendData;

  UniqueFriendsList? get getFriendData {
    return unqFriendData;
  }
}
