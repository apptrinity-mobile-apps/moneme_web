import 'package:mone_me/utils/strings.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SessionManager {
  userLogin(bool isLogin) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(keyIsLogged, isLogin);
  }

  Future<bool?> isUserLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(keyIsLogged) ?? false;
  }

  saveLoginDetails(String loginDetails) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(keyLoginDetails, loginDetails);
  }

  Future<String?> getLoginDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(keyLoginDetails);
  }

  // clearing total session data
  Future clearSession() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    Set<String> keys = preferences.getKeys();
    for (String key in keys) {
      preferences.remove(key);
    }
  }
}
