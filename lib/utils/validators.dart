import 'package:mone_me/models/countries_response_model.dart';
import 'package:mone_me/models/languages_response_model.dart';
import 'package:mone_me/utils/strings.dart';

String? passwordValidator(String? value) {
  // RegExp regExp = RegExp(passwordPattern);
  if (value == null || value.isEmpty) {
    return emptyFieldError;
  }
  /*else if (!regExp.hasMatch(value)) {
    return "Enter a valid password";
  }*/
  else {
    return null;
  }
}

String? emailValidator(String? value) {
  RegExp regex = RegExp(emailPattern);
  if (value == null || value.isEmpty) {
    return emptyFieldError;
  } else if (!regex.hasMatch(value)) {
    return enterValidEmail;
  } else {
    return null;
  }
}

String? phoneValidator(String? value) {
  if (value == null || value.isEmpty) {
    return emptyFieldError;
  }
  /*else if (value.length < 10) {
    return phone10Digits;
  }*/
  else {
    return null;
  }
}

String? emptyTextValidator(String? value) {
  if (value == null || value.isEmpty) {
    return emptyFieldError;
  } else {
    return null;
  }
}

String? amountValidator(String? value) {
  if (value!.isEmpty) {
    return errorValidAmount;
  } else {
    return null;
  }
}

String? pinPutValidator(String? value) {
  if (value == null || value.isEmpty) {
    return emptyFieldError;
  } else if (value.length != 6) {
    return pinPutLengthError;
  } else {
    return null;
  }
}

String? pinPut4DigitPValidator(String? value) {
  if (value == null || value.isEmpty) {
    return emptyFieldError;
  } else if (value.length != 4) {
    return pinPutLength4Error;
  } else {
    return null;
  }
}

String? countriesDropDownValidator(CountriesList? value) {
  if (value == null || value.name == null) {
    return emptyFieldError;
  } else if (value.name.toString() == selectCountry) {
    return pleaseSelectCountry;
  } else {
    return null;
  }
}

String? languagesDropDownValidator(LanguagesList? value) {
  if (value == null || value.name == null) {
    return emptyFieldError;
  } else if (value.name.toString() == selectLanguage) {
    return pleaseSelectLanguage;
  } else {
    return null;
  }
}

String? minAccountNumberDigitsValidator(String? value) {
  if (value == null || value.isEmpty) {
    return emptyFieldError;
  } else if (value.length < 9) {
    return 'Enter a minimum of 9 chars';
  } else {
    return null;
  }
}
