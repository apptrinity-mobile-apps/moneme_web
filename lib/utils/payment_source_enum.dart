enum PaymentSource { wallet, user }

extension InnerPagesExtension on PaymentSource {
  String? get name {
    switch (this) {
      case PaymentSource.wallet:
        return "wallet";
      case PaymentSource.user:
        return "user";
      default:
        return "wallet";
    }
  }
}
