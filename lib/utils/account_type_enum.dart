enum AccountType { bank, card }

extension InnerPagesExtension on AccountType {
  String? get bank {
    switch (this) {
      case AccountType.bank:
        return "bank";
      case AccountType.card:
        return "card";
      default:
        return "bank";
    }
  }
}
