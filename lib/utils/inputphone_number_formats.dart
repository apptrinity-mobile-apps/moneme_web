import 'package:easy_mask/easy_mask.dart';

String inputPhoneNumberFormatByCountry(String phoneNumber, String code) {
  MagicMask? mask;
  if (code == '+1') {
    mask = MagicMask.buildMask('(999)999-9999');
  } else if (code == '+61') {
    mask = MagicMask.buildMask('9-9999-9999');
  } else if (code == '+91') {
    mask = MagicMask.buildMask('99999 99999');
  } else if (code == '+965') {
    mask = MagicMask.buildMask('99999999');
  }
  String formatted = mask!.getMaskedString(phoneNumber);
  return formatted;
}

String phoneNumberFormatByCountry(String phoneNumber, String code) {
  MagicMask? mask;
  if (code == '+1') {
    mask = MagicMask.buildMask('\\$code (999)999-9999');
  } else if (code == '+61') {
    mask = MagicMask.buildMask('\\$code 9-9999-9999');
  } else if (code == '+91') {
    mask = MagicMask.buildMask('\\$code 99999 99999');
  } else if (code == '+965') {
    mask = MagicMask.buildMask('\\$code 99999999');
  }
  String formatted = mask!.getMaskedString(phoneNumber);
  return formatted;
}
