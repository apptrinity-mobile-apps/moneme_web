import 'package:flutter/services.dart';

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    String formattedText = newValue.text.split(' ').map((element) => textToUpperCase(element)).toList().join(' ');
    return TextEditingValue(
      text: formattedText,
      selection: newValue.selection,
    );
  }

  String textToUpperCase(String text) {
    if (text.length > 1) {
      return text.toUpperCase();
    } else if (text.length == 1) {
      return text[0].toUpperCase();
    }
    return '';
  }
}

class CamelCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    String formattedText = newValue.text.split(' ').map((element) => textToTitleCase(element)).toList().join(' ');
    return TextEditingValue(
      text: formattedText,
      selection: newValue.selection,
    );
  }

  String textToTitleCase(String text) {
    if (text.length > 1) {
      return text[0].toUpperCase() + text.substring(1);
    } else if (text.length == 1) {
      return text[0].toUpperCase();
    }
    return '';
  }
}
