import 'package:flutter/material.dart';
import 'package:mone_me/screens/home/ui/activate_video_consultation_page.dart';
import 'package:mone_me/screens/bank_account/ui/add_bank_accound_page.dart';
import 'package:mone_me/screens/home/ui/home_page.dart';
import 'package:mone_me/screens/home/ui/profile_page.dart';
import 'package:mone_me/screens/bank_account/ui/linked_bank_accounts_page.dart';
import 'package:mone_me/screens/bank_account/ui/transfer_to_bank_page.dart';
import 'package:mone_me/screens/bank_account/ui/transfer_to_bank_success_page.dart';
import 'package:mone_me/screens/home/ui/transaction_details_page.dart';
import 'package:mone_me/screens/home/ui/user_to_user_transactions_page.dart';
import 'package:mone_me/screens/login/ui/forgot_password_page.dart';
import 'package:mone_me/screens/login/ui/login_page.dart';
import 'package:mone_me/screens/sign_up/ui/sign_up_page.dart';
import 'package:mone_me/screens/splash/ui/splash_screen.dart';
import 'package:mone_me/screens/wallet/ui/wallet_page.dart';
import 'package:mone_me/utils/routes.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case AppRoutes.splash:
        return buildRoute(const SplashScreen(), settings: settings);
      case AppRoutes.login:
        return buildRoute(const LoginPage(), settings: settings);
      case AppRoutes.signUp:
        return buildRoute(const SignUpPage(), settings: settings);
      case AppRoutes.forgotPassword:
        return buildRoute(const ForgotPasswordPage(), settings: settings);
      case AppRoutes.home:
        return buildRoute(const HomePage(), settings: settings);
      case AppRoutes.wallet:
        return buildRoute(const WalletPage(), settings: settings);
      case AppRoutes.linkedBankAccounts:
        return buildRoute(const LinkedBankAccountsPage(), settings: settings);
      case AppRoutes.addBankAccount:
        return buildRoute(const AddBankAccountPage(), settings: settings);
      case AppRoutes.transferToBank:
        return buildRoute(const TransferToBankPage(), settings: settings);
      case AppRoutes.bankTransferSuccess:
        return buildRoute(const TransferSuccessPage(), settings: settings);
      case AppRoutes.transactionDetails:
        return buildRoute(const TransactionDetailsPage(), settings: settings);
      case AppRoutes.transactionHistory:
        return buildRoute(const HomePage(), settings: settings);
      case AppRoutes.userToUserTransactions:
        return buildRoute(const UserToUserTransactionsPage(), settings: settings);
      case AppRoutes.profile:
        return buildRoute(const ProfilePage(), settings: settings);
      case AppRoutes.activateVideoConsultation:
        return buildRoute(const ActivateVideoConsultPage(), settings: settings);
      default:
        return _errorRoute();
    }
  }

  static NoAnimationMaterialPageRoute buildRoute(Widget child, {required RouteSettings settings}) {
    return NoAnimationMaterialPageRoute(settings: settings, builder: (context) => child);
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              children: const [
                Text(
                  '404: Seems the route you\'ve navigated to doesn\'t exist!!',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}

class NoAnimationMaterialPageRoute<T> extends MaterialPageRoute<T> {
  NoAnimationMaterialPageRoute({
    required WidgetBuilder builder,
    RouteSettings? settings,
    bool maintainState = true,
    bool fullscreenDialog = false,
  }) : super(builder: builder, maintainState: maintainState, settings: settings, fullscreenDialog: fullscreenDialog);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
    return child;
  }
}
