enum TransactionMessage { message, transaction }

extension InnerPagesExtension on TransactionMessage {
  String? get name {
    switch (this) {
      case TransactionMessage.message:
        return "message";
      case TransactionMessage.transaction:
        return "transaction";
      default:
        return "message";
    }
  }
}
