import 'package:flutter/cupertino.dart';
import 'package:mone_me/models/countries_response_model.dart';

/// Returns true when a country should be included in lists / dialogs
/// offered to the user.
typedef bool ItemFilter(CountriesList country);

///Predicate to be satisfied in order to add country to search list
typedef bool SearchFilter(CountriesList country, String searchWord);

typedef Widget ItemBuilder(CountriesList country);

/// Simple closure which always returns true.
bool acceptAllCountries(_) => true;