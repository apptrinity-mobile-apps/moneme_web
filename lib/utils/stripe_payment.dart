import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart' as http;
import 'package:mone_me/models/card_response_model.dart';
import 'package:mone_me/models/stripecustomermodel.dart';
import 'package:stripe_payment/stripe_payment.dart';

class StripeTransactionResponse {
  String? message;
  bool? success;

  StripeTransactionResponse({
    this.message,
    this.success,
  });
}

class StripeServices {
  static String apiBase = 'https://api.stripe.com/v1';
  static String paymentApiUrl = '${StripeServices.apiBase}/payment_intents';
  static Uri paymentApiUri = Uri.parse(paymentApiUrl);
  static String secret = 'sk_test_51JGMaCFTNJJcMAGxfMOyqLMwWdtvLxJGip9Or0W4JcwktmgjGGFxlAyoqXnnG0Wiica7rv6KNpiZq2yYvfjU9EAy00bL65GwjK';

  static Map<String, String> headers = {'Authorization': 'Bearer ${StripeServices.secret}', 'Content-Type': 'application/x-www-form-urlencoded'};
  static PaymentMethod? paymentMethod;
  static Token? cardtoken;

  static init() {
    StripePayment.setOptions(StripeOptions(
        publishableKey: 'pk_test_51JGMaCFTNJJcMAGx73DPkLtBOycCkfPg8F1qebJuYlIIToceU2MoUh9C0MZXuO20YhZiuCIPPTudI5EDO6DTFZ2M00EDusXSs8',
        androidPayMode: 'test',
        merchantId: 'test'));
  }

  static Future<Map<String, dynamic>> createPaymentIntent(String amount, String currency, String customerid) async {
    try {
      Map<String, dynamic> body = {
        'amount': amount,
        'currency': currency,
        'customer': customerid,
      };
      var response = await http.post(paymentApiUri, headers: headers, body: body);
      return jsonDecode(response.body);
    } catch (error) {
      print('error Happened');
      throw error;
    }
  }

  static getErrorAndAnalyze(err) {
    String message = 'Something went wrong';
    if (err.code == 'cancelled') {
      message = 'Transaction canceled';
    }
    return StripeTransactionResponse(message: message, success: false);
  }

  static Future<CardResponseModel> addcardtocustomer(String customerId, String token) async {
    final String url = 'https://api.stripe.com/v1/customers/$customerId/sources';
    var response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: {
        'source': token,
      },
    );
    if (response.statusCode == 200) {
      log(response.body);
      CardResponseModel result_response = CardResponseModel.fromJson(jsonDecode(response.body));
      return result_response;
    } else {
      print(json.decode(response.body));
      throw 'Failed to attach PaymentMethod.';
    }
  }

  static Future<CreateStripeCustomerModel> createCustomerStripe(Uri url, String name, String email) async {
    try {
      Map<String, dynamic> body = {
        'name': name,
        'email': email,
      };
      var response = await http.post(url, headers: headers, body: body);
      return CreateStripeCustomerModel.fromJson(jsonDecode(response.body));
    } catch (error) {
      print('error Happened');
      throw error;
    }
  }
}

