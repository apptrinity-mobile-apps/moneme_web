enum InnerPages {
  transactionHistory,
  profileTransactions,
  addMoneySend,
  confirmOTPAfterAmount,
  transactionDetails,
  myWallet,
  addMoneyWallet,
  addMoneyWalletSuccess,
  editProfile
}

extension InnerPagesExtension on InnerPages {
  String? get name {
    switch (this) {
      case InnerPages.transactionHistory:
        return "transaction_history";
      case InnerPages.profileTransactions:
        return "profile_transaction";
      case InnerPages.addMoneySend:
        return "add_money_send";
      case InnerPages.confirmOTPAfterAmount:
        return "confirm_otp_amount";
      case InnerPages.transactionDetails:
        return "transaction_details";
      case InnerPages.myWallet:
        return "my_wallet";
      case InnerPages.addMoneyWallet:
        return "add_money_to_wallet";
      case InnerPages.addMoneyWalletSuccess:
        return "add_money_wallet_success";
      case InnerPages.editProfile:
        return "edit_profile";
      default:
        return "transaction_history";
    }
  }
}