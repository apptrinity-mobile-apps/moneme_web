import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/strings.dart';

const maxAmount = 1000000;
/* constants */
const EdgeInsets textFormFieldPadding = EdgeInsets.fromLTRB(20, 20, 20, 20);
const EdgeInsets searchFormFieldPadding = EdgeInsets.fromLTRB(20, 10, 20, 10);
const EdgeInsets cardDetailsFieldPadding = EdgeInsets.fromLTRB(10, 10, 10, 10);
const buttonGradient = LinearGradient(colors: <Color>[buttonGradientStart, buttonGradientEnd]);
const BorderRadius defaultBorderRadius =
    BorderRadius.only(topLeft: Radius.circular(30), bottomLeft: Radius.circular(6), topRight: Radius.circular(6), bottomRight: Radius.circular(30));
const BorderRadius amountsIconBorderRadius =
    BorderRadius.only(topLeft: Radius.circular(25), bottomLeft: Radius.circular(2), topRight: Radius.circular(2), bottomRight: Radius.circular(25));
const BorderRadius amountsBorderRadius =
    BorderRadius.only(topLeft: Radius.circular(0), bottomLeft: Radius.circular(40), topRight: Radius.circular(40), bottomRight: Radius.circular(0));
const BorderRadius totalAmountBorderRadius =
    BorderRadius.only(topLeft: Radius.circular(0), bottomLeft: Radius.circular(0), topRight: Radius.circular(50), bottomRight: Radius.circular(0));
const BorderRadius transferredAmountBorderRadius =
    BorderRadius.only(topLeft: Radius.circular(0), bottomLeft: Radius.circular(50), topRight: Radius.circular(0), bottomRight: Radius.circular(0));
const BorderRadius detailsMainBorderRadius =
    BorderRadius.only(topLeft: Radius.circular(0), bottomLeft: Radius.circular(0), topRight: Radius.circular(0), bottomRight: Radius.circular(50));
const BorderRadius detailsBorderRadius =
    BorderRadius.only(topLeft: Radius.circular(50), bottomLeft: Radius.circular(0), topRight: Radius.circular(0), bottomRight: Radius.circular(50));
const BorderRadius peopleBorderRadius =
    BorderRadius.only(topLeft: Radius.circular(75), bottomLeft: Radius.circular(0), topRight: Radius.circular(0), bottomRight: Radius.circular(75));
const BorderRadius peopleProfileBorderRadius =
    BorderRadius.only(topLeft: Radius.circular(30), bottomLeft: Radius.circular(0), topRight: Radius.circular(0), bottomRight: Radius.circular(30));
const BorderRadius payBorderRadius =
    BorderRadius.only(topLeft: Radius.circular(25), bottomLeft: Radius.circular(6), topRight: Radius.circular(6), bottomRight: Radius.circular(25));
const BorderRadius paymentTransactionsRadius =
    BorderRadius.only(topLeft: Radius.circular(25), bottomLeft: Radius.circular(0), topRight: Radius.circular(0), bottomRight: Radius.circular(25));
BorderRadius searchBorderRadius = BorderRadius.circular(50);

/* text styles */
TextStyle snackBarDarkTextStyle =
    const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: CupertinoColors.black, fontSize: 14);
TextStyle snackBarLightTextStyle =
    const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: CupertinoColors.white, fontSize: 14);
TextStyle welcomeTextStyle =
    const TextStyle(letterSpacing: 1.38, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: defaultText, fontSize: 34);
TextStyle signInContinueTextStyle =
    const TextStyle(letterSpacing: 1.2, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: defaultText, fontSize: 15);
TextStyle signUpContinueTextStyle =
    const TextStyle(letterSpacing: 1.2, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: buttonGradientStart, fontSize: 18);
TextStyle resendTextStyle =
    const TextStyle(letterSpacing: 1.2, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: buttonGradientEnd, fontSize: 18);
TextStyle forgotPasswordTextStyle =
    const TextStyle(letterSpacing: 1.05, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: defaultText, fontSize: 16);
TextStyle pinPutTextStyle =
    const TextStyle(letterSpacing: 1, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: CupertinoColors.white, fontSize: 18);
TextStyle textFormFieldHintStyle =
    TextStyle(letterSpacing: 1.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: defaultText.withOpacity(0.7), fontSize: 16);
TextStyle textFormFieldStyle =
    const TextStyle(letterSpacing: 1.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: defaultText, fontSize: 16);
TextStyle phoneDropdownStyle =
    const TextStyle(letterSpacing: 0, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: defaultText, fontSize: 14);
TextStyle searchFormFieldStyle =
    const TextStyle(letterSpacing: 1.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: defaultText, fontSize: 16);
TextStyle submitButtonTextStyle =
    const TextStyle(letterSpacing: 1.5, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: CupertinoColors.white, fontSize: 18);
TextStyle headerTextStyle =
    const TextStyle(letterSpacing: 1.1, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: defaultHeaderText, fontSize: 14);
TextStyle logoutHeaderTextStyle =
    const TextStyle(letterSpacing: 0, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: defaultText, fontSize: 18);
TextStyle logoutContentHeaderTextStyle =
    const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: defaultText, fontSize: 16);
TextStyle logoutButtonHeaderTextStyle =
    const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: defaultText, fontSize: 14);
TextStyle popMenuHeaderTextStyle =
    const TextStyle(letterSpacing: 0.8, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: optionsColor, fontSize: 14);
TextStyle amountHeaderTextStyle =
    const TextStyle(letterSpacing: 1.9, fontFamily: "Aller", fontWeight: FontWeight.w700, color: defaultBackground, fontSize: 20);
TextStyle amountDescriptionHeaderTextStyle =
    const TextStyle(letterSpacing: 1.2, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: defaultBackground, fontSize: 16);
TextStyle headersTextStyle =
    const TextStyle(letterSpacing: 1.5, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: headersText, fontSize: 18);
TextStyle showTextStyle = const TextStyle(letterSpacing: 1.21, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: headersText, fontSize: 15);
TextStyle peopleNameListTextStyle =
    const TextStyle(letterSpacing: 1.07, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: headersText, fontSize: 14);
TextStyle utilitiesNameListTextStyle =
    const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: headersText, fontSize: 14);
TextStyle peopleNameListIconTextStyle =
    const TextStyle(letterSpacing: 1.38, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: CupertinoColors.white, fontSize: 28);
TextStyle tabSelectedTextStyle =
    const TextStyle(letterSpacing: 1.19, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: tabsHeaderColor, fontSize: 14);
TextStyle tabUnSelectedTextStyle =
    TextStyle(letterSpacing: 1.19, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: tabsHeaderColor.withOpacity(0.5), fontSize: 14);
TextStyle sendHistoryAmountTextStyle =
    const TextStyle(letterSpacing: 1.43, fontFamily: "Aller", fontWeight: FontWeight.w700, color: sendMoneyText, fontSize: 18);
TextStyle addHistoryAmountTextStyle =
    const TextStyle(letterSpacing: 1.43, fontFamily: "Aller", fontWeight: FontWeight.w700, color: addMoneyText, fontSize: 18);
TextStyle nameHistoryAmountTextStyle =
    const TextStyle(letterSpacing: 1.43, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: nameText, fontSize: 18);
TextStyle typeHistoryAmountTextStyle =
    TextStyle(letterSpacing: 1.21, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: nameText.withOpacity(0.6), fontSize: 15);
TextStyle profilePhoneTextStyle =
    const TextStyle(letterSpacing: 0.85, fontFamily: "Aller", fontWeight: FontWeight.w400, color: profilePhoneText, fontSize: 15);
TextStyle profileNameTextStyle =
    const TextStyle(letterSpacing: 1.1, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: nameText, fontSize: 17);
TextStyle paymentButtonsTextStyle =
    const TextStyle(letterSpacing: 1.19, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: CupertinoColors.white, fontSize: 15);
TextStyle paymentDateSeparatorTextStyle =
    const TextStyle(letterSpacing: 0.85, fontFamily: "Aller", fontWeight: FontWeight.w400, color: nameText, fontSize: 15);
TextStyle paymentAmountTextStyle =
    const TextStyle(letterSpacing: 2.57, fontFamily: "Aller", fontWeight: FontWeight.w400, color: nameText, fontSize: 18);
TextStyle paymentDateTextStyle =
    const TextStyle(letterSpacing: 0.86, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: nameText, fontSize: 12);
TextStyle numPadTextStyle = const TextStyle(letterSpacing: 1.38, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: nameText, fontSize: 16);
TextStyle enteredAmountTextStyle =
    const TextStyle(letterSpacing: 3.14, fontFamily: "Aller", fontWeight: FontWeight.w700, color: nameText, fontSize: 20);
TextStyle enteredAmountHintTextStyle =
    TextStyle(letterSpacing: 3.14, fontFamily: "Aller", fontWeight: FontWeight.w600, color: nameText.withOpacity(0.5), fontSize: 20);
TextStyle paymentSuccessDetailsTextStyle =
    const TextStyle(letterSpacing: 0.85, fontFamily: "Aller", fontWeight: FontWeight.w400, color: profilePhoneText, fontSize: 14);
TextStyle paymentSuccessAmountTextStyle =
    const TextStyle(letterSpacing: 3.86, fontFamily: "Aller", fontWeight: FontWeight.w700, color: nameText, fontSize: 24);
TextStyle paymentSuccessDateTextStyle =
    const TextStyle(letterSpacing: 1.21, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: nameText, fontSize: 14);
TextStyle paymentSuccessHeadersTextStyle =
    const TextStyle(letterSpacing: 0.95, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: nameText, fontSize: 14);
TextStyle paymentSuccessMainHeaderTextStyle =
    const TextStyle(letterSpacing: 1.38, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: nameText, fontSize: 18);
TextStyle walletAvailableBalanceHeaderTextStyle =
    const TextStyle(letterSpacing: 1.06, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: availableBalanceText, fontSize: 14);
TextStyle walletAvailableBalanceTextStyle =
    const TextStyle(letterSpacing: 1.06, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: totalIconBackground, fontSize: 15);
TextStyle walletTextStyle =
    const TextStyle(letterSpacing: 1.06, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: totalIconBackground, fontSize: 14);
TextStyle addAmountWalletTextStyle =
    const TextStyle(letterSpacing: 1.94, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: nameText, fontSize: 18);
TextStyle addAmountWalletHintTextStyle =
    TextStyle(letterSpacing: 1.94, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: nameText.withOpacity(0.5), fontSize: 18);
TextStyle addMoneyTextStyle = const TextStyle(letterSpacing: 1.25, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: nameText, fontSize: 15);
TextStyle recommendedTextStyle =
    const TextStyle(letterSpacing: 1.25, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: nameText, fontSize: 17);
TextStyle recommendedAmountTextStyle =
    const TextStyle(letterSpacing: 1.44, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: nameText, fontSize: 14);
TextStyle addMoneyWalletTextStyle =
    const TextStyle(letterSpacing: 0.15, fontFamily: "Aller", fontWeight: FontWeight.w700, color: nameText, fontSize: 22);
TextStyle walletAmountTextStyle =
    const TextStyle(letterSpacing: 2.63, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: totalIconBackground, fontSize: 22);
TextStyle tabWalletSelectedTextStyle =
    TextStyle(letterSpacing: 1.25, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: totalIconBackground.withOpacity(0.8), fontSize: 16);
TextStyle tabWalletUnSelectedTextStyle =
    TextStyle(letterSpacing: 1.25, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: nameText.withOpacity(0.8), fontSize: 16);
TextStyle addNewBankTextStyle =
    const TextStyle(letterSpacing: 1.19, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: nameText, fontSize: 14);
TextStyle addCardTextStyle = const TextStyle(letterSpacing: 1.13, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: nameText, fontSize: 14);
TextStyle addCardHintTextStyle =
    TextStyle(letterSpacing: 1.13, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: nameText.withOpacity(0.6), fontSize: 14);
TextStyle saveCardHelpTextStyle =
    const TextStyle(letterSpacing: 1.1, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: defaultHeaderText, fontSize: 12);
TextStyle cardNameTextStyle = const TextStyle(letterSpacing: 1.13, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: nameText, fontSize: 15);
TextStyle cardNumberTextStyle =
    const TextStyle(letterSpacing: 1.13, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: nameText, fontSize: 14);
TextStyle walletTransactionDetailsTextStyle =
    const TextStyle(letterSpacing: 1.19, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: nameText, fontSize: 15);
TextStyle walletTransactionBalanceTextStyle =
    const TextStyle(letterSpacing: 1.19, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: nameText, fontSize: 15);
TextStyle walletTransactionBalanceAmountTextStyle =
    const TextStyle(letterSpacing: 1.19, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: totalIconBackground, fontSize: 15);
TextStyle transactionMessageTextStyle =
    const TextStyle(letterSpacing: 0.5, fontFamily: 'Poppins', fontWeight: FontWeight.w400, color: nameText, fontSize: 16);

/* text form fields decorators */
InputDecoration firstNameDecoration = InputDecoration(
  fillColor: CupertinoColors.white,
  filled: true,
  border: border,
  isDense: true,
  enabledBorder: border,
  focusedBorder: border,
  errorBorder: errorBorder,
  focusedErrorBorder: errorBorder,
  contentPadding: textFormFieldPadding,
  hintStyle: textFormFieldHintStyle,
  hintText: firstName,
  prefixIcon: Container(
    width: 90,
    alignment: Alignment.center,
    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
    // child: Image.asset("assets/images/user_icon.png", height: 15, width: 20),
    child: const Icon(Icons.person_outline, size: 28, color: defaultText),
  ),
);
InputDecoration lastNameDecoration = InputDecoration(
  fillColor: CupertinoColors.white,
  filled: true,
  border: border,
  isDense: true,
  enabledBorder: border,
  focusedBorder: border,
  errorBorder: errorBorder,
  focusedErrorBorder: errorBorder,
  contentPadding: textFormFieldPadding,
  hintStyle: textFormFieldHintStyle,
  hintText: lastName,
  prefixIcon: Container(
    width: 90,
    alignment: Alignment.center,
    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
    // child: Image.asset("assets/images/user_icon.png", height: 15, width: 20),
    child: const Icon(Icons.person_outline, size: 28, color: defaultText),
  ),
);
InputDecoration emailDecoration = InputDecoration(
  fillColor: CupertinoColors.white,
  filled: true,
  border: border,
  isDense: true,
  enabledBorder: border,
  focusedBorder: border,
  errorBorder: errorBorder,
  focusedErrorBorder: errorBorder,
  contentPadding: textFormFieldPadding,
  hintStyle: textFormFieldHintStyle,
  hintText: email,
  prefixIcon: Container(
    width: 90,
    alignment: Alignment.center,
    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
// child: Image.asset("assets/images/email_icon.png", height: 15, width: 20),
    child: const Icon(Icons.email_outlined, size: 28, color: defaultText),
  ),
);
InputDecoration languageDecoration = InputDecoration(
  fillColor: CupertinoColors.white,
  filled: true,
  border: border,
  isDense: true,
  enabledBorder: border,
  focusedBorder: border,
  errorBorder: errorBorder,
  focusedErrorBorder: errorBorder,
  contentPadding: const EdgeInsets.fromLTRB(0, 20, 10, 20),
  hintStyle: textFormFieldHintStyle,
  hintText: language,
  prefixIcon: Container(
    width: 90,
    alignment: Alignment.center,
    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
    // child: Image.asset("assets/images/email_icon.png", height: 15, width: 20),
    child: const Icon(Icons.language, size: 28, color: defaultText),
  ),
);
InputDecoration countryDecoration = InputDecoration(
  fillColor: CupertinoColors.white,
  filled: true,
  border: border,
  isDense: true,
  enabledBorder: border,
  focusedBorder: border,
  errorBorder: errorBorder,
  focusedErrorBorder: errorBorder,
  contentPadding: const EdgeInsets.fromLTRB(0, 20, 10, 20),
  hintStyle: textFormFieldHintStyle,
  hintText: country,
  prefixIcon: Container(
    width: 90,
    alignment: Alignment.center,
    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
    // child: Image.asset("assets/images/email_icon.png", height: 15, width: 20),
    child: const Icon(Icons.flag_outlined, size: 28, color: defaultText),
  ),
);
InputDecoration searchDecorator = InputDecoration(
  fillColor: CupertinoColors.white,
  filled: true,
  border: searchBorder,
  isDense: true,
  enabledBorder: searchBorder,
  focusedBorder: searchBorder,
  errorBorder: searchErrorBorder,
  focusedErrorBorder: searchErrorBorder,
  contentPadding: searchFormFieldPadding,
  hintStyle: textFormFieldHintStyle,
  hintText: searchByPhone,
  suffixIcon: const Icon(Icons.search, size: 25, color: defaultText),
);
InputDecoration addMoneyDecoration = InputDecoration(
    fillColor: Colors.transparent,
    filled: true,
    border: InputBorder.none,
    isDense: true,
    enabledBorder: InputBorder.none,
    focusedBorder: InputBorder.none,
    errorBorder: InputBorder.none,
    focusedErrorBorder: InputBorder.none,
    contentPadding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
    hintStyle: enteredAmountHintTextStyle,
    hintText: defaultAmount);
InputDecoration addMoneyToWalletDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: addMoneyToWalletBorder,
    isDense: true,
    enabledBorder: addMoneyToWalletBorder,
    focusedBorder: addMoneyToWalletBorder,
    errorBorder: addMoneyToWalletErrorBorder,
    focusedErrorBorder: addMoneyToWalletErrorBorder,
    contentPadding: cardDetailsFieldPadding,
    hintStyle: addAmountWalletHintTextStyle,
    hintText: defaultAmount);
InputDecoration cardNumberDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: InputBorder.none,
    isDense: true,
    enabledBorder: InputBorder.none,
    focusedBorder: InputBorder.none,
    errorBorder: addMoneyToWalletErrorBorder,
    focusedErrorBorder: addMoneyToWalletErrorBorder,
    contentPadding: cardDetailsFieldPadding,
    hintStyle: addCardHintTextStyle,
    hintText: enterCardNumber);
InputDecoration cardNameDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: InputBorder.none,
    isDense: true,
    enabledBorder: InputBorder.none,
    focusedBorder: InputBorder.none,
    errorBorder: addMoneyToWalletErrorBorder,
    focusedErrorBorder: addMoneyToWalletErrorBorder,
    contentPadding: cardDetailsFieldPadding,
    hintStyle: addCardHintTextStyle,
    hintText: enterCardName);
InputDecoration cardMonthDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: InputBorder.none,
    isDense: true,
    enabledBorder: InputBorder.none,
    focusedBorder: InputBorder.none,
    errorBorder: addMoneyToWalletErrorBorder,
    focusedErrorBorder: addMoneyToWalletErrorBorder,
    contentPadding: cardDetailsFieldPadding,
    hintStyle: addCardHintTextStyle,
    hintText: cardMonth);
InputDecoration cardYearDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: InputBorder.none,
    isDense: true,
    enabledBorder: InputBorder.none,
    focusedBorder: InputBorder.none,
    errorBorder: addMoneyToWalletErrorBorder,
    focusedErrorBorder: addMoneyToWalletErrorBorder,
    contentPadding: cardDetailsFieldPadding,
    hintStyle: addCardHintTextStyle,
    hintText: cardYear);
InputDecoration cardCvvDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: InputBorder.none,
    isDense: true,
    enabledBorder: InputBorder.none,
    focusedBorder: InputBorder.none,
    errorBorder: addMoneyToWalletErrorBorder,
    focusedErrorBorder: addMoneyToWalletErrorBorder,
    contentPadding: cardDetailsFieldPadding,
    hintStyle: addCardHintTextStyle,
    hintText: cardCVV);
InputDecoration bankAccountDetailsDecorator = InputDecoration(
  fillColor: CupertinoColors.white,
  filled: true,
  isDense: true,
  border: border.copyWith(
    borderRadius: amountsIconBorderRadius,
    borderSide: const BorderSide(color: profileFieldsBorder),
  ),
  enabledBorder: border.copyWith(
    borderRadius: amountsIconBorderRadius,
    borderSide: const BorderSide(color: profileFieldsBorder),
  ),
  focusedBorder: border.copyWith(
    borderRadius: amountsIconBorderRadius,
    borderSide: const BorderSide(color: profileFieldsBorder),
  ),
  errorBorder: errorBorder.copyWith(borderRadius: amountsIconBorderRadius),
  focusedErrorBorder: errorBorder.copyWith(borderRadius: amountsIconBorderRadius),
  contentPadding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
  hintStyle: textFormFieldHintStyle,
  hintText: accNumber,
);

/* borders for edit texts */
OutlineInputBorder border = const OutlineInputBorder(borderSide: BorderSide(color: spinnerDropDown, width: 1), borderRadius: defaultBorderRadius);
OutlineInputBorder errorBorder =
    const OutlineInputBorder(borderSide: BorderSide(color: Colors.redAccent, width: 1), borderRadius: defaultBorderRadius);
OutlineInputBorder addMoneyToWalletBorder =
    OutlineInputBorder(borderSide: const BorderSide(color: totalIconBackground, width: 1), borderRadius: BorderRadius.circular(0));
OutlineInputBorder addMoneyToWalletErrorBorder =
    OutlineInputBorder(borderSide: const BorderSide(color: Colors.redAccent, width: 1), borderRadius: BorderRadius.circular(0));
OutlineInputBorder searchBorder =
    OutlineInputBorder(borderSide: const BorderSide(color: spinnerDropDown, width: 1), borderRadius: searchBorderRadius);
OutlineInputBorder searchErrorBorder =
    OutlineInputBorder(borderSide: const BorderSide(color: Colors.redAccent, width: 1), borderRadius: searchBorderRadius);
