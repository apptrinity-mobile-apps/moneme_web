class AppRoutes {
  static const splash = '/';
  static const home = '/home';
  static const login = '/login';
  static const signUp = '/sign-up';
  static const forgotPassword = '/forgot-password';
  static const settings = '/settings';
  static const profile = '/profile';
  static const activateVideoConsultation = '/activate-video-consultation';
  static const inviteFriend = '/invite';
  static const helpFeedback = '/help-feedback';
  static const linkedBankAccounts = '/linked-bank-accounts';
  static const addBankAccount = '/link-bank-new';
  static const transferToBank = '/transfer-to-bank';
  static const bankTransferSuccess = '/bank-transfer-success';
  static const transactionHistory = '/transactions-history';
  static const transactionDetails = '/transaction-details';
  static const userToUserTransactions = '/user-transactions';
  static const wallet = '/wallet';
}
