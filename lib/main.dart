import 'package:flutter/material.dart';
import 'package:mone_me/screens/home/notifier/home_notifier.dart';
import 'package:mone_me/screens/home/notifier/settings_notifier.dart';
import 'package:mone_me/screens/home/notifier/transactions_notifier.dart';
import 'package:mone_me/screens/home/notifier/user_to_user_notifier.dart';
import 'package:mone_me/screens/login/login_notifier/login_notifier.dart';
import 'package:mone_me/utils/routes.dart';
import 'package:mone_me/utils/route_generator.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => LoginNotifier()),
        ChangeNotifierProvider(create: (context) => HomeNotifier()),
        ChangeNotifierProvider(create: (context) => SettingsNotifier()),
        ChangeNotifierProvider(create: (context) => TransactionsNotifier()),
        ChangeNotifierProvider(create: (context) => UserToUserNotifier()),
      ],
      child: MaterialApp(
        title: appName,
        debugShowCheckedModeBanner: false,
        theme: ThemeData(primarySwatch: Colors.indigo),
        // home: const SplashScreen(),
        initialRoute: AppRoutes.splash,
        onGenerateRoute: RouteGenerator.generateRoute,
      ),
    );
  }
}
