import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mone_me/models/add_transaction_model.dart';
import 'package:mone_me/models/friend_transactions_response_model.dart';
import 'package:mone_me/screens/home/notifier/home_notifier.dart';
import 'package:mone_me/screens/home/notifier/transactions_notifier.dart';
import 'package:mone_me/screens/login/login_notifier/login_notifier.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/inner_pages_enum.dart';
import 'package:mone_me/utils/payment_source_enum.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/utils/transaction_message_enum.dart';
import 'package:mone_me/utils/transaction_type_enum.dart';
import 'package:mone_me/utils/validators.dart';
import 'package:mone_me/widgets/adaptable_text.dart';
import 'package:mone_me/widgets/custom_toast.dart';
import 'package:mone_me/widgets/loader.dart';
import 'package:mone_me/widgets/num_pad.dart';
import 'package:pinput/pinput.dart';
import 'package:provider/provider.dart';

final TextEditingController pinController = TextEditingController();
final _pinPutFocusNode = FocusNode();
final _pinPutKey = GlobalKey<FormState>();
final defaultPinTheme = PinTheme(
    width: 56,
    height: 60,
    margin: const EdgeInsets.fromLTRB(5, 0, 5, 0),
    textStyle: pinPutTextStyle,
    decoration: BoxDecoration(color: spinnerDropDown.withOpacity(0.5), borderRadius: payBorderRadius));

Widget addMoneyForSendingOtpWidget(BuildContext context) {
  pinController.clear();
  var profileDetails = Provider.of<TransactionsNotifier>(context, listen: true).friendsProfile;
  return Card(
      shadowColor: cardShadow,
      shape: const RoundedRectangleBorder(borderRadius: detailsBorderRadius),
      elevation: 5,
      child: Column(children: [header(context), profileDetail(profileDetails), otpField(context), Expanded(child: numPad())]));
}

Widget header(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(20, 25, 20, 5),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        IconButton(
            onPressed: () {
              Provider.of<HomeNotifier>(context, listen: false).setInnerPage(InnerPages.addMoneySend.name!);
              pinController.clear();
            },
            icon: const Icon(Icons.keyboard_backspace_rounded, color: buttonGradientEnd, size: 24)),
        IconButton(onPressed: () {}, icon: Image.asset("assets/images/menu.png", color: buttonGradientEnd, height: 21, width: 24))
      ],
    ),
  );
}

Widget profileDetail(UserProfileDetails? profileDetails) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ClipRRect(
          borderRadius: peopleProfileBorderRadius,
          child: Container(
            width: 75,
            height: 75,
            margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
            decoration: const BoxDecoration(borderRadius: peopleProfileBorderRadius, color: buttonGradientStart),
            child: Center(
                child: profileDetails == null
                    ? AdaptableText(appName[0].toUpperCase(), style: peopleNameListIconTextStyle)
                    : profileDetails.profilePicture == ""
                        ? AdaptableText(profileDetails.userName == "" ? appName[0].toUpperCase() : profileDetails.userName![0].toUpperCase(),
                            style: peopleNameListIconTextStyle)
                        : Image.network(profileDetails.profilePicture!, fit: BoxFit.fill,)),
          ),
        ),
        Center(
            child: AdaptableText(
                profileDetails == null
                    ? appName.toUpperCase()
                    : profileDetails.userName == ""
                        ? appName.toUpperCase()
                        : profileDetails.userName!.toUpperCase(),
                style: profileNameTextStyle,
                textMaxLines: 1)),
        Center(
            child: AdaptableText(
                profileDetails == null
                    ? phoneNumber
                    : profileDetails.phoneNumber == ""
                        ? phoneNumber
                        : profileDetails.phoneNumber!,
                style: profilePhoneTextStyle,
                textMaxLines: 1)),
      ],
    ),
  );
}

Widget otpField(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
    child: Column(
      children: [
        Align(
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
              child: AdaptableText(enterPin, style: signInContinueTextStyle, textMaxLines: 1),
            )),
        Align(
          alignment: Alignment.center,
          child: Form(
            key: _pinPutKey,
            child: Pinput(
              animationCurve: Curves.linear,
              animationDuration: const Duration(milliseconds: 160),
              pinAnimationType: PinAnimationType.slide,
              separator: const SizedBox(width: 1.0),
              showCursor: false,
              length: 4,
              closeKeyboardWhenCompleted: true,
              focusNode: _pinPutFocusNode,
              controller: pinController,
              defaultPinTheme: defaultPinTheme,
              focusedPinTheme: defaultPinTheme.copyWith(height: 68, width: 64),
              errorPinTheme:
                  defaultPinTheme.copyWith(decoration: BoxDecoration(color: spinnerDropDown.withOpacity(0.5), borderRadius: payBorderRadius)),
              inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.allow(RegExp(r'[0-9]')), LengthLimitingTextInputFormatter(4)],
              validator: pinPut4DigitPValidator,
              pinputAutovalidateMode: PinputAutovalidateMode.onSubmit,
              obscuringCharacter: '*',
              obscureText: true,
              onChanged: (value) {},
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 5),
          child: ClipRRect(
            borderRadius: defaultBorderRadius,
            child: Container(
              decoration: const BoxDecoration(gradient: buttonGradient),
              child: TextButton(
                onPressed: () async {
                  if (_pinPutKey.currentState!.validate()) {
                    showLoadingDialog(context);
                    var transactionsViewModel = Provider.of<TransactionsNotifier>(context, listen: false);
                    var loginViewModel = Provider.of<LoginNotifier>(context, listen: false);
                    var homeViewModel = Provider.of<HomeNotifier>(context, listen: false);
                    var userId = loginViewModel.userId!;
                    var addTransactionModel = AddTransactionModel(
                        senderId: userId,
                        receiverId: transactionsViewModel.friendsProfile!.id!,
                        amount: transactionsViewModel.getTransactionAmount,
                        message: "",
                        paymentSource: PaymentSource.user.name,
                        transactionType: TransactionTypes.debit.name,
                        messageTransaction: TransactionMessage.transaction.name,
                        transferSecurePin: pinController.text.trim());
                    debugPrint(addTransactionModel.toJson().toString());
                    await transactionsViewModel.addTransactionAPI(addTransactionModel).then((value) async {
                      if (transactionsViewModel.addTransactionResponse != null) {
                        if (transactionsViewModel.addTransactionResponse!.responseStatus == 1) {
                          showToast(transactionsViewModel.addTransactionResponse!.result!);
                          await transactionsViewModel
                              .getSingleTransactionAPI(userId, transactionsViewModel.addTransactionResponse!.transactionId!)
                              .then((value) async {
                            Navigator.of(context).pop();
                            await homeViewModel.getDashboardAPI(userId, DateTime.now().timeZoneName);
                            await homeViewModel.getAllFriendsAPI(userId);
                            await transactionsViewModel.geAllTransactionsAPI(userId, "", "");
                            await transactionsViewModel.getAllTransferredTransactionsAPI(userId, "", "");
                            await transactionsViewModel.getAllReceivedTransactionsAPI(userId, "", "");
                            homeViewModel.setInnerPage(InnerPages.transactionDetails.name!);
                          });
                        } else {
                          Navigator.of(context).pop();
                          showToast(transactionsViewModel.addTransactionResponse!.result!);
                        }
                      } else {
                        Navigator.of(context).pop();
                        showToast(tryAgain);
                      }
                    });
                  }
                },
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(30, 15, 30, 15), child: AdaptableText(pay, style: submitButtonTextStyle, textMaxLines: 1)),
              ),
            ),
          ),
        ),
      ],
    ),
  );
}

Widget numPad() {
  return Padding(
    padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
    child: NumPad(
      buttonSize: 60,
      buttonColor: numPadBackground,
      iconColor: nameText,
      controller: pinController,
      delete: () {
        pinController.text = pinController.text.substring(0, pinController.text.length - 1);
      },
      onSubmit: () {},
    ),
  );
}
