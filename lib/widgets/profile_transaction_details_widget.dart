import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mone_me/models/friend_transactions_response_model.dart';
import 'package:mone_me/screens/home/notifier/home_notifier.dart';
import 'package:mone_me/screens/home/notifier/transactions_notifier.dart';
import 'package:mone_me/screens/login/login_notifier/login_notifier.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/date_time_formats.dart';
import 'package:mone_me/utils/inner_pages_enum.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/utils/transaction_type_enum.dart';
import 'package:mone_me/widgets/adaptable_text.dart';
import 'package:mone_me/widgets/custom_toast.dart';
import 'package:mone_me/widgets/loader.dart';
import 'package:provider/provider.dart';

Widget profileTransactionDetailsWidget(BuildContext context) {
  var allTransactions = Provider.of<TransactionsNotifier>(context, listen: true).friendsTransferredTransactionsList();
  var profileDetails = Provider.of<TransactionsNotifier>(context, listen: true).friendsProfile;
  return Card(
    shadowColor: cardShadow,
    shape: const RoundedRectangleBorder(borderRadius: detailsBorderRadius),
    elevation: 5,
    child: Column(children: [
      header(context),
      Expanded(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [profileDetail(profileDetails), transactionsList(allTransactions)],
          ),
        ),
      ),
      actions(context)
    ]),
  );
}

Widget header(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(20, 25, 20, 5),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        IconButton(
            onPressed: () {
              Provider.of<HomeNotifier>(context, listen: false).setInnerPage(InnerPages.transactionHistory.name!);
            },
            icon: const Icon(Icons.keyboard_backspace_rounded, color: buttonGradientEnd, size: 24)),
        IconButton(onPressed: () {}, icon: Image.asset("assets/images/menu.png", color: buttonGradientEnd, height: 21, width: 24))
      ],
    ),
  );
}

Widget profileDetail(UserProfileDetails? profileDetails) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ClipRRect(
          borderRadius: peopleProfileBorderRadius,
          child: Container(
            width: 75,
            height: 75,
            margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
            decoration: const BoxDecoration(borderRadius: peopleProfileBorderRadius, color: buttonGradientStart),
            child: profileDetails == null
                ? Center(child: AdaptableText(appName[0].toUpperCase(), style: peopleNameListIconTextStyle))
                : profileDetails.profilePicture == ""
                    ? Center(
                      child: AdaptableText(profileDetails.userName == "" ? appName[0].toUpperCase() : profileDetails.userName![0].toUpperCase(),
                          style: peopleNameListIconTextStyle),
                    )
                    : Image.network(profileDetails.profilePicture!, fit: BoxFit.fill,),
          ),
        ),
        Center(
            child: AdaptableText(
                profileDetails == null
                    ? appName.toUpperCase()
                    : profileDetails.userName == ""
                        ? appName.toUpperCase()
                        : profileDetails.userName!.toUpperCase(),
                style: profileNameTextStyle,
                textMaxLines: 1)),
        Center(
            child: AdaptableText(
                profileDetails == null
                    ? phoneNumber
                    : profileDetails.phoneNumber == ""
                        ? phoneNumber
                        : profileDetails.phoneNumber!,
                style: profilePhoneTextStyle,
                textMaxLines: 1)),
      ],
    ),
  );
}

Widget transactionsList(List<UserTransactionsList?> list) {
  return Flexible(
    fit: FlexFit.loose,
    child: Padding(
      padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
      child: list.isNotEmpty
          ? ListView.builder(
              controller: ScrollController(),
              shrinkWrap: true,
              itemCount: list.length,
              itemBuilder: (context, index) {
                return dateBasedItems(list[index]);
              })
          : const SizedBox(),
    ),
  );
}

Widget dateBasedItems(UserTransactionsList? transactionsList) {
  return Column(
    children: [
      Padding(
        padding: const EdgeInsets.fromLTRB(8, 15, 8, 10),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Expanded(child: Divider(color: dottedLine, thickness: 1)),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: AdaptableText(dateFormatMDY(transactionsList!.date!), style: paymentDateSeparatorTextStyle, textMaxLines: 1),
              ),
            ),
            const Expanded(child: Divider(color: dottedLine, thickness: 1)),
          ],
        ),
      ),
      ListView.builder(
          controller: ScrollController(),
          shrinkWrap: true,
          itemCount: transactionsList.transactionsList!.length,
          itemBuilder: (context, index) {
            return transactionsList.transactionsList![index].transactionType == TransactionTypes.debit.name
                ? transferredWidget(context, transactionsList.transactionsList![index])
                : receivedWidget(context, transactionsList.transactionsList![index]);
          })
    ],
  );
}

Widget receivedWidget(BuildContext context, TransactionsList? list) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(0, 0, 0, 2),
    child: Row(
      children: [
        Expanded(
          child: Card(
            shadowColor: cardShadow,
            margin: const EdgeInsets.fromLTRB(5, 0, 5, 0),
            shape: const RoundedRectangleBorder(borderRadius: paymentTransactionsRadius),
            elevation: 3,
            color: CupertinoColors.white,
            child: InkWell(
              borderRadius: paymentTransactionsRadius,
              onTap: () async {
                showLoadingDialog(context);
                await Provider.of<TransactionsNotifier>(context, listen: false)
                    .getSingleTransactionAPI(Provider.of<LoginNotifier>(context, listen: false).userId!, list!.id!)
                    .then((value) {
                  Navigator.of(context).pop();
                  if (value != null) {
                    if (value.responseStatus == 1) {
                      Provider.of<HomeNotifier>(context, listen: false).setInnerPage(InnerPages.transactionDetails.name!);
                    } else {
                      showToast(value.result!);
                    }
                  } else {
                    showToast(tryAgain);
                  }
                });
              },
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Align(
                        alignment: Alignment.centerRight, child: Icon(Icons.arrow_right_alt_rounded, color: receivedIconBackground, size: 20)),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 5, 0, 3),
                      child: AdaptableText("\u0024${list!.amount}", style: paymentAmountTextStyle, textMaxLines: 1),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 3, 0, 5),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Image.asset("assets/images/paid_icon.png", height: 15, width: 15),
                          Flexible(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                              child: AdaptableText("$youArePaid ${dateFormatMD(list.succeededOn!)}", style: paymentDateTextStyle, textMaxLines: 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        const Expanded(child: SizedBox())
      ],
    ),
  );
}

Widget transferredWidget(BuildContext context, TransactionsList? list) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(0, 0, 0, 2),
    child: Row(
      children: [
        const Expanded(child: SizedBox()),
        Expanded(
          child: Card(
            shadowColor: cardShadow,
            margin: const EdgeInsets.fromLTRB(5, 0, 5, 0),
            shape: const RoundedRectangleBorder(borderRadius: paymentTransactionsRadius),
            elevation: 3,
            color: CupertinoColors.white,
            child: InkWell(
              borderRadius: paymentTransactionsRadius,
              onTap: () async {
                showLoadingDialog(context);
                await Provider.of<TransactionsNotifier>(context, listen: false)
                    .getSingleTransactionAPI(Provider.of<LoginNotifier>(context, listen: false).userId!, list!.id!)
                    .then((value) {
                  Navigator.of(context).pop();
                  if (value != null) {
                    if (value.responseStatus == 1) {
                      Provider.of<HomeNotifier>(context, listen: false).setInnerPage(InnerPages.transactionDetails.name!);
                    } else {
                      showToast(value.result!);
                    }
                  } else {
                    showToast(tryAgain);
                  }
                });
              },
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Align(
                        alignment: Alignment.centerRight, child: Icon(Icons.arrow_right_alt_rounded, color: receivedIconBackground, size: 20)),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 5, 0, 3),
                      child: AdaptableText("\u0024${list!.amount}", style: paymentAmountTextStyle, textMaxLines: 1),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 3, 0, 5),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Image.asset("assets/images/paid_icon.png", height: 15, width: 15),
                          Flexible(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                              child: AdaptableText("$youPaid ${dateFormatMD(list.succeededOn!)}", style: paymentDateTextStyle, textMaxLines: 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    ),
  );
}

Widget actions(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(20, 5, 20, 10),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: [
        Flexible(
          child: Container(
            decoration: const BoxDecoration(color: transferredIconBackground, borderRadius: payBorderRadius),
            child: TextButton(
              onPressed: () {
                Provider.of<HomeNotifier>(context, listen: false).setInnerPage(InnerPages.addMoneySend.name!);
              },
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: AdaptableText(pay, style: paymentButtonsTextStyle, textMaxLines: 1),
              ),
            ),
          ),
        ),
        Flexible(
          child: Container(
            decoration: const BoxDecoration(color: receivedIconBackground, borderRadius: payBorderRadius),
            child: TextButton(
              onPressed: () {},
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: AdaptableText(request, style: paymentButtonsTextStyle, textMaxLines: 1),
              ),
            ),
          ),
        )
      ],
    ),
  );
}
