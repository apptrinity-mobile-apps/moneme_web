import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mone_me/screens/home/notifier/home_notifier.dart';
import 'package:mone_me/screens/home/notifier/settings_notifier.dart';
import 'package:mone_me/screens/login/login_notifier/login_notifier.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/screen_config.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/utils/validators.dart';
import 'package:mone_me/widgets/custom_toast.dart';
import 'package:pinput/pinput.dart';
import 'package:provider/provider.dart';

createSecurePinDialog(BuildContext context) {
  final TextEditingController pinController = TextEditingController();
  final _pinPutFocusNode = FocusNode();
  final _pinPutKey = GlobalKey<FormState>();
  final defaultPinTheme = PinTheme(
      height: 50,
      width: 48,
      margin: const EdgeInsets.fromLTRB(5, 0, 5, 0),
      textStyle: pinPutTextStyle,
      decoration: BoxDecoration(color: spinnerDropDown.withOpacity(0.5), borderRadius: payBorderRadius));
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(securePin, style: logoutHeaderTextStyle),
          insetPadding: EdgeInsets.zero,
          titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
          contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
          actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
          content: Builder(builder: (context) {
            return SizedBox(
              width: ScreenConfig.width(context) / 3.5,
              child: Form(
                key: _pinPutKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                      child: Text(createSecurePin, style: logoutContentHeaderTextStyle, maxLines: 1),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                      child: Text(enterPin, style: logoutButtonHeaderTextStyle, maxLines: 1),
                    ),
                    Pinput(
                      animationCurve: Curves.linear,
                      animationDuration: const Duration(milliseconds: 160),
                      pinAnimationType: PinAnimationType.slide,
                      separator: const SizedBox(width: 1.0),
                      showCursor: false,
                      length: 4,
                      closeKeyboardWhenCompleted: true,
                      focusNode: _pinPutFocusNode,
                      controller: pinController,
                      defaultPinTheme: defaultPinTheme,
                      focusedPinTheme: defaultPinTheme.copyWith(height: 52, width: 50),
                      errorPinTheme:
                          defaultPinTheme.copyWith(decoration: BoxDecoration(color: spinnerDropDown.withOpacity(0.5), borderRadius: payBorderRadius)),
                      inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.allow(RegExp(r'[0-9]')), LengthLimitingTextInputFormatter(4)],
                      validator: pinPut4DigitPValidator,
                      pinputAutovalidateMode: PinputAutovalidateMode.onSubmit,
                      obscuringCharacter: '*',
                      obscureText: true,
                      onChanged: (value) {},
                    ),
                  ],
                ),
              ),
            );
          }),
          actions: [
            ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: TextButton(
                  onPressed: () async {
                    if (_pinPutKey.currentState!.validate()) {
                      await Provider.of<SettingsNotifier>(context, listen: false)
                          .createSecurePinAPI(Provider.of<LoginNotifier>(context, listen: false).userId!, pinController.text.trim())
                          .then((value) async {
                        if (Provider.of<SettingsNotifier>(context, listen: false).createPinResponseModel != null) {
                          if (Provider.of<SettingsNotifier>(context, listen: false).createPinResponseModel!.responseStatus == 1) {
                            await Provider.of<HomeNotifier>(context, listen: false)
                                .getDashboardAPI(Provider.of<LoginNotifier>(context, listen: false).userId!, DateTime.now().timeZoneName);
                            showToast(Provider.of<SettingsNotifier>(context, listen: false).createPinResponseModel!.result!);
                            Navigator.of(context).pop();
                          } else {
                            showToast(Provider.of<SettingsNotifier>(context, listen: false).createPinResponseModel!.result!);
                          }
                        }
                      });
                    }
                  },
                  child: Text(save, style: logoutButtonHeaderTextStyle)),
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(cancel, style: logoutButtonHeaderTextStyle)),
            )
          ],
        );
      });
}
