import 'package:flutter/material.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/screen_config.dart';
import 'package:shimmer/shimmer.dart';

Widget gridLoadingShimmer() {
  return SizedBox(
    width: double.infinity,
    child: Shimmer.fromColors(
      baseColor: Colors.white,
      highlightColor: Colors.grey.shade300,
      child: GridView.builder(
          controller: ScrollController(),
          physics: const ScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemCount: 7,
          gridDelegate:
              const SliverGridDelegateWithMaxCrossAxisExtent(childAspectRatio: 0.9, mainAxisSpacing: 4, crossAxisSpacing: 4, maxCrossAxisExtent: 125),
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: [
                Expanded(
                  flex: 2,
                  child: ClipRRect(
                    borderRadius: peopleProfileBorderRadius,
                    child: Container(
                      width: ScreenConfig.width(context),
                      height: ScreenConfig.height(context),
                      margin: const EdgeInsets.fromLTRB(15, 2, 15, 2),
                      decoration: const BoxDecoration(
                        borderRadius: peopleProfileBorderRadius,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                    child: Container(
                      width: ScreenConfig.width(context),
                      height: 8,
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            );
          }),
    ),
  );
}
