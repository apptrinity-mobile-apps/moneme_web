import 'package:flutter/cupertino.dart';

Widget homeIconButton() {
  return Center(
    child: Padding(
      padding: const EdgeInsets.all(5),
      child: Image.asset("assets/images/app_logo_horizontal.png", height: 70),
    ),
  );
}