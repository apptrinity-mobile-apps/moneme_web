import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mone_me/models/countries_response_model.dart';
import 'package:mone_me/utils/country_picker_typedef.dart';

class CountryPickerDropdown extends StatefulWidget {
  const CountryPickerDropdown({Key? key,
    required this.onValuePicked,
    required this.countryList,
    this.itemFilter,
    this.sortComparator,
    this.priorityList,
    this.itemBuilder,
    this.initialValue,
    this.isExpanded = false,
    this.itemHeight = kMinInteractiveDimension,
    this.selectedItemBuilder,
    this.isDense = false,
    this.underline,
    this.dropdownColor,
    this.onTap,
    this.icon,
    this.iconDisabledColor,
    this.iconEnabledColor,
    this.iconSize = 24.0,
    this.hint,
    this.disabledHint,
    this.isFirstDefaultIfInitialValueNotProvided = true,
  }) : super(key: key);

  /// Filters the available country list
  final ItemFilter? itemFilter;

  /// [Comparator] to be used in sort of country list
  final Comparator<CountriesList>? sortComparator;

  /// List of countries that are placed on top
  final List<CountriesList>? priorityList;
  final List<CountriesList>? countryList;

  ///This function will be called to build the child of DropdownMenuItem
  ///If it is not provided, default one will be used which displays
  ///flag image, isoCode and phoneCode in a row.
  ///Check _buildDefaultMenuItem method for details.
  final ItemBuilder? itemBuilder;

  ///It should be one of the ISO ALPHA-2 Code that is provided
  ///in countryList map of countries.dart file.
  final String? initialValue;

  ///This function will be called whenever a Country item is selected.
  final ValueChanged<CountriesList> onValuePicked;

  /// Boolean property to enabled/disable expanded property of DropdownButton
  final bool isExpanded;

  /// See [itemHeight] of [DropdownButton]
  final double? itemHeight;

  /// See [isDense] of [DropdownButton]
  final bool isDense;

  /// See [underline] of [DropdownButton]
  final Widget? underline;

  /// Selected country widget builder to display. See [selectedItemBuilder] of [DropdownButton]
  final ItemBuilder? selectedItemBuilder;

  /// See [dropdownColor] of [DropdownButton]
  final Color? dropdownColor;

  /// See [onTap] of [DropdownButton]
  final VoidCallback? onTap;

  /// See [icon] of [DropdownButton]
  final Widget? icon;

  /// See [iconDisabledColor] of [DropdownButton]
  final Color? iconDisabledColor;

  /// See [iconEnabledColor] of [DropdownButton]
  final Color? iconEnabledColor;

  /// See [iconSize] of [DropdownButton]
  final double iconSize;

  /// See [hint] of [DropdownButton]
  final Widget? hint;

  /// See [disabledHint] of [DropdownButton]
  final Widget? disabledHint;

  /// Set first item in the country list as selected initially
  /// if initialValue is not provided
  final bool isFirstDefaultIfInitialValueNotProvided;

  @override
  _CountryPickerDropdownState createState() => _CountryPickerDropdownState();
}

class _CountryPickerDropdownState extends State<CountryPickerDropdown> {
  late List<CountriesList> _countries;
  late CountriesList _selectedCountry;

  @override
  void initState() {
    _countries = widget.countryList!.where(widget.itemFilter ?? acceptAllCountries).toList();

    if (widget.sortComparator != null) {
      _countries.sort(widget.sortComparator);
    }

    /*if (widget.priorityList != null) {
      widget.priorityList!.forEach((CountriesList country) => _countries.removeWhere((CountriesList c) => country.isoCode == c.isoCode));
      _countries.insertAll(0, widget.priorityList!);
    }*/

    if (widget.initialValue != null) {
      try {
        _selectedCountry = _countries.firstWhere(
          (country) => country.name == widget.initialValue!.toUpperCase(),
        );
      } catch (error) {
        throw Exception("The initialValue provided is not a supported iso code!");
      }
    } else {
      if (widget.isFirstDefaultIfInitialValueNotProvided && _countries.isNotEmpty) {
        _selectedCountry = _countries[0];
      }
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<DropdownMenuItem<CountriesList>> items = _countries
        .map((country) => DropdownMenuItem<CountriesList>(
            value: country, child: widget.itemBuilder != null ? widget.itemBuilder!(country) : _buildDefaultMenuItem(country)))
        .toList();

    return DropdownButton<CountriesList>(
      hint: widget.hint,
      disabledHint: widget.disabledHint,
      onTap: widget.onTap,
      icon: widget.icon,
      iconSize: widget.iconSize,
      iconDisabledColor: widget.iconDisabledColor,
      iconEnabledColor: widget.iconEnabledColor,
      dropdownColor: widget.dropdownColor,
      underline: widget.underline ?? const SizedBox(),
      isDense: widget.isDense,
      isExpanded: widget.isExpanded,
      onChanged: (value) {
        if (value != null) {
          setState(() {
            _selectedCountry = value;
            widget.onValuePicked(value);
          });
        }
      },
      items: items,
      value: _selectedCountry,
      itemHeight: widget.itemHeight,
      selectedItemBuilder: widget.selectedItemBuilder != null
          ? (context) {
              return _countries.map((c) => widget.selectedItemBuilder!(c)).toList();
            }
          : null,
    );
  }

  Widget _buildDefaultMenuItem(CountriesList country) {
    return Row(
      children: <Widget>[
        ClipRRect(
          borderRadius: BorderRadius.circular(5),
          child: Image.network(country.countryFlag!, height: 30, width: 30, fit: BoxFit.fill),
        )
      ],
    );
  }
}
