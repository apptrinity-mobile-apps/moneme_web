import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:mone_me/models/saved_cards_response_model.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/widgets/adaptable_text.dart';

class CardRadioListTile<T> extends StatelessWidget {
  final int position;
  final T value;
  final T groupValue;
  final ValueChanged<T?> onChanged;

  const CardRadioListTile({
    Key? key,
    required this.value,
    required this.position,
    required this.groupValue,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var alternate = position % 2 == 0;
    var card = value as CreditCardsList;
    return InkWell(
      onTap: () => onChanged(value),
      child: Container(
        padding: const EdgeInsets.all(15),
        color: alternate ? headerColor : CupertinoColors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _customRadioButton,
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(5, 0, 5, 3),
                    child: AdaptableText("Credit card", style: cardNameTextStyle, textMaxLines: 1),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(5, 0, 5, 3),
                    child: AdaptableText(card.cardDetails!.cardNumber!, style: cardNumberTextStyle, textMaxLines: 1),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget get _customRadioButton {
    return Container(
      padding: const EdgeInsets.fromLTRB(0, 0, 5, 5),
      child: Radio(value: value, groupValue: groupValue, onChanged: onChanged, toggleable: true),
    );
  }
}

class MyRadioListTile<T> extends StatelessWidget {
  final int position;
  final T value;
  final T groupValue;
  final ValueChanged<T?> onChanged;

  const MyRadioListTile({
    Key? key,
    required this.value,
    required this.position,
    required this.groupValue,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var alternate = position % 2 == 0;
    return InkWell(
      onTap: () => onChanged(value),
      child: Container(
        padding: const EdgeInsets.all(15),
        color: alternate ? headerColor : CupertinoColors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _customRadioButton,
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(5, 0, 5, 3),
                    child: AdaptableText("card name", style: cardNameTextStyle, textMaxLines: 1),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(5, 0, 5, 3),
                    child: AdaptableText("card number", style: cardNumberTextStyle, textMaxLines: 1),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget get _customRadioButton {
    return Container(
      padding: const EdgeInsets.fromLTRB(0, 0, 5, 5),
      child: Radio(value: value, groupValue: groupValue, onChanged: onChanged, toggleable: true),
    );
  }
}
