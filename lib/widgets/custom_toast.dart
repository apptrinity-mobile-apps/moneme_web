import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void showToast(String message) {
  Fluttertoast.showToast(
      msg: message,
      gravity: ToastGravity.TOP,
      toastLength: Toast.LENGTH_LONG,
      webShowClose: false,
      webPosition: "center",
      timeInSecForIosWeb: 3,
      textColor: Colors.white,
      // webBgColor: "linear-gradient(to right, #6D6D6D, #6D6D6D)");
      webBgColor: "linear-gradient(to right, #000000, #000000)");
}
