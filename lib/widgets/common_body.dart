import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mone_me/models/friends_response_model.dart';
import 'package:mone_me/models/user_to_user_model.dart';
import 'package:mone_me/screens/home/notifier/home_notifier.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/routes.dart';
import 'package:mone_me/utils/screen_config.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/widgets/adaptable_text.dart';
import 'package:mone_me/widgets/amount_received_widget.dart';
import 'package:mone_me/widgets/amount_total_widget.dart';
import 'package:mone_me/widgets/amount_transferred_widget.dart';
import 'package:mone_me/widgets/grid_loading_shimmer.dart';
import 'package:provider/provider.dart';

class CommonBody extends StatefulWidget {
  final Function()? onSelectedChanged;
  final int? selectedMenu;

  const CommonBody({Key? key, this.onSelectedChanged, this.selectedMenu}) : super(key: key);

  @override
  State<CommonBody> createState() => _CommonBodyState();
}

class _CommonBodyState extends State<CommonBody> with AutomaticKeepAliveClientMixin, SingleTickerProviderStateMixin {
  List<UniqueFriendsList?> utilitiesList = [];
  int selectedPerson = -1, selectedBusiness = -1, selectedUtility = -1, selectedTab = 0;
  TextEditingController searchController = TextEditingController();
  bool expandSearchBox = false;
  TabController? tabController;

  @override
  void initState() {
    utilitiesList.add(UniqueFriendsList(id: "", name: "Car Parking", phoneNumber: "", profilePicture: "images/car_parking.png"));
    utilitiesList.add(UniqueFriendsList(id: "", name: "Car Parking", phoneNumber: "", profilePicture: "images/car_parking.png"));
    tabController = TabController(length: 3, vsync: this);
    if (widget.selectedMenu != null) {
      tabController!.animateTo(widget.selectedMenu!);
    }
    tabController!.addListener(() {
      setState(() {
        selectedTab = tabController!.index;
      });
    });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    debugPrint("state-- didChangeDependencies");
    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(covariant CommonBody oldWidget) {
    debugPrint("state-- didUpdateWidget");
    super.didUpdateWidget(oldWidget);
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return commonBody(context);
  }

  Widget commonBody(BuildContext context) {
    return Expanded(
      flex: 2,
      child: Container(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        margin: const EdgeInsets.only(right: 3),
        child: Column(
          children: [
            Expanded(
              child: Container(
                height: ScreenConfig.height(context),
                width: ScreenConfig.width(context),
                margin: const EdgeInsets.only(bottom: 3),
                child: Card(
                  shadowColor: cardShadow,
                  shape: const RoundedRectangleBorder(borderRadius: amountsBorderRadius),
                  elevation: 3,
                  child: totalAmountsFields(context),
                ),
              ),
            ),
            Expanded(
              flex: 5,
              child: Container(
                height: ScreenConfig.height(context),
                width: ScreenConfig.width(context),
                margin: const EdgeInsets.only(top: 3),
                child: Card(
                  shadowColor: cardShadow,
                  // shape: const RoundedRectangleBorder(borderRadius: peopleBorderRadius),
                  elevation: 3,
                  child: body(context),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget totalAmountsFields(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(0),
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: ScreenConfig.height(context),
              width: ScreenConfig.width(context),
              margin: const EdgeInsets.fromLTRB(10, 10, 0, 10),
              padding: const EdgeInsets.all(5),
              decoration: const BoxDecoration(borderRadius: transferredAmountBorderRadius, color: CupertinoColors.white),
              alignment: Alignment.center,
              child: amountTransferredWidget(context),
            ),
          ),
          Expanded(
            child: Container(
              height: ScreenConfig.height(context),
              width: ScreenConfig.width(context),
              margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(0), color: CupertinoColors.white),
              alignment: Alignment.center,
              child: amountReceivedWidget(context),
            ),
          ),
          Expanded(
            child: Container(
              height: ScreenConfig.height(context),
              width: ScreenConfig.width(context),
              margin: const EdgeInsets.fromLTRB(0, 10, 10, 10),
              padding: const EdgeInsets.all(5),
              decoration: const BoxDecoration(borderRadius: totalAmountBorderRadius, color: totalAmountBackground),
              alignment: Alignment.center,
              child: amountTotalWidget(context),
            ),
          ),
        ],
      ),
    );
  }

  Widget body(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        margin: const EdgeInsets.fromLTRB(15, 10, 15, 10),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
          child: Scaffold(
            backgroundColor: CupertinoColors.white,
            appBar: PreferredSize(
              preferredSize: const Size.fromHeight(kToolbarHeight - 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: Container(
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(50), color: totalAmountBackground),
                      padding: const EdgeInsets.only(left: 15, right: 15),
                      alignment: Alignment.centerLeft,
                      child: Center(
                        child: Material(
                          type: MaterialType.transparency,
                          child: TabBar(
                            controller: tabController,
                            padding: const EdgeInsets.only(right: 15),
                            isScrollable: true,
                            indicatorSize: TabBarIndicatorSize.label,
                            indicatorColor: transferredIconBackground,
                            labelStyle: const TextStyle(
                                letterSpacing: 1.38, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: defaultBackground, fontSize: 16),
                            unselectedLabelStyle: const TextStyle(
                                letterSpacing: 1.38, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: tabsHeaderColor, fontSize: 16),
                            labelColor: defaultBackground,
                            unselectedLabelColor: tabsHeaderColor,
                            labelPadding: const EdgeInsets.only(left: 20, right: 20),
                            tabs: const <Widget>[
                              Tab(text: people),
                              Tab(text: business),
                              Tab(text: utilities),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Visibility(
                          visible: expandSearchBox,
                          child: Expanded(
                            child: ConstrainedBox(
                              constraints: const BoxConstraints(minWidth: 200, maxWidth: 300),
                              child: Container(
                                margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                                child: TextFormField(
                                  controller: searchController,
                                  decoration: InputDecoration(
                                    fillColor: CupertinoColors.white,
                                    filled: true,
                                    border: searchBorder,
                                    isDense: true,
                                    enabledBorder: searchBorder,
                                    focusedBorder: searchBorder,
                                    errorBorder: searchErrorBorder,
                                    focusedErrorBorder: searchErrorBorder,
                                    contentPadding: searchFormFieldPadding,
                                    hintStyle: textFormFieldHintStyle,
                                    hintText: search,
                                    prefixIcon: const Icon(Icons.search, size: 25, color: defaultText),
                                  ),
                                  style: searchFormFieldStyle,
                                ),
                              ),
                            ),
                          ),
                          replacement: IconButton(
                              icon: const Icon(Icons.search_rounded, size: 24),
                              onPressed: () {
                                setState(() {
                                  expandSearchBox = true;
                                });
                              },
                              splashRadius: 16),
                        ),
                        TextButton(
                          onPressed: () {},
                          child: AdaptableText(showAll, style: showTextStyle, textMaxLines: 1),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            body: TabBarView(
              controller: tabController,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: peopleContainer(),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: businessesContainer(),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: utilitiesContainer(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget peopleContainer() {
    final viewModelHome = Provider.of<HomeNotifier>(context, listen: true);
    // final viewModelTransactions = Provider.of<TransactionsNotifier>(context, listen: true);
    // final userId = Provider.of<LoginNotifier>(context, listen: false).userId!;
    return viewModelHome.friendsListLoading
        ? gridLoadingShimmer()
        : GridView.builder(
            controller: ScrollController(),
            physics: const ScrollPhysics(),
            scrollDirection: Axis.vertical,
            itemCount: viewModelHome.allUniqueFriendsList().length,
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                childAspectRatio: 0.9, mainAxisSpacing: 4, crossAxisSpacing: 4, maxCrossAxisExtent: 125),
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                onTap: () async {
                  setState(() {
                    if (selectedPerson == -1 || selectedPerson != index) {
                      selectedPerson = index;
                    } else if (selectedPerson == index) {
                      selectedPerson = -1;
                    }
                  });
                  Navigator.pushNamed(context, AppRoutes.userToUserTransactions,
                      arguments: UserToUserModel(friendId: viewModelHome.allUniqueFriendsList()[index]!.id, tabSelected: selectedTab));
                },
                child: Column(
                  children: [
                    Expanded(
                      flex: 2,
                      child: ClipRRect(
                        borderRadius: peopleProfileBorderRadius,
                        child: Container(
                          width: ScreenConfig.width(context),
                          height: ScreenConfig.height(context),
                          margin: const EdgeInsets.fromLTRB(15, 2, 15, 2),
                          decoration: const BoxDecoration(
                            borderRadius: peopleProfileBorderRadius,
                            color: buttonGradientStart,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                offset: Offset(1, 1),
                                blurRadius: 1,
                              ),
                            ],
                          ),
                          child: viewModelHome.allUniqueFriendsList()[index]!.profilePicture! == ""
                              ? Center(
                                  child: AdaptableText(viewModelHome.allUniqueFriendsList()[index]!.name![0].toUpperCase(),
                                      style: peopleNameListIconTextStyle),
                                )
                              : ClipRRect(
                                  borderRadius: peopleProfileBorderRadius,
                                  child: Image.network(
                                    viewModelHome.allUniqueFriendsList()[index]!.profilePicture!,
                                    fit: BoxFit.fill,
                                    errorBuilder: (ctx, _, __) {
                                      return Center(
                                        child: AdaptableText(viewModelHome.allUniqueFriendsList()[index]!.name![0].toUpperCase(),
                                            style: peopleNameListIconTextStyle),
                                      );
                                    },
                                  ),
                                ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Tooltip(
                        message: viewModelHome.allUniqueFriendsList()[index]!.name!,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                          child: AdaptableText(
                            viewModelHome.allUniqueFriendsList()[index]!.name!,
                            style: selectedPerson == index
                                ? peopleNameListTextStyle.copyWith(fontWeight: FontWeight.w600, color: transferredIconBackground)
                                : peopleNameListTextStyle,
                            textMaxLines: 1,
                            textAlign: TextAlign.center,
                            textOverflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              );
            });
  }

  Widget businessesContainer() {
    final viewModelHome = Provider.of<HomeNotifier>(context, listen: true);
    // final viewModelTransactions = Provider.of<TransactionsNotifier>(context, listen: true);
    // final userId = Provider.of<LoginNotifier>(context, listen: false).userId!;
    return viewModelHome.friendsListLoading
        ? gridLoadingShimmer()
        : GridView.builder(
            controller: ScrollController(),
            physics: const ScrollPhysics(),
            scrollDirection: Axis.vertical,
            itemCount: viewModelHome.allUniqueBusinessesList().length,
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                childAspectRatio: 0.9, mainAxisSpacing: 4, crossAxisSpacing: 4, maxCrossAxisExtent: 125),
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                onTap: () async {
                  setState(() {
                    if (selectedBusiness == -1 || selectedBusiness != index) {
                      selectedBusiness = index;
                    } else if (selectedBusiness == index) {
                      selectedBusiness = -1;
                    }
                  });
                  Navigator.pushNamed(context, AppRoutes.userToUserTransactions,
                      arguments: UserToUserModel(friendId: viewModelHome.allUniqueBusinessesList()[index]!.id, tabSelected: selectedTab));
                },
                child: Column(
                  children: [
                    Expanded(
                      flex: 2,
                      child: ClipRRect(
                        borderRadius: peopleProfileBorderRadius,
                        child: Container(
                          width: ScreenConfig.width(context),
                          height: ScreenConfig.height(context),
                          margin: const EdgeInsets.fromLTRB(15, 2, 15, 2),
                          decoration: const BoxDecoration(
                            borderRadius: peopleProfileBorderRadius,
                            color: buttonGradientStart,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                offset: Offset(1, 1),
                                blurRadius: 1,
                              ),
                            ],
                          ),
                          child: viewModelHome.allUniqueBusinessesList()[index]!.profilePicture! == ""
                              ? Center(
                                  child: AdaptableText(viewModelHome.allUniqueBusinessesList()[index]!.name![0].toUpperCase(),
                                      style: peopleNameListIconTextStyle),
                                )
                              : ClipRRect(
                                  borderRadius: peopleProfileBorderRadius,
                                  child: Image.network(
                                    viewModelHome.allUniqueBusinessesList()[index]!.profilePicture!,
                                    fit: BoxFit.fill,
                                    errorBuilder: (ctx, _, __) {
                                      return Center(
                                        child: AdaptableText(viewModelHome.allUniqueBusinessesList()[index]!.name![0].toUpperCase(),
                                            style: peopleNameListIconTextStyle),
                                      );
                                    },
                                  ),
                                ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Tooltip(
                        message: viewModelHome.allUniqueBusinessesList()[index]!.name!,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                          child: AdaptableText(
                            viewModelHome.allUniqueBusinessesList()[index]!.name!,
                            style: selectedBusiness == index
                                ? peopleNameListTextStyle.copyWith(fontWeight: FontWeight.w600, color: transferredIconBackground)
                                : peopleNameListTextStyle,
                            textMaxLines: 1,
                            textAlign: TextAlign.center,
                            textOverflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              );
            });
  }

  Widget utilitiesContainer() {
    final viewModelHome = Provider.of<HomeNotifier>(context, listen: true);
    // final viewModelTransactions = Provider.of<TransactionsNotifier>(context, listen: true);
    // final userId = Provider.of<LoginNotifier>(context, listen: false).userId!;
    return viewModelHome.friendsListLoading
        ? gridLoadingShimmer()
        : GridView.builder(
            controller: ScrollController(),
            physics: const ScrollPhysics(),
            scrollDirection: Axis.vertical,
            itemCount: utilitiesList.length,
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                childAspectRatio: 0.9, mainAxisSpacing: 4, crossAxisSpacing: 4, maxCrossAxisExtent: 125),
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                onTap: () async {
                  setState(() {
                    if (selectedUtility == -1 || selectedUtility != index) {
                      selectedUtility = index;
                    } else if (selectedUtility == index) {
                      selectedUtility = -1;
                    }
                  });
                  // Navigator.pushNamed(context, AppRoutes.userToUserTransactions, arguments: viewModelHome.allUniqueBusinessesList()[index]!.id);
                  // TODO car parking screen
                },
                child: Column(
                  children: [
                    Expanded(
                      flex: 2,
                      child: ClipRRect(
                        borderRadius: peopleProfileBorderRadius,
                        child: Container(
                          width: ScreenConfig.width(context),
                          height: ScreenConfig.height(context),
                          margin: const EdgeInsets.fromLTRB(15, 2, 15, 2),
                          decoration: const BoxDecoration(
                            borderRadius: peopleProfileBorderRadius,
                            color: buttonGradientStart,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                offset: Offset(1, 1),
                                blurRadius: 1,
                              ),
                            ],
                          ),
                          child: utilitiesList[index]!.profilePicture! == ""
                              ? Center(
                                  child: AdaptableText(utilitiesList[index]!.name![0].toUpperCase(), style: peopleNameListIconTextStyle),
                                )
                              : ClipRRect(
                                  borderRadius: peopleProfileBorderRadius,
                                  child: Image.asset(
                                    utilitiesList[index]!.profilePicture!,
                                    fit: BoxFit.fill,
                                    errorBuilder: (ctx, _, __) {
                                      return Center(
                                        child: AdaptableText(utilitiesList[index]!.name![0].toUpperCase(), style: peopleNameListIconTextStyle),
                                      );
                                    },
                                  ),
                                ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Tooltip(
                        message: utilitiesList[index]!.name!,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                          child: AdaptableText(
                            utilitiesList[index]!.name!,
                            style: selectedUtility == index
                                ? utilitiesNameListTextStyle.copyWith(fontWeight: FontWeight.w600, color: transferredIconBackground)
                                : utilitiesNameListTextStyle,
                            textMaxLines: 2,
                            textAlign: TextAlign.center,
                            textOverflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              );
            });
  }
}
