import 'package:flutter/material.dart';
import 'package:mone_me/screens/login/login_notifier/login_notifier.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/routes.dart';
import 'package:mone_me/utils/session_manager.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/widgets/custom_toast.dart';
import 'package:mone_me/widgets/loader.dart';
import 'package:provider/provider.dart';

logoutDialog(BuildContext context) {
  showDialog(
      context: context,
      builder: (context) {
        SessionManager sessionManager = SessionManager();
        return AlertDialog(
          title: Text(logout, style: logoutHeaderTextStyle),
          titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
          contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
          actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),
          content: Text(confirmLogout, style: logoutContentHeaderTextStyle),
          actions: [
            ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: TextButton(
                onPressed: () {
                  showLoadingDialog(context);
                  final userId = Provider.of<LoginNotifier>(context, listen: false).userId!;
                  Provider.of<LoginNotifier>(context, listen: false).logoutAPI(userId).then((value) {
                    Navigator.pop(context);
                    if (Provider.of<LoginNotifier>(context, listen: false).logoutResponseModel != null) {
                      if (Provider.of<LoginNotifier>(context, listen: false).logoutResponseModel!.responseStatus == 1) {
                        showToast(Provider.of<LoginNotifier>(context, listen: false).logoutResponseModel!.result!);
                        sessionManager.clearSession();
                        Navigator.of(context).pop();
                        Navigator.pushNamedAndRemoveUntil(context, AppRoutes.splash, (route) => false);
                      } else {
                        showToast(Provider.of<LoginNotifier>(context, listen: false).logoutResponseModel!.result!);
                      }
                    } else {
                      showToast(tryAgain);
                    }
                  });
                },
                child: Text(ok, style: logoutButtonHeaderTextStyle),
              ),
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(cancel, style: logoutButtonHeaderTextStyle),
              ),
            )
          ],
        );
      });
}
