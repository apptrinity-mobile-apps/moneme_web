import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mone_me/screens/home/notifier/home_notifier.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/widgets/adaptable_text.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

Widget amountTransferredWidget(BuildContext context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Flexible(
        fit: FlexFit.loose,
        child: Container(
          height: 60,
          width: 65,
          margin: const EdgeInsets.fromLTRB(10, 0, 5, 0),
          child: const Card(
            shadowColor: cardShadow,
            color: transferredIconBackground,
            elevation: 3,
            shape: RoundedRectangleBorder(borderRadius: amountsIconBorderRadius),
            child: Center(
              child: FittedBox(
                child: Icon(Icons.arrow_upward_rounded, color: CupertinoColors.white, size: 35),
              ),
            ),
          ),
        ),
      ),
      Flexible(
        fit: FlexFit.loose,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flexible(
              child: Provider.of<HomeNotifier>(context, listen: true).balancesLoading
                  ? Shimmer.fromColors(
                      baseColor: Colors.white,
                      highlightColor: Colors.grey.shade300,
                      child: Container(
                        width: double.infinity,
                        height: 20,
                        color: Colors.white,
                      ),
                    )
                  : Padding(
                      padding: const EdgeInsets.fromLTRB(5, 0, 10, 0),
                      child: AdaptableText(
                          "\u0024${commaSeparatedAmount.format(Provider.of<HomeNotifier>(context, listen: true).getTransferredAmount!)}",
                          style: amountHeaderTextStyle,
                          textMaxLines: 1),
                    ),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(5, 0, 10, 0),
                child: AdaptableText(transferred, style: amountDescriptionHeaderTextStyle, textMaxLines: 1),
              ),
            )
          ],
        ),
      )
    ],
  );
}
