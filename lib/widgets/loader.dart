import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void showLoadingDialog(BuildContext context) {
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return loader();
      });
}

Widget loader() {
  return const Center(
    child: SizedBox(height: 30, width: 30, child: CircularProgressIndicator()),
  );
}
