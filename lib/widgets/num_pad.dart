import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';

// KeyPad widget
// This widget is reusable and its buttons are customizable (color, size)
class NumPad extends StatelessWidget {
  final double buttonSize;
  final Color buttonColor;
  final Color iconColor;
  final TextEditingController controller;
  final Function delete;
  final Function onSubmit;

  const NumPad({
    Key? key,
    this.buttonSize = 70,
    this.buttonColor = Colors.indigo,
    this.iconColor = Colors.amber,
    required this.delete,
    required this.onSubmit,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 30, right: 30),
      child: Column(
        children: [
          const SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            // implement the number keys (from 0 to 9) with the NumberButton widget
            // the NumberButton widget is defined in the bottom of this file
            children: [
              NumberButton(
                number: "1",
                size: buttonSize,
                color: buttonColor,
                controller: controller,
              ),
              NumberButton(
                number: "2",
                size: buttonSize,
                color: buttonColor,
                controller: controller,
              ),
              NumberButton(
                number: "3",
                size: buttonSize,
                color: buttonColor,
                controller: controller,
              ),
              ClipRRect(
                borderRadius: payBorderRadius,
                child: Container(
                  width: buttonSize,
                  height: buttonSize,
                  decoration: const BoxDecoration(borderRadius: payBorderRadius, color: numPadBackground),
                  child: TextButton(
                    onPressed: () => delete(),
                    child: Icon(
                      Icons.backspace_outlined,
                      color: iconColor,
                    ),
                    // iconSize: 40,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              NumberButton(
                number: "4",
                size: buttonSize,
                color: buttonColor,
                controller: controller,
              ),
              NumberButton(
                number: "5",
                size: buttonSize,
                color: buttonColor,
                controller: controller,
              ),
              NumberButton(
                number: "6",
                size: buttonSize,
                color: buttonColor,
                controller: controller,
              ),
              SizedBox(height: buttonSize, width: buttonSize),
            ],
          ),
          const SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              NumberButton(
                number: "7",
                size: buttonSize,
                color: buttonColor,
                controller: controller,
              ),
              NumberButton(
                number: "8",
                size: buttonSize,
                color: buttonColor,
                controller: controller,
              ),
              NumberButton(
                number: "9",
                size: buttonSize,
                color: buttonColor,
                controller: controller,
              ),
              SizedBox(height: buttonSize, width: buttonSize),
            ],
          ),
          const SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(height: buttonSize, width: buttonSize),
              NumberButton(
                number: "0",
                size: buttonSize,
                color: buttonColor,
                controller: controller,
              ),
              SizedBox(height: buttonSize, width: buttonSize),
              SizedBox(height: buttonSize, width: buttonSize)
            ],
          ),
        ],
      ),
    );
  }
}

// define NumberButton widget
// its shape is round
class NumberButton extends StatelessWidget {
  final String number;
  final double size;
  final Color color;
  final TextEditingController controller;

  const NumberButton({
    Key? key,
    required this.number,
    required this.size,
    required this.color,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: payBorderRadius,
      child: Container(
        width: size,
        height: size,
        decoration: const BoxDecoration(borderRadius: payBorderRadius, color: numPadBackground),
        child: TextButton(
          onPressed: () {
            if (controller.text.trim().length < 4) {
              controller.text += number;
            }
            debugPrint('text entered: ${controller.text}');
          },
          child: Center(
            child: Text(number, style: numPadTextStyle),
          ),
        ),
      ),
    );
  }
}
