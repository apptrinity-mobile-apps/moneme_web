import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:mone_me/utils/constants.dart';

void snackBarLight(BuildContext context, String message) {
  final snackBar = SnackBar(
      backgroundColor: Colors.white,
      behavior: SnackBarBehavior.floating,
      content: Text(message, style: snackBarDarkTextStyle, softWrap: true, maxLines: 1, overflow: TextOverflow.ellipsis));
  ScaffoldMessenger.of(context).clearSnackBars();
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

void snackBarDark(BuildContext context, String message) {
  final snackBar = SnackBar(
      backgroundColor: Colors.black,
      behavior: SnackBarBehavior.floating,
      content: Text(message, style: snackBarLightTextStyle, softWrap: true, maxLines: 1, overflow: TextOverflow.ellipsis));
  ScaffoldMessenger.of(context).clearSnackBars();
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}
