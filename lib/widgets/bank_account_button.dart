import 'package:flutter/cupertino.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/widgets/adaptable_text.dart';

Widget bankAccountButton() {
  return Padding(
    padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Image.asset("assets/images/bank_account_icon.png", height: 20, width: 20),
        Padding(
          padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
          child: AdaptableText(bankAccount, style: headerTextStyle, textMaxLines: 1),
        )
      ],
    ),
  );
}