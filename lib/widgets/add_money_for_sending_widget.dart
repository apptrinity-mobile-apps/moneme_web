import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mone_me/models/friend_transactions_response_model.dart';
import 'package:mone_me/screens/home/notifier/home_notifier.dart';
import 'package:mone_me/screens/home/notifier/transactions_notifier.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/inner_pages_enum.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/utils/validators.dart';
import 'package:mone_me/widgets/adaptable_text.dart';
import 'package:mone_me/widgets/create_secure_pin_dialog.dart';
import 'package:mone_me/widgets/custom_toast.dart';
import 'package:mone_me/widgets/num_pad_with_decimal.dart';
import 'package:provider/provider.dart';

final TextEditingController amountController = TextEditingController();
final _formKey = GlobalKey<FormState>();

Widget addMoneyForSendingWidget(BuildContext context) {
  amountController.clear();
  var profileDetails = Provider.of<TransactionsNotifier>(context, listen: true).friendsProfile;
  return Card(
      shadowColor: cardShadow,
      shape: const RoundedRectangleBorder(borderRadius: detailsBorderRadius),
      elevation: 5,
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [header(context), profileDetail(profileDetails), amountSend(context), Flexible(child: numPad())]));
}

Widget header(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(20, 25, 20, 5),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        IconButton(
            onPressed: () {
              Provider.of<HomeNotifier>(context, listen: false).setInnerPage(InnerPages.profileTransactions.name!);
              amountController.clear();
            },
            icon: const Icon(Icons.keyboard_backspace_rounded, color: buttonGradientEnd, size: 24)),
        IconButton(onPressed: () {}, icon: Image.asset("assets/images/menu.png", color: buttonGradientEnd, height: 21, width: 24))
      ],
    ),
  );
}

Widget profileDetail(UserProfileDetails? profileDetails) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ClipRRect(
          borderRadius: peopleProfileBorderRadius,
          child: Container(
            width: 75,
            height: 75,
            margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
            decoration: const BoxDecoration(borderRadius: peopleProfileBorderRadius, color: buttonGradientStart),
            child: Center(
                child: profileDetails == null
                    ? AdaptableText(appName[0].toUpperCase(), style: peopleNameListIconTextStyle)
                    : profileDetails.profilePicture == ""
                        ? AdaptableText(profileDetails.userName == "" ? appName[0].toUpperCase() : profileDetails.userName![0].toUpperCase(),
                            style: peopleNameListIconTextStyle)
                        : Image.network(profileDetails.profilePicture!, fit: BoxFit.fill,)),
          ),
        ),
        Center(
            child: AdaptableText(
                profileDetails == null
                    ? appName.toUpperCase()
                    : profileDetails.userName == ""
                        ? appName.toUpperCase()
                        : profileDetails.userName!.toUpperCase(),
                style: profileNameTextStyle,
                textMaxLines: 1)),
        Center(
            child: AdaptableText(
                profileDetails == null
                    ? phoneNumber
                    : profileDetails.phoneNumber == ""
                        ? phoneNumber
                        : profileDetails.phoneNumber!,
                style: profilePhoneTextStyle,
                textMaxLines: 1)),
      ],
    ),
  );
}

Widget amountSend(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
    child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: [
      Form(
          key: _formKey,
          child: TextFormField(
              controller: amountController,
              maxLines: 1,
              keyboardType: TextInputType.none,
              textAlign: TextAlign.center,
              showCursor: true,
              inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.allow(RegExp(r'^\d*\.?\d{0,2}'))],
              decoration: addMoneyDecoration,
              style: enteredAmountTextStyle,
              validator: emptyTextValidator)),
      ClipRRect(
        borderRadius: defaultBorderRadius,
        child: Container(
          decoration: const BoxDecoration(gradient: buttonGradient),
          child: TextButton(
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                if (Provider.of<HomeNotifier>(context, listen: false).hasSecurePin!) {
                  Provider.of<TransactionsNotifier>(context, listen: false).setTransactionAmount(amountController.text.trim());
                  Provider.of<HomeNotifier>(context, listen: false).setInnerPage(InnerPages.confirmOTPAfterAmount.name!);
                } else {
                  showToast("Secure pin is not yet set!");
                  createSecurePinDialog(context);
                }
              }
            },
            child: Padding(
                padding: const EdgeInsets.fromLTRB(30, 15, 30, 15), child: AdaptableText(sendMoney, style: submitButtonTextStyle, textMaxLines: 1)),
          ),
        ),
      ),
    ]),
  );
}

Widget numPad() {
  return Padding(
    padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
    child: NumPadDecimal(
      buttonSize: 60,
      buttonColor: numPadBackground,
      iconColor: nameText,
      controller: amountController,
      delete: () {
        amountController.text = amountController.text.substring(0, amountController.text.length - 1);
      },
      onSubmit: () {},
    ),
  );
}
