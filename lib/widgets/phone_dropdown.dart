import 'package:flutter/material.dart';
import 'package:mone_me/models/countries_response_model.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/widgets/adaptable_text.dart';

Widget buildDropdownItem(CountriesList country) {
  return SizedBox(
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        ClipRRect(
          borderRadius: BorderRadius.circular(5),
          // child: Image.network(country.countryFlag!, height: 30, width: 30, fit: BoxFit.fill),
          child: AdaptableText(country.phoneCode!, style: phoneDropdownStyle, textAlign: TextAlign.center),
        )
      ],
    ),
  );
}