import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mone_me/models/user_to_user_model.dart';
import 'package:mone_me/screens/home/notifier/home_notifier.dart';
import 'package:mone_me/screens/home/notifier/transactions_notifier.dart';
import 'package:mone_me/screens/login/login_notifier/login_notifier.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/inner_pages_enum.dart';
import 'package:mone_me/utils/routes.dart';
import 'package:mone_me/utils/screen_config.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/widgets/adaptable_text.dart';
import 'package:mone_me/widgets/bank_account_button.dart';
import 'package:mone_me/widgets/bank_transfer_button.dart';
import 'package:mone_me/widgets/home_logo_button.dart';
import 'package:mone_me/widgets/logout_dialog.dart';
import 'package:mone_me/widgets/textfield_search.dart';
import 'package:mone_me/widgets/transactions_history_button.dart';
import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';

class CommonHeader extends StatefulWidget {
  const CommonHeader({Key? key}) : super(key: key);

  @override
  State<CommonHeader> createState() => _CommonHeaderState();
}

class _CommonHeaderState extends State<CommonHeader> {
  @override
  Widget build(BuildContext context) {
    return commonHeader(context);
  }

  Widget commonHeader(BuildContext context) {
    final viewModelHome = Provider.of<HomeNotifier>(context, listen: true);
    final viewModelTransactions = Provider.of<TransactionsNotifier>(context, listen: true);
    final userId = Provider.of<LoginNotifier>(context, listen: false).userId!;
    return Container(
      color: CupertinoColors.white,
      width: ScreenConfig.width(context),
      padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
            child: Material(
              type: MaterialType.transparency,
              child: InkWell(
                onTap: () {
                  viewModelHome.getDashboardAPI(userId, DateTime.now().timeZoneName);
                  viewModelHome.getAllFriendsAPI(userId);
                  // viewModelTransactions.getSavedCardsAPI(userId);
                  viewModelTransactions.geAllTransactionsAPI(userId, "", "");
                  viewModelTransactions.getAllTransferredTransactionsAPI(userId, "", "");
                  viewModelTransactions.getAllReceivedTransactionsAPI(userId, "", "");
                  Navigator.pushNamed(context, AppRoutes.home);
                },
                child: homeIconButton(),
              ),
            ),
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: Material(
                    type: MaterialType.transparency,
                    child: InkWell(
                      borderRadius: BorderRadius.circular(25),
                      onTap: () {
                        Navigator.pushNamed(context, AppRoutes.linkedBankAccounts);
                      },
                      child: bankTransferButton(),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: Material(
                    type: MaterialType.transparency,
                    child: InkWell(
                      borderRadius: BorderRadius.circular(25),
                      onTap: () {
                        Navigator.pushNamed(context, AppRoutes.addBankAccount);
                      },
                      child: bankAccountButton(),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: Material(
                    type: MaterialType.transparency,
                    child: InkWell(
                      borderRadius: BorderRadius.circular(25),
                      onTap: () {
                        Navigator.pushNamed(context, AppRoutes.home);
                      },
                      child: transactionsHistoryButton(),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: Material(
                    type: MaterialType.transparency,
                    child: Tooltip(
                      message: wallet,
                      child: Ink.image(
                        image: const AssetImage("assets/images/wallet_icon_rounded.png"),
                        height: 40,
                        width: 40,
                        child: InkWell(
                            borderRadius: BorderRadius.circular(25),
                            onTap: () {
                              Navigator.pushNamed(context, AppRoutes.wallet);
                            }),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: Material(
                    type: MaterialType.transparency,
                    child: headerSearchField(context),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: Material(
                    type: MaterialType.transparency,
                    child: Tooltip(
                      message: notifications,
                      child: Ink.image(
                        image: const AssetImage("assets/images/notification_bell_icon.png"),
                        height: 25,
                        width: 25,
                        child: InkWell(borderRadius: BorderRadius.circular(25), onTap: () {}),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: PopupMenuButton<int>(
                      padding: const EdgeInsets.all(0),
                      tooltip: settings,
                      child: Image.asset("assets/images/setting_icon.png", width: 25, height: 25),
                      onSelected: (value) => onSelected(context, value),
                      itemBuilder: (context) => [
                            PopupMenuItem(
                                height: 25,
                                padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                                child: Text(logout, style: popMenuHeaderTextStyle),
                                value: 0),
                          ]),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: PopupMenuButton<int>(
                      shape: const RoundedRectangleBorder(
                        side: BorderSide(color: receivedIconBackground),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10),
                        ),
                      ),
                      offset: const Offset(-7, 50),
                      padding: const EdgeInsets.all(0),
                      tooltip: viewModelHome.userName != "" ? viewModelHome.userName : userName,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: CircleAvatar(
                          backgroundColor: receivedIconBackground,
                          radius: 21,
                          child: CircleAvatar(
                            backgroundColor: receivedIconBackground,
                            radius: 20,
                            child: viewModelHome.userProfileImage == ""
                                ? AdaptableText(viewModelHome.userName != "" ? viewModelHome.userName![0].toUpperCase() : appName[0].toUpperCase(),
                                    style: submitButtonTextStyle)
                                : ClipRRect(
                                    borderRadius: BorderRadius.circular(50),
                                    child: Image.network(
                                      viewModelHome.userProfileImage!,
                                      height: 40,
                                      width: 40,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                          ),
                        ),
                      ),
                      onSelected: (value) => onSelected(context, value),
                      itemBuilder: (context) => [
                            PopupMenuItem(
                                height: 25,
                                padding: const EdgeInsets.fromLTRB(10, 7, 7, 7),
                                child: const Text(profile),
                                textStyle: popMenuHeaderTextStyle,
                                value: 0),
                            PopupMenuItem(
                                height: 25,
                                padding: const EdgeInsets.fromLTRB(10, 7, 7, 7),
                                child: const Text(videoConsultation),
                                textStyle: popMenuHeaderTextStyle,
                                value: 1),
                            PopupMenuItem(
                                height: 25,
                                padding: const EdgeInsets.fromLTRB(10, 7, 7, 7),
                                child: const Text(inviteFriend),
                                textStyle: popMenuHeaderTextStyle,
                                value: 2),
                            PopupMenuItem(
                                height: 25,
                                padding: const EdgeInsets.fromLTRB(10, 7, 7, 7),
                                child: const Text(helpFeedBack),
                                textStyle: popMenuHeaderTextStyle,
                                value: 3),
                            PopupMenuItem(
                                height: 25,
                                padding: const EdgeInsets.fromLTRB(10, 7, 7, 7),
                                child: const Text(logout),
                                textStyle: popMenuHeaderTextStyle,
                                value: 4),
                          ]),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget headerSearchField(BuildContext context) {
    TextEditingController searchController = TextEditingController();
    final viewModelHome = Provider.of<HomeNotifier>(context, listen: true);
    final viewModelTransactions = Provider.of<TransactionsNotifier>(context, listen: true);
    final userId = Provider.of<LoginNotifier>(context, listen: false).userId!;
    Future<List> fetchData() async {
      await viewModelHome.searchUserByPhoneAPI(userId, searchController.text.trim());
      return viewModelHome.getUsersSearchList();
    }

    return ConstrainedBox(
      constraints: const BoxConstraints(minWidth: 200, maxWidth: 300),
      child: Container(
        margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: TextFieldSearch(
          minStringLength: 7,
          label: searchByPhone,
          controller: searchController,
          decoration: searchDecorator,
          textStyle: searchFormFieldStyle,
          initialList: viewModelHome.getUsersSearchList(),
          getSelectedValue: (value) async {
              Navigator.pushNamed(context, AppRoutes.userToUserTransactions, arguments: UserToUserModel(friendId: value!.id!, tabSelected: 0));
          },
          future: () {
            return fetchData();
          },
        ),
      ),
    );
  }

  onSelected(BuildContext context, int item) {
    switch (item) {
      case 0:
        Navigator.pushNamed(context, AppRoutes.profile);
        break;
      case 1:
        Navigator.pushNamed(context, AppRoutes.activateVideoConsultation);
        break;
      case 2:
        Share.share("Hey, Checkout www.moneeme.com");
        break;
      case 3:
        Navigator.pushNamed(context, AppRoutes.helpFeedback);
        break;
      case 4:
        logoutDialog(context);
        break;
    }
  }
}
