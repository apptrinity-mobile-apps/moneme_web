import 'package:flutter/cupertino.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/widgets/adaptable_text.dart';

Widget transactionsHistoryButton() {
  return Padding(
    padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Image.asset("assets/images/transaction_history_icon.png", height: 20, width: 20),
        Padding(
          padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
          child: AdaptableText(transactionsHistory, style: headerTextStyle, textMaxLines: 1),
        )
      ],
    ),
  );
}