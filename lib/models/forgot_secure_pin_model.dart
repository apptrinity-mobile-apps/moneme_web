class ForgotSecurePinModel {
  String? secureOTP;
  String? email;
  String? newPin;
  String? confirmNewPin;

  ForgotSecurePinModel({this.secureOTP, this.email, this.newPin, this.confirmNewPin});

  ForgotSecurePinModel.fromJson(Map<String, dynamic> json) {
    secureOTP = json['secureOTP'] ?? "";
    email = json['email'] ?? "";
    newPin = json['newPin'] ?? "";
    confirmNewPin = json['confirmNewPin'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['secureOTP'] = secureOTP ?? "";
    data['email'] = email ?? "";
    data['newPin'] = newPin ?? "";
    data['confirmNewPin'] = confirmNewPin ?? "";
    return data;
  }
}
