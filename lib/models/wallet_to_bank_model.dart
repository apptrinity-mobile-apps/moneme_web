class WalletToBankModel {
  String? userId;
  String? senderId;
  String? message;
  String? transactionType;
  String? paymentSource;
  String? bankAccountId;
  double? amount;
  double? transactionFee;
  double? transactionFeePercentage;
  String? messageTransaction;

  WalletToBankModel(
      {this.userId,
      this.senderId,
      this.message,
      this.transactionType,
      this.paymentSource,
      this.bankAccountId,
      this.amount,
      this.transactionFee,
      this.transactionFeePercentage,
      this.messageTransaction});

  WalletToBankModel.fromJson(Map<String, dynamic> json) {
    userId = json['userId'] ?? "";
    senderId = json['senderId'] ?? "";
    message = json['message'] ?? "";
    transactionType = json['transactionType'] ?? "";
    paymentSource = json['paymentSource'] ?? "";
    bankAccountId = json['bankAccountId'] ?? "";
    amount = json['amount'] ?? 0.00;
    transactionFee = json['transactionFee'] ?? 0.00;
    transactionFeePercentage = json['transactionFeePercentage'] ?? 0.00;
    messageTransaction = json['messageTransaction'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['userId'] = userId ?? "";
    data['senderId'] = senderId ?? "";
    data['message'] = message ?? "";
    data['transactionType'] = transactionType ?? "";
    data['paymentSource'] = paymentSource ?? "";
    data['bankAccountId'] = bankAccountId ?? "";
    data['amount'] = amount ?? 0.00;
    data['transactionFee'] = transactionFee ?? 0.00;
    data['transactionFeePercentage'] = transactionFeePercentage ?? 0.00;
    data['messageTransaction'] = messageTransaction ?? "";
    return data;
  }
}
