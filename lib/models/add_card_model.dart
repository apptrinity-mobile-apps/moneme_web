class AddCardModel {
  String? userId;
  String? accountType;
  String? last4Digits;
  CardDetails? cardDetails;

  AddCardModel({this.userId, this.accountType, this.last4Digits, this.cardDetails});

  AddCardModel.fromJson(Map<String, dynamic> json) {
    userId = json['userId'] ?? "";
    accountType = json['accountType'] ?? "";
    last4Digits = json['last4Digits'] ?? "";
    cardDetails = json['cardDetails'] != null ? CardDetails.fromJson(json['cardDetails']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['userId'] = userId ?? "";
    data['accountType'] = accountType ?? "";
    data['last4Digits'] = last4Digits ?? "";
    if (cardDetails != null) {
      data['cardDetails'] = cardDetails!.toJson();
    }
    return data;
  }
}

class CardDetails {
  String? cardNumber;
  String? expDate;
  String? cvv;
  String? last4Digits;

  CardDetails({this.cardNumber, this.expDate, this.cvv, this.last4Digits});

  CardDetails.fromJson(Map<String, dynamic> json) {
    cardNumber = json['cardNumber'] ?? "";
    expDate = json['expDate'] ?? "";
    cvv = json['cvv'] ?? "";
    last4Digits = json['last4Digits'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['cardNumber'] = cardNumber ?? "";
    data['expDate'] = expDate ?? "";
    data['cvv'] = cvv ?? "";
    data['last4Digits'] = last4Digits ?? "";
    return data;
  }
}
