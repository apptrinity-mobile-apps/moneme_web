class UserToUserModel {
  String? friendId;
  int? tabSelected;

  UserToUserModel({this.friendId, this.tabSelected});

  UserToUserModel.fromJson(Map<String, dynamic> json) {
    friendId = json['friendId'] ?? "";
    tabSelected = json['tabSelected'] ?? -1;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['friendId'] = friendId;
    data['tabSelected'] = tabSelected;
    return data;
  }
}
