class LanguagesResponseModel {
  List<LanguagesList>? languagesList;
  int? responseStatus;
  String? result;

  LanguagesResponseModel({this.languagesList, this.responseStatus, this.result});

  LanguagesResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['languagesList'] != null) {
      languagesList = <LanguagesList>[];
      json['languagesList'].forEach((v) {
        languagesList!.add(LanguagesList.fromJson(v));
      });
    } else {
      languagesList = <LanguagesList>[];
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (languagesList != null) {
      data['languagesList'] = languagesList!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class LanguagesList {
  String? createdOn;
  String? id;
  String? name;
  int? status;
  String? symbol;

  LanguagesList({this.createdOn, this.id, this.name, this.status, this.symbol});

  LanguagesList.fromJson(Map<String, dynamic> json) {
    createdOn = json['createdOn'] ?? "";
    id = json['id'] ?? "";
    name = json['name'] ?? "";
    status = json['status'] ?? 0;
    symbol = json['symbol'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['createdOn'] = createdOn;
    data['id'] = id;
    data['name'] = name;
    data['status'] = status;
    data['symbol'] = symbol;
    return data;
  }
}
