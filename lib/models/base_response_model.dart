class BaseResponseModel {
  int? responseStatus;
  String? result;
  String? verificationOtp;
  String? transactionId;
  String? stripeCustomerId;

  BaseResponseModel({this.responseStatus, this.result, this.verificationOtp, this.transactionId, this.stripeCustomerId});

  BaseResponseModel.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'] ?? "";
    result = json['result'] ?? "";
    verificationOtp = json['verificationOTP'] ?? "";
    transactionId = json['transactionId'] ?? "";
    stripeCustomerId = json['stripeCustomerId'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    data['verificationOTP'] = verificationOtp;
    data['transactionId'] = transactionId;
    data['stripeCustomerId'] = stripeCustomerId;
    return data;
  }
}
