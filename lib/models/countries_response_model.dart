class CountriesResponseModel {
  List<CountriesList>? countriesList;
  int? responseStatus;
  String? result;

  CountriesResponseModel({this.countriesList, this.responseStatus, this.result});

  CountriesResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['countriesList'] != null) {
      countriesList = <CountriesList>[];
      json['countriesList'].forEach((v) {
        countriesList!.add(CountriesList.fromJson(v));
      });
    } else {
      countriesList = <CountriesList>[];
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (countriesList != null) {
      data['countriesList'] = countriesList!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class CountriesList {
  String? countryCode;
  String? countryFlag;
  String? id;
  String? name;
  String? phoneCode;
  int? status;

  CountriesList({this.countryCode, this.countryFlag, this.id, this.name, this.phoneCode, this.status});

  CountriesList.fromJson(Map<String, dynamic> json) {
    countryCode = json['countryCode'] ?? "";
    countryFlag = json['countryFlag'] ?? "";
    id = json['id'] ?? "";
    name = json['name'] ?? "";
    phoneCode = json['phoneCode'] ?? "";
    status = json['status'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['countryCode'] = countryCode;
    data['countryFlag'] = countryFlag;
    data['id'] = id;
    data['name'] = name;
    data['phoneCode'] = phoneCode;
    data['status'] = status;
    return data;
  }
}
