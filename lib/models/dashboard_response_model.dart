class DashboardResponseModel {
  int? responseStatus;
  String? result;
  UserProfileDetails? userProfileDetails;

  DashboardResponseModel({this.responseStatus, this.result, this.userProfileDetails});

  DashboardResponseModel.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    userProfileDetails = json['userProfileDetails'] != null ? UserProfileDetails.fromJson(json['userProfileDetails']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    if (userProfileDetails != null) {
      data['userProfileDetails'] = userProfileDetails!.toJson();
    }
    return data;
  }
}

class UserProfileDetails {
  String? countryId;
  String? createdOn;
  String? email;
  String? id;
  String? languageId;
  String? phoneNumber;
  String? phoneCode;
  String? profilePicture;
  String? userName;
  double? currentBalance;
  double? receivedBalance;
  int? status;
  double? transferredBalance;
  bool? transferSecurePin;
  String? firstName;
  String? lastName;
  String? bussinessName;
  String? accountType;

  UserProfileDetails(
      {this.countryId,
      this.createdOn,
      this.currentBalance,
      this.email,
      this.id,
      this.languageId,
      this.phoneNumber,
      this.phoneCode,
      this.profilePicture,
      this.receivedBalance,
      this.status,
      this.transferredBalance,
      this.userName,
      this.transferSecurePin,
        this.accountType,
        this.firstName,
        this.lastName,
        this.bussinessName});

  UserProfileDetails.fromJson(Map<String, dynamic> json) {
    countryId = json['countryId'] ?? "";
    createdOn = json['createdOn'] ?? "";
    userName = json['userName'] ?? "";
    email = json['email'] ?? "";
    id = json['id'] ?? "";
    languageId = json['languageId'] ?? "";
    phoneNumber = json['phoneNumber'] ?? "";
    phoneCode = json['phoneCode'] ?? "";
    profilePicture = json['profilePicture'] ?? "";
    currentBalance = json['currentBalance'] ?? 0.00;
    receivedBalance = json['receivedBalance'] ?? 0.00;
    transferredBalance = json['transferredBalance'] ?? 0.00;
    status = json['status'] ?? 0;
    transferSecurePin = json['transferSecurePin'] ?? false;
    accountType = json['accountType'] ?? "";
    firstName = json['firstName'] ?? "";
    lastName = json['lastName'] ?? "";
    bussinessName = json['bussinessName'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['accountType'] = accountType;
    data['countryId'] = countryId;
    data['createdOn'] = createdOn;
    data['currentBalance'] = currentBalance;
    data['email'] = email;
    data['id'] = id;
    data['languageId'] = languageId;
    data['phoneNumber'] = phoneNumber;
    data['phoneCode'] = phoneCode;
    data['profilePicture'] = profilePicture;
    data['receivedBalance'] = receivedBalance;
    data['status'] = status;
    data['transferredBalance'] = transferredBalance;
    data['userName'] = userName;
    data['transferSecurePin'] = transferSecurePin;
    data['firstName'] = firstName;
    data['lastName'] = lastName;
    data['bussinessName'] = bussinessName;
    return data;
  }
}
