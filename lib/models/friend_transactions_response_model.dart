class FriendTransactionsResponseModel {
  int? responseStatus;
  String? result;
  UserProfileDetails? userProfileDetails;
  List<UserTransactionsList>? userTransactionsList;

  FriendTransactionsResponseModel({this.responseStatus, this.result, this.userProfileDetails, this.userTransactionsList});

  FriendTransactionsResponseModel.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    userProfileDetails = json['userProfileDetails'] != null ? UserProfileDetails.fromJson(json['userProfileDetails']) : null;
    if (json['userTransactionsList'] != null) {
      userTransactionsList = <UserTransactionsList>[];
      json['userTransactionsList'].forEach((v) {
        userTransactionsList!.add(UserTransactionsList.fromJson(v));
      });
    } else {
      userTransactionsList = <UserTransactionsList>[];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    if (userProfileDetails != null) {
      data['userProfileDetails'] = userProfileDetails!.toJson();
    }
    if (userTransactionsList != null) {
      data['userTransactionsList'] = userTransactionsList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class UserProfileDetails {
  String? actionText;
  String? countryId;
  String? createdOn;
  double? currentBalance;
  String? email;
  String? id;
  String? languageId;
  String? phoneNumber;
  String? profilePicture;
  double? receivedBalance;
  int? status;
  double? transferredBalance;
  String? userName;

  UserProfileDetails(
      {this.actionText,
      this.countryId,
      this.createdOn,
      this.currentBalance,
      this.email,
      this.id,
      this.languageId,
      this.phoneNumber,
      this.profilePicture,
      this.receivedBalance,
      this.status,
      this.transferredBalance,
      this.userName});

  UserProfileDetails.fromJson(Map<String, dynamic> json) {
    actionText = json['actionText'] ?? "";
    countryId = json['countryId'] ?? "";
    createdOn = json['createdOn'] ?? "";
    currentBalance = json['currentBalance'] ?? 0.00;
    email = json['email'] ?? "";
    id = json['id'] ?? "";
    languageId = json['languageId'] ?? "";
    phoneNumber = json['phoneNumber'] ?? "";
    profilePicture = json['profilePicture'] ?? "";
    receivedBalance = json['receivedBalance'] ?? 0.00;
    status = json['status'] ?? 0;
    transferredBalance = json['transferredBalance'] ?? 0.00;
    userName = json['userName'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['actionText'] = actionText;
    data['countryId'] = countryId;
    data['createdOn'] = createdOn;
    data['currentBalance'] = currentBalance;
    data['email'] = email;
    data['id'] = id;
    data['languageId'] = languageId;
    data['phoneNumber'] = phoneNumber;
    data['profilePicture'] = profilePicture;
    data['receivedBalance'] = receivedBalance;
    data['status'] = status;
    data['transferredBalance'] = transferredBalance;
    data['userName'] = userName;
    return data;
  }
}

class UserTransactionsList {
  String? date;
  List<TransactionsList>? transactionsList;

  UserTransactionsList({this.date, this.transactionsList});

  UserTransactionsList.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    if (json['transactionsList'] != null) {
      transactionsList = <TransactionsList>[];
      json['transactionsList'].forEach((v) {
        transactionsList!.add(TransactionsList.fromJson(v));
      });
    } else {
      transactionsList = <TransactionsList>[];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['date'] = date;
    if (transactionsList != null) {
      data['transactionsList'] = transactionsList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TransactionsList {
  double? amount;
  String? createdOn;
  double? currentBalance;
  String? id;
  String? message;
  String? messageTransaction;
  String? paymentSource;
  int? status;
  String? succeededOn;
  String? transactionType;
  String? transactionUniqueId;

  TransactionsList(
      {this.amount,
      this.createdOn,
      this.currentBalance,
      this.id,
      this.message,
      this.messageTransaction,
      this.paymentSource,
      this.status,
      this.succeededOn,
      this.transactionType,
      this.transactionUniqueId});

  TransactionsList.fromJson(Map<String, dynamic> json) {
    amount = json['amount'] ?? 0.00;
    createdOn = json['createdOn'] ?? "";
    currentBalance = json['currentBalance'] ?? 0.00;
    id = json['id'] ?? "";
    message = json['message'] ?? "";
    messageTransaction = json['messageTransaction'] ?? "";
    paymentSource = json['paymentSource'] ?? "";
    status = json['status'] ?? 0;
    succeededOn = json['succeededOn'] ?? "";
    transactionType = json['transactionType'] ?? "";
    transactionUniqueId = json['transactionUniqueId'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['amount'] = amount;
    data['createdOn'] = createdOn;
    data['currentBalance'] = currentBalance;
    data['id'] = id;
    data['message'] = message;
    data['messageTransaction'] = messageTransaction;
    data['paymentSource'] = paymentSource;
    data['status'] = status;
    data['succeededOn'] = succeededOn;
    data['transactionType'] = transactionType;
    data['transactionUniqueId'] = transactionUniqueId;
    return data;
  }
}
