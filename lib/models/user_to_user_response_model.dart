class UserToUserResponseModel {
  int? responseStatus;
  String? result;
  bool? userBlockFlag;
  UserProfileDetails? userProfileDetails;
  List<UserTransactionsList>? userTransactionsList;
  String? userTxnEndDate;
  String? userTxnStartDate;

  UserToUserResponseModel(
      {this.responseStatus,
      this.result,
      this.userBlockFlag,
      this.userProfileDetails,
      this.userTransactionsList,
      this.userTxnEndDate,
      this.userTxnStartDate});

  UserToUserResponseModel.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    userBlockFlag = json['userBlockFlag'] ?? false;
    userProfileDetails = json['userProfileDetails'] != null ? UserProfileDetails.fromJson(json['userProfileDetails']) : null;
    if (json['userTransactionsList'] != null) {
      userTransactionsList = <UserTransactionsList>[];
      json['userTransactionsList'].forEach((v) {
        userTransactionsList!.add(UserTransactionsList.fromJson(v));
      });
    } else {
      userTransactionsList = <UserTransactionsList>[];
    }
    userTxnEndDate = json['user_txn_end_date'] ?? "";
    userTxnStartDate = json['user_txn_start_date'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    data['userBlockFlag'] = userBlockFlag;
    if (userProfileDetails != null) {
      data['userProfileDetails'] = userProfileDetails!.toJson();
    }
    if (userTransactionsList != null) {
      data['userTransactionsList'] = userTransactionsList!.map((v) => v.toJson()).toList();
    }
    data['user_txn_end_date'] = userTxnEndDate;
    data['user_txn_start_date'] = userTxnStartDate;
    return data;
  }
}

class UserProfileDetails {
  String? accountType;
  String? actionText;
  String? bussinessName;
  String? countryId;
  String? createdOn;
  String? createdOnTime;
  double? currentBalance;
  String? currentTimezone;
  String? email;
  String? firstName;
  String? id;
  String? languageId;
  String? lastName;
  String? phoneCode;
  String? phoneNumber;
  String? profilePicture;
  double? receivedBalance;
  int? status;
  bool? transferSecurePin;
  double? transferredBalance;
  String? userName;

  UserProfileDetails(
      {this.accountType,
      this.actionText,
      this.bussinessName,
      this.countryId,
      this.createdOn,
      this.createdOnTime,
      this.currentBalance,
      this.currentTimezone,
      this.email,
      this.firstName,
      this.id,
      this.languageId,
      this.lastName,
      this.phoneCode,
      this.phoneNumber,
      this.profilePicture,
      this.receivedBalance,
      this.status,
      this.transferSecurePin,
      this.transferredBalance,
      this.userName});

  UserProfileDetails.fromJson(Map<String, dynamic> json) {
    accountType = json['accountType'] ?? "";
    actionText = json['actionText'] ?? "";
    bussinessName = json['bussinessName'] ?? "";
    countryId = json['countryId'] ?? "";
    createdOn = json['createdOn'] ?? "";
    createdOnTime = json['createdOnTime'] ?? "";
    currentBalance = json['currentBalance'] ?? 0.00;
    currentTimezone = json['currentTimezone'] ?? "";
    email = json['email'] ?? "";
    firstName = json['firstName'] ?? "";
    id = json['id'] ?? "";
    languageId = json['languageId'] ?? "";
    lastName = json['lastName'] ?? "";
    phoneCode = json['phoneCode'] ?? "";
    phoneNumber = json['phoneNumber'] ?? "";
    profilePicture = json['profilePicture'] ?? "";
    receivedBalance = json['receivedBalance'] ?? 0.00;
    status = json['status'] ?? 0;
    transferSecurePin = json['transferSecurePin'] ?? false;
    transferredBalance = json['transferredBalance'] ?? 0.00;
    userName = json['userName'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['accountType'] = accountType;
    data['actionText'] = actionText;
    data['bussinessName'] = bussinessName;
    data['countryId'] = countryId;
    data['createdOn'] = createdOn;
    data['createdOnTime'] = createdOnTime;
    data['currentBalance'] = currentBalance;
    data['currentTimezone'] = currentTimezone;
    data['email'] = email;
    data['firstName'] = firstName;
    data['id'] = id;
    data['languageId'] = languageId;
    data['lastName'] = lastName;
    data['phoneCode'] = phoneCode;
    data['phoneNumber'] = phoneNumber;
    data['profilePicture'] = profilePicture;
    data['receivedBalance'] = receivedBalance;
    data['status'] = status;
    data['transferSecurePin'] = transferSecurePin;
    data['transferredBalance'] = transferredBalance;
    data['userName'] = userName;
    return data;
  }
}

class UserTransactionsList {
  String? date;
  List<TransactionsList>? transactionsList;

  UserTransactionsList({this.date, this.transactionsList});

  UserTransactionsList.fromJson(Map<String, dynamic> json) {
    date = json['date'] ?? "";
    if (json['transactionsList'] != null) {
      transactionsList = <TransactionsList>[];
      json['transactionsList'].forEach((v) {
        transactionsList!.add(TransactionsList.fromJson(v));
      });
    } else {
      transactionsList = <TransactionsList>[];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['date'] = date;
    if (transactionsList != null) {
      data['transactionsList'] = transactionsList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TransactionsList {
  double? amount;
  String? createdOn;
  double? currentBalance;
  String? id;
  String? message;
  String? messageTransaction;
  String? paymentSource;
  String? requestMessage;
  int? requestStatus;
  int? status;
  String? succeededOn;
  String? transactionTime;
  String? transactionType;
  String? transactionUniqueId;

  TransactionsList(
      {this.amount,
      this.createdOn,
      this.currentBalance,
      this.id,
      this.message,
      this.messageTransaction,
      this.paymentSource,
      this.requestMessage,
      this.requestStatus,
      this.status,
      this.succeededOn,
      this.transactionTime,
      this.transactionType,
      this.transactionUniqueId});

  TransactionsList.fromJson(Map<String, dynamic> json) {
    amount = json['amount'] ?? 0.00;
    createdOn = json['createdOn'] ?? 0.00;
    currentBalance = json['currentBalance'] ?? "";
    id = json['id'] ?? "";
    message = json['message'] ?? "";
    messageTransaction = json['messageTransaction'] ?? "";
    paymentSource = json['paymentSource'] ?? "";
    requestMessage = json['requestMessage'] ?? "";
    requestStatus = json['requestStatus'] ?? 0;
    status = json['status'] ?? 0;
    succeededOn = json['succeededOn'] ?? "";
    transactionTime = json['transactionTime'] ?? "";
    transactionType = json['transactionType'] ?? "";
    transactionUniqueId = json['transactionUniqueId'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['amount'] = amount;
    data['createdOn'] = createdOn;
    data['currentBalance'] = currentBalance;
    data['id'] = id;
    data['message'] = message;
    data['messageTransaction'] = messageTransaction;
    data['paymentSource'] = paymentSource;
    data['requestMessage'] = requestMessage;
    data['requestStatus'] = requestStatus;
    data['status'] = status;
    data['succeededOn'] = succeededOn;
    data['transactionTime'] = transactionTime;
    data['transactionType'] = transactionType;
    data['transactionUniqueId'] = transactionUniqueId;
    return data;
  }
}
