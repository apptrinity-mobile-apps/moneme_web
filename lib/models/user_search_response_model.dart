class UserSearchResponseModel {
  int? responseStatus;
  String? result;
  List<UsersList>? usersList;

  UserSearchResponseModel({this.responseStatus, this.result, this.usersList});

  UserSearchResponseModel.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    if (json['usersList'] != null) {
      usersList = <UsersList>[];
      json['usersList'].forEach((v) {
        usersList!.add(UsersList.fromJson(v));
      });
    } else {
      usersList = <UsersList>[];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    if (usersList != null) {
      data['usersList'] = usersList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class UsersList {
  String? actionText;
  String? countryId;
  String? createdOn;
  double? currentBalance;
  String? email;
  String? id;
  String? languageId;
  String? phoneNumber;
  String? profilePicture;
  double? receivedBalance;
  int? status;
  double? transferredBalance;
  String? userName;

  UsersList(
      {this.actionText,
      this.countryId,
      this.createdOn,
      this.currentBalance,
      this.email,
      this.id,
      this.languageId,
      this.phoneNumber,
      this.profilePicture,
      this.receivedBalance,
      this.status,
      this.transferredBalance,
      this.userName});

  UsersList.fromJson(Map<String, dynamic> json) {
    actionText = json['actionText'] ?? "";
    countryId = json['countryId'] ?? "";
    createdOn = json['createdOn'] ?? "";
    currentBalance = json['currentBalance'] ?? 0.00;
    email = json['email'] ?? "";
    id = json['id'] ?? "";
    languageId = json['languageId'] ?? "";
    phoneNumber = json['phoneNumber'] ?? "";
    profilePicture = json['profilePicture'] ?? "";
    receivedBalance = json['receivedBalance'] ?? 0.00;
    status = json['status'] ?? 0;
    transferredBalance = json['transferredBalance'] ?? 0.00;
    userName = json['userName'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['actionText'] = actionText;
    data['countryId'] = countryId;
    data['createdOn'] = createdOn;
    data['currentBalance'] = currentBalance;
    data['email'] = email;
    data['id'] = id;
    data['languageId'] = languageId;
    data['phoneNumber'] = phoneNumber;
    data['profilePicture'] = profilePicture;
    data['receivedBalance'] = receivedBalance;
    data['status'] = status;
    data['transferredBalance'] = transferredBalance;
    data['userName'] = userName;
    return data;
  }
}
