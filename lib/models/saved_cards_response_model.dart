class SavedCardsResponseModel {
  List<CreditCardsList>? creditCardsList;
  int? responseStatus;
  String? result;

  SavedCardsResponseModel({this.creditCardsList, this.responseStatus, this.result});

  SavedCardsResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['creditCardsList'] != null) {
      creditCardsList = <CreditCardsList>[];
      json['creditCardsList'].forEach((v) {
        creditCardsList!.add(CreditCardsList.fromJson(v));
      });
    } else {
      creditCardsList = <CreditCardsList>[];
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (creditCardsList != null) {
      data['creditCardsList'] = creditCardsList!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class CreditCardsList {
  String? accountType;
  String? bankId;
  String? bankName;
  CardDetails? cardDetails;
  String? createdOn;
  String? id;
  String? last4Digits;
  int? status;
  String? userId;

  CreditCardsList({this.accountType, this.bankId, this.cardDetails, this.createdOn, this.id, this.last4Digits, this.status, this.userId});

  CreditCardsList.fromJson(Map<String, dynamic> json) {
    accountType = json['accountType'] ?? "";
    bankId = json['bankId'] ?? "";
    bankName = json['bankName'] ?? "";
    cardDetails = json['cardDetails'] != null ? CardDetails.fromJson(json['cardDetails']) : null;
    createdOn = json['createdOn'] ?? "";
    id = json['id'] ?? "";
    last4Digits = json['last4Digits'] ?? "";
    status = json['status'] ?? 0;
    userId = json['userId'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['accountType'] = accountType;
    data['bankId'] = bankId;
    data['bankName'] = bankName;
    if (cardDetails != null) {
      data['cardDetails'] = cardDetails!.toJson();
    }
    data['createdOn'] = createdOn;
    data['id'] = id;
    data['last4Digits'] = last4Digits;
    data['status'] = status;
    data['userId'] = userId;
    return data;
  }
}

class CardDetails {
  String? cardNumber;
  String? cvv;
  String? expDate;
  String? last4Digits;

  CardDetails({this.cardNumber, this.cvv, this.expDate, this.last4Digits});

  CardDetails.fromJson(Map<String, dynamic> json) {
    cardNumber = json['cardNumber'] ?? "";
    cvv = json['cvv'] ?? "";
    expDate = json['expDate'] ?? "";
    last4Digits = json['last4Digits'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['cardNumber'] = cardNumber;
    data['cvv'] = cvv;
    data['expDate'] = expDate;
    data['last4Digits'] = last4Digits;
    return data;
  }
}
