class AddTransactionModel {
  String? senderId;
  String? receiverId;
  String? amount;
  String? transferSecurePin;
  String? message;
  String? transactionType;
  String? messageTransaction;
  String? paymentSource;

  AddTransactionModel(
      {this.senderId,
      this.receiverId,
      this.amount,
      this.transferSecurePin,
      this.message,
      this.transactionType,
      this.messageTransaction,
      this.paymentSource});

  AddTransactionModel.fromJson(Map<String, dynamic> json) {
    senderId = json['senderId'] ?? "";
    receiverId = json['receiverId'] ?? "";
    amount = json['amount'] ?? "";
    transferSecurePin = json['transferSecurePin'] ?? "";
    message = json['message'] ?? "";
    transactionType = json['transactionType'] ?? "";
    messageTransaction = json['messageTransaction'] ?? "";
    paymentSource = json['paymentSource'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['senderId'] = senderId ?? "";
    data['receiverId'] = receiverId ?? "";
    data['amount'] = amount ?? "";
    data['transferSecurePin'] = transferSecurePin ?? "";
    data['message'] = message ?? "";
    data['transactionType'] = transactionType ?? "";
    data['messageTransaction'] = messageTransaction ?? "";
    data['paymentSource'] = paymentSource ?? "";
    return data;
  }
}
