class TransactionsFeesResponseModel {
  int? responseStatus;
  String? result;
  TransactionFeeDetails? transactionFeeDetails;

  TransactionsFeesResponseModel({this.responseStatus, this.result, this.transactionFeeDetails});

  TransactionsFeesResponseModel.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    transactionFeeDetails = json['transactionFeeDetails'] != null ? TransactionFeeDetails.fromJson(json['transactionFeeDetails']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    if (transactionFeeDetails != null) {
      data['transactionFeeDetails'] = transactionFeeDetails!.toJson();
    }
    return data;
  }
}

class TransactionFeeDetails {
  String? depositFee;
  String? withdrawFee;

  TransactionFeeDetails({this.depositFee, this.withdrawFee});

  TransactionFeeDetails.fromJson(Map<String, dynamic> json) {
    depositFee = json['depositFee'] ?? "";
    withdrawFee = json['withdrawFee'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['depositFee'] = depositFee;
    data['withdrawFee'] = withdrawFee;
    return data;
  }
}
