class LoginResponseModel {
  int? responseStatus;
  String? result;
  UserData? userData;

  LoginResponseModel({this.responseStatus, this.result, this.userData});

  LoginResponseModel.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    userData = json['userData'] != null ? UserData.fromJson(json['userData']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    if (userData != null) {
      data['userData'] = userData!.toJson();
    }
    return data;
  }
}

class UserData {
  String? accountType;
  String? actionText;
  String? countryId;
  String? createdOn;
  String? createdOnTime;
  String? currentTimezone;
  String? email;
  String? id;
  String? languageId;
  String? phoneCode;
  String? phoneNumber;
  String? profilePicture;
  double? currentBalance;
  double? receivedBalance;
  double? transferredBalance;
  int? status;
  bool? transferSecurePin;
  String? userName;
  String? firstName;
  String? lastName;
  String? bussinessName;

  UserData(
      {this.accountType,
      this.actionText,
      this.countryId,
      this.createdOn,
      this.createdOnTime,
      this.currentBalance,
      this.currentTimezone,
      this.email,
      this.id,
      this.languageId,
      this.phoneCode,
      this.phoneNumber,
      this.profilePicture,
      this.receivedBalance,
      this.status,
      this.transferSecurePin,
      this.transferredBalance,
      this.userName,
      this.firstName,
      this.lastName,
      this.bussinessName});

  UserData.fromJson(Map<String, dynamic> json) {
    accountType = json['accountType'] ?? "";
    actionText = json['actionText'] ?? "";
    countryId = json['countryId'] ?? "";
    createdOn = json['createdOn'] ?? "";
    createdOnTime = json['createdOnTime'] ?? "";
    currentBalance = json['currentBalance'] ?? 0.00;
    currentTimezone = json['currentTimezone'] ?? "";
    email = json['email'] ?? "";
    id = json['id'] ?? "";
    languageId = json['languageId'] ?? "";
    phoneCode = json['phoneCode'] ?? "";
    phoneNumber = json['phoneNumber'] ?? "";
    profilePicture = json['profilePicture'] ?? "";
    receivedBalance = json['receivedBalance'] ?? 0.00;
    status = json['status'] ?? 0;
    transferSecurePin = json['transferSecurePin'] ?? "";
    transferredBalance = json['transferredBalance'] ?? 0.00;
    userName = json['userName'] ?? "";
    firstName = json['firstName'] ?? "";
    lastName = json['lastName'] ?? "";
    bussinessName = json['bussinessName'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['accountType'] = accountType;
    data['actionText'] = actionText;
    data['countryId'] = countryId;
    data['createdOn'] = createdOn;
    data['createdOnTime'] = createdOnTime;
    data['currentBalance'] = currentBalance;
    data['currentTimezone'] = currentTimezone;
    data['email'] = email;
    data['id'] = id;
    data['languageId'] = languageId;
    data['phoneCode'] = phoneCode;
    data['phoneNumber'] = phoneNumber;
    data['profilePicture'] = profilePicture;
    data['receivedBalance'] = receivedBalance;
    data['status'] = status;
    data['transferSecurePin'] = transferSecurePin;
    data['transferredBalance'] = transferredBalance;
    data['userName'] = userName;
    data['firstName'] = firstName;
    data['lastName'] = lastName;
    data['bussinessName'] = bussinessName;
    return data;
  }
}
