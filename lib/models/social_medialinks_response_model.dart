class SocialMediaLinksResponseModel {
  int? responseStatus;
  String? result;
  List<SocialMediasList>? socialMediasList;

  SocialMediaLinksResponseModel({this.responseStatus, this.result, this.socialMediasList});

  SocialMediaLinksResponseModel.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    if (json['socialMediasList'] != null) {
      socialMediasList = <SocialMediasList>[];
      json['socialMediasList'].forEach((v) {
        socialMediasList!.add(SocialMediasList.fromJson(v));
      });
    } else {
      socialMediasList = <SocialMediasList>[];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    if (socialMediasList != null) {
      data['socialMediasList'] = socialMediasList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SocialMediasList {
  String? image;
  String? name;
  String? urlLink;

  SocialMediasList({this.image, this.name, this.urlLink});

  SocialMediasList.fromJson(Map<String, dynamic> json) {
    image = json['image'];
    name = json['name'];
    urlLink = json['urlLink'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['image'] = image;
    data['name'] = name;
    data['urlLink'] = urlLink;
    return data;
  }
}
