class UserSignUpModel {
  String? languageId;
  String? countryId;
  String? firstName;
  String? lastName;
  String? accountType;
  String? bussinessName;
  String? email;
  String? password;
  String? confirmNewPassword;
  String? phoneNumber;
  String? profilePicture;
  String? phoneCode;

  UserSignUpModel(
      {required this.languageId,
      required this.countryId,
      required this.firstName,
      required this.lastName,
      required this.email,
      required this.accountType,
      required this.bussinessName,
      required this.password,
      required this.confirmNewPassword,
      required this.phoneNumber,
      required this.profilePicture,
      required this.phoneCode});

  UserSignUpModel.fromJson(Map<String, dynamic> json) {
    languageId = json['languageId'] ?? "";
    countryId = json['countryId'] ?? "";
    firstName = json['firstName'] ?? "";
    lastName = json['lastName'] ?? "";
    accountType = json['accountType'] ?? "";
    bussinessName = json['bussinessName'] ?? "";
    email = json['email'] ?? "";
    password = json['password'] ?? "";
    confirmNewPassword = json['confirmNewPassword'] ?? "";
    phoneNumber = json['phoneNumber'] ?? "";
    profilePicture = json['profilePicture'] ?? "";
    phoneCode = json['phoneCode'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['languageId'] = languageId ?? "";
    data['countryId'] = countryId ?? "";
    data['firstName'] = firstName ?? "";
    data['lastName'] = lastName ?? "";
    data['accountType'] = accountType ?? "";
    data['bussinessName'] = bussinessName ?? "";
    data['email'] = email ?? "";
    data['password'] = password ?? "";
    data['confirmNewPassword'] = confirmNewPassword ?? "";
    data['phoneNumber'] = phoneNumber ?? "";
    data['profilePicture'] = profilePicture ?? "";
    data['phoneCode'] = phoneCode ?? "";
    return data;
  }
}
