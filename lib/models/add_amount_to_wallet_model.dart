class AddAmountToWalletModel {
  String? userId;
  String? accountType;
  String? last4Digits;
  CardDetails? cardDetails;
  String? receiverId;
  String? transactionType;
  String? amount;
  String? messageTransaction;
  String? paymentSource;
  String? cardAccountId;

  AddAmountToWalletModel(
      {this.userId,
      this.accountType,
      this.last4Digits,
      this.cardDetails,
      this.receiverId,
      this.transactionType,
      this.amount,
      this.messageTransaction,
      this.paymentSource,
      this.cardAccountId});

  AddAmountToWalletModel.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    accountType = json['accountType'] ?? "";
    last4Digits = json['last4Digits'] ?? "";
    cardDetails = json['cardDetails'] != null ? CardDetails.fromJson(json['cardDetails']) : null;
    receiverId = json['receiverId'] ?? "";
    transactionType = json['transactionType'] ?? "";
    amount = json['amount'] ?? "";
    messageTransaction = json['messageTransaction'] ?? "";
    paymentSource = json['paymentSource'] ?? "";
    cardAccountId = json['cardAccountId'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['userId'] = userId ?? "";
    data['accountType'] = accountType ?? "";
    data['last4Digits'] = last4Digits ?? "";
    if (cardDetails != null) {
      data['cardDetails'] = cardDetails!.toJson();
    }
    data['receiverId'] = receiverId ?? "";
    data['transactionType'] = transactionType ?? "";
    data['amount'] = amount ?? "";
    data['messageTransaction'] = messageTransaction ?? "";
    data['paymentSource'] = paymentSource ?? "";
    data['cardAccountId'] = cardAccountId ?? "";
    return data;
  }
}

class CardDetails {
  String? cardNumber;
  String? expDate;
  String? cvv;
  String? last4Digits;

  CardDetails({this.cardNumber, this.expDate, this.cvv, this.last4Digits});

  CardDetails.fromJson(Map<String, dynamic> json) {
    cardNumber = json['cardNumber'] ?? "";
    expDate = json['expDate'] ?? "";
    cvv = json['cvv'] ?? "";
    last4Digits = json['last4Digits'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['cardNumber'] = cardNumber ?? "";
    data['expDate'] = expDate ?? "";
    data['cvv'] = cvv ?? "";
    data['last4Digits'] = last4Digits ?? "";
    return data;
  }
}
