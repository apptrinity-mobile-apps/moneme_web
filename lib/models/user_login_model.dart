class UserLoginModel {
  String? password;
  String? phoneNumber;
  String? phoneCode;

  UserLoginModel({this.phoneNumber, this.password, this.phoneCode});

  UserLoginModel.fromJson(Map<String, dynamic> json) {
    phoneNumber = json['phoneNumber'] ?? "";
    password = json['password'] ?? "";
    phoneCode = json['phoneCode'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['phoneNumber'] = phoneNumber ?? "";
    data['password'] = password ?? "";
    data['phoneCode'] = phoneCode ?? "";
    return data;
  }
}
