class FriendsResponseModel {
  int? responseStatus;
  String? result;
  List<UniqueFriendsList>? uniqueFriendsList;
  List<UniqueFriendsList>? businessFriendsList;

  FriendsResponseModel({this.responseStatus, this.result, this.uniqueFriendsList});

  FriendsResponseModel.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    if (json['uniqueFriendsList'] != null) {
      uniqueFriendsList = <UniqueFriendsList>[];
      json['uniqueFriendsList'].forEach((v) {
        uniqueFriendsList!.add(UniqueFriendsList.fromJson(v));
      });
    } else {
      uniqueFriendsList = <UniqueFriendsList>[];
    }
    if (json['businessFriendsList'] != null) {
      businessFriendsList = <UniqueFriendsList>[];
      json['businessFriendsList'].forEach((v) {
        businessFriendsList!.add(UniqueFriendsList.fromJson(v));
      });
    } else {
      businessFriendsList = <UniqueFriendsList>[];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    if (uniqueFriendsList != null) {
      data['uniqueFriendsList'] = uniqueFriendsList!.map((v) => v.toJson()).toList();
    }
    if (businessFriendsList != null) {
      data['businessFriendsList'] = businessFriendsList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class UniqueFriendsList {
  String? id;
  String? name;
  String? phoneNumber;
  String? profilePicture;

  UniqueFriendsList({this.id, this.name, this.phoneNumber, this.profilePicture});

  UniqueFriendsList.fromJson(Map<String, dynamic> json) {
    id = json['id'] ?? "";
    name = json['name'] ?? "";
    phoneNumber = json['phoneNumber'] ?? "";
    profilePicture = json['profilePicture'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['phoneNumber'] = phoneNumber;
    data['profilePicture'] = profilePicture;
    return data;
  }
}
