class TransactionSingleViewResponseModel {
  FriendsProfileDetails? friendsProfileDetails;
  int? responseStatus;
  String? result;
  TransactionsDetails? transactionsDetails;

  TransactionSingleViewResponseModel({this.friendsProfileDetails, this.responseStatus, this.result, this.transactionsDetails});

  TransactionSingleViewResponseModel.fromJson(Map<String, dynamic> json) {
    friendsProfileDetails = json['friendsProfileDetails'] != null ? FriendsProfileDetails.fromJson(json['friendsProfileDetails']) : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
    transactionsDetails = json['transactionsDetails'] != null ? TransactionsDetails.fromJson(json['transactionsDetails']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (friendsProfileDetails != null) {
      data['friendsProfileDetails'] = friendsProfileDetails!.toJson();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    if (transactionsDetails != null) {
      data['transactionsDetails'] = transactionsDetails!.toJson();
    }
    return data;
  }
}

class FriendsProfileDetails {
  String? accountType;
  String? actionText;
  String? bussinessName;
  String? countryId;
  String? createdOn;
  String? createdOnTime;
  double? currentBalance;
  String? currentTimezone;
  String? email;
  String? firstName;
  String? id;
  String? languageId;
  String? lastName;
  String? phoneCode;
  String? phoneNumber;
  String? profilePicture;
  double? receivedBalance;
  int? status;
  bool? transferSecurePin;
  double? transferredBalance;
  String? userName;

  FriendsProfileDetails(
      {this.accountType,
      this.actionText,
      this.bussinessName,
      this.countryId,
      this.createdOn,
      this.createdOnTime,
      this.currentBalance,
      this.currentTimezone,
      this.email,
      this.firstName,
      this.id,
      this.languageId,
      this.lastName,
      this.phoneCode,
      this.phoneNumber,
      this.profilePicture,
      this.receivedBalance,
      this.status,
      this.transferSecurePin,
      this.transferredBalance,
      this.userName});

  FriendsProfileDetails.fromJson(Map<String, dynamic> json) {
    accountType = json['accountType'] ?? "";
    actionText = json['actionText'] ?? "";
    bussinessName = json['bussinessName'] ?? "";
    countryId = json['countryId'] ?? "";
    createdOn = json['createdOn'] ?? "";
    createdOnTime = json['createdOnTime'] ?? "";
    currentBalance = json['currentBalance'] ?? 0.00;
    currentTimezone = json['currentTimezone'] ?? "";
    email = json['email'] ?? "";
    firstName = json['firstName'] ?? "";
    id = json['id'] ?? "";
    languageId = json['languageId'] ?? "";
    lastName = json['lastName'] ?? "";
    phoneCode = json['phoneCode'] ?? "";
    phoneNumber = json['phoneNumber'] ?? "";
    profilePicture = json['profilePicture'] ?? "";
    receivedBalance = json['receivedBalance'] ?? 0.00;
    status = json['status'] ?? 0;
    transferSecurePin = json['transferSecurePin'] ?? "";
    transferredBalance = json['transferredBalance'] ?? 0.00;
    userName = json['userName'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['accountType'] = accountType;
    data['actionText'] = actionText;
    data['bussinessName'] = bussinessName;
    data['countryId'] = countryId;
    data['createdOn'] = createdOn;
    data['createdOnTime'] = createdOnTime;
    data['currentBalance'] = currentBalance;
    data['currentTimezone'] = currentTimezone;
    data['email'] = email;
    data['firstName'] = firstName;
    data['id'] = id;
    data['languageId'] = languageId;
    data['lastName'] = lastName;
    data['phoneCode'] = phoneCode;
    data['phoneNumber'] = phoneNumber;
    data['profilePicture'] = profilePicture;
    data['receivedBalance'] = receivedBalance;
    data['status'] = status;
    data['transferSecurePin'] = transferSecurePin;
    data['transferredBalance'] = transferredBalance;
    data['userName'] = userName;
    return data;
  }
}

class TransactionsDetails {
  String? amount;
  String? bookingId;
  String? createdOn;
  String? createdOn1;
  String? currentBalance;
  String? endTime;
  String? id;
  String? message;
  String? messageTransaction;
  String? orderId;
  String? paymentSource;
  String? receiverId;
  String? receiverImage;
  String? receiverName;
  String? receiverPhoneCode;
  String? receiverPhoneNumber;
  String? requestMessage;
  String? senderId;
  String? senderImage;
  String? senderName;
  String? senderPhoneCode;
  String? senderPhoneNumber;
  String? serviceType;
  String? startTime;
  int? status;
  String? succeededOn;
  String? totalAmount;
  String? transactionFee;
  String? transactionFeePercentage;
  String? transactionTime;
  String? transactionType;
  String? transactionUniqueId;

  TransactionsDetails(
      {this.amount,
      this.bookingId,
      this.createdOn,
      this.createdOn1,
      this.currentBalance,
      this.endTime,
      this.id,
      this.message,
      this.messageTransaction,
      this.orderId,
      this.paymentSource,
      this.receiverId,
      this.receiverImage,
      this.receiverName,
      this.receiverPhoneCode,
      this.receiverPhoneNumber,
      this.requestMessage,
      this.senderId,
      this.senderImage,
      this.senderName,
      this.senderPhoneCode,
      this.senderPhoneNumber,
      this.serviceType,
      this.startTime,
      this.status,
      this.succeededOn,
      this.totalAmount,
      this.transactionFee,
      this.transactionFeePercentage,
      this.transactionTime,
      this.transactionType,
      this.transactionUniqueId});

  TransactionsDetails.fromJson(Map<String, dynamic> json) {
    amount = json['amount'] ?? "";
    bookingId = json['bookingId'] ?? "";
    createdOn = json['createdOn'] ?? "";
    createdOn1 = json['createdOn1'] ?? "";
    currentBalance = json['currentBalance'] ?? "";
    endTime = json['endTime'] ?? "";
    id = json['id'] ?? "";
    message = json['message'] ?? "";
    messageTransaction = json['messageTransaction'] ?? "";
    orderId = json['orderId'] ?? "";
    paymentSource = json['paymentSource'] ?? "";
    receiverId = json['receiverId'] ?? "";
    receiverImage = json['receiverImage'] ?? "";
    receiverName = json['receiverName'] ?? "";
    receiverPhoneCode = json['receiverPhoneCode'] ?? "";
    receiverPhoneNumber = json['receiverPhoneNumber'] ?? "";
    requestMessage = json['requestMessage'] ?? "";
    senderId = json['senderId'] ?? "";
    senderImage = json['senderImage'] ?? "";
    senderName = json['senderName'] ?? "";
    senderPhoneCode = json['senderPhoneCode'] ?? "";
    senderPhoneNumber = json['senderPhoneNumber'] ?? "";
    serviceType = json['serviceType'] ?? "";
    startTime = json['startTime'] ?? "";
    status = json['status'] ?? 0;
    succeededOn = json['succeededOn'] ?? "";
    totalAmount = json['totalAmount'] ?? "";
    transactionFee = json['transactionFee'] ?? "";
    transactionFeePercentage = json['transactionFeePercentage'] ?? "";
    transactionTime = json['transactionTime'] ?? "";
    transactionType = json['transactionType'] ?? "";
    transactionUniqueId = json['transactionUniqueId'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['amount'] = amount;
    data['bookingId'] = bookingId;
    data['createdOn'] = createdOn;
    data['createdOn1'] = createdOn1;
    data['currentBalance'] = currentBalance;
    data['endTime'] = endTime;
    data['id'] = id;
    data['message'] = message;
    data['messageTransaction'] = messageTransaction;
    data['orderId'] = orderId;
    data['paymentSource'] = paymentSource;
    data['receiverId'] = receiverId;
    data['receiverImage'] = receiverImage;
    data['receiverName'] = receiverName;
    data['receiverPhoneCode'] = receiverPhoneCode;
    data['receiverPhoneNumber'] = receiverPhoneNumber;
    data['requestMessage'] = requestMessage;
    data['senderId'] = senderId;
    data['senderImage'] = senderImage;
    data['senderName'] = senderName;
    data['senderPhoneCode'] = senderPhoneCode;
    data['senderPhoneNumber'] = senderPhoneNumber;
    data['serviceType'] = serviceType;
    data['startTime'] = startTime;
    data['status'] = status;
    data['succeededOn'] = succeededOn;
    data['totalAmount'] = totalAmount;
    data['transactionFee'] = transactionFee;
    data['transactionFeePercentage'] = transactionFeePercentage;
    data['transactionTime'] = transactionTime;
    data['transactionType'] = transactionType;
    data['transactionUniqueId'] = transactionUniqueId;
    return data;
  }
}
