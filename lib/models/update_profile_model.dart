class UpdateProfileModel {
  String? userId;
  String? countryId;
  String? languageId;
  String? firstName;
  String? lastName;
  String? accountType;
  String? bussinessName;
  String? profilePicture;

  UpdateProfileModel(
      {this.userId, this.countryId, this.languageId, this.firstName, this.lastName, this.accountType, this.bussinessName, this.profilePicture});

  UpdateProfileModel.fromJson(Map<String, dynamic> json) {
    userId = json['userId'] ?? "";
    countryId = json['countryId'] ?? "";
    languageId = json['languageId'] ?? "";
    firstName = json['firstName'] ?? "";
    lastName = json['lastName'] ?? "";
    accountType = json['accountType'] ?? "";
    bussinessName = json['bussinessName'] ?? "";
    profilePicture = json['profilePicture'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['userId'] = userId ?? "";
    data['countryId'] = countryId ?? "";
    data['languageId'] = languageId ?? "";
    data['firstName'] = firstName ?? "";
    data['lastName'] = lastName ?? "";
    data['accountType'] = accountType ?? "";
    data['bussinessName'] = bussinessName ?? "";
    data['profilePicture'] = profilePicture ?? "";
    return data;
  }
}
