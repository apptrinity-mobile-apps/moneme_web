class BankModelResponseModel {
  List<Bank>? banksList;
  int? responseStatus;
  String? result;

  BankModelResponseModel({this.banksList, this.responseStatus, this.result});

  BankModelResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['banksList'] != null) {
      banksList = <Bank>[];
      json['banksList'].forEach((v) {
        banksList!.add(Bank.fromJson(v));
      });
    } else {
      banksList = <Bank>[];
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (banksList != null) {
      data['banksList'] = banksList!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class Bank {
  String? accountHolderName;
  String? accountNumber;
  String? bankId;
  String? bankName;
  String? ifscCode;
  int? status;

  Bank({this.accountHolderName, this.accountNumber, this.bankId, this.bankName, this.ifscCode, this.status});

  Bank.fromJson(Map<String, dynamic> json) {
    accountHolderName = json['accountHolderName'] ?? "";
    accountNumber = json['accountNumber'] ?? "";
    bankId = json['bankId'] ?? "";
    bankName = json['bankName'] ?? "";
    ifscCode = json['ifscCode'] ?? "";
    status = json['status'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['accountHolderName'] = accountHolderName;
    data['accountNumber'] = accountNumber;
    data['bankId'] = bankId;
    data['bankName'] = bankName;
    data['ifscCode'] = ifscCode;
    data['status'] = status;
    return data;
  }
}
