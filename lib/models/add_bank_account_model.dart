class AddBankAccountModel {
  String? userId;
  String? bankName;
  String? accountHolderName;
  String? accountNumber;
  String? routingNumber;
  String? ifscCode;
  String? accountType;

  AddBankAccountModel({this.userId, this.bankName, this.accountHolderName, this.accountNumber, this.routingNumber, this.ifscCode, this.accountType});

  AddBankAccountModel.fromJson(Map<String, dynamic> json) {
    userId = json['userId'] ?? "";
    bankName = json['bankName'] ?? "";
    accountHolderName = json['accountHolderName'] ?? "";
    accountNumber = json['accountNumber'] ?? "";
    routingNumber = json['routingNumber'] ?? "";
    ifscCode = json['ifscCode'] ?? "";
    accountType = json['accountType'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['userId'] = userId ?? "";
    data['bankName'] = bankName ?? "";
    data['accountHolderName'] = accountHolderName ?? "";
    data['accountNumber'] = accountNumber ?? "";
    data['routingNumber'] = routingNumber ?? "";
    data['ifscCode'] = ifscCode ?? "";
    data['accountType'] = accountType ?? "";
    return data;
  }
}
