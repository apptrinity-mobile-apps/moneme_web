class TransactionHistoryResponseModel {
  int? responseStatus;
  String? result;
  List<TransactionsList>? transactionsList;
  List<TransactionsList>? requestsList;
  List<TransactionsList>? requestedList;
  String? txnEndDate;
  String? txnStartDate;

  TransactionHistoryResponseModel({this.responseStatus, this.result, this.transactionsList, this.txnEndDate, this.txnStartDate});

  TransactionHistoryResponseModel.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    if (json['transactionsList'] != null) {
      transactionsList = <TransactionsList>[];
      json['transactionsList'].forEach((v) {
        transactionsList!.add(TransactionsList.fromJson(v));
      });
    } else {
      transactionsList = <TransactionsList>[];
    }
    if (json['transactionsList'] != null) {
      requestedList = <TransactionsList>[];
      json['transactionsList'].forEach((v) {
        requestedList!.add(TransactionsList.fromJson(v));
      });
    } else {
      requestedList = <TransactionsList>[];
    }
    if (json['receiverTransactionsList'] != null) {
      requestsList = <TransactionsList>[];
      json['receiverTransactionsList'].forEach((v) {
        requestsList!.add(TransactionsList.fromJson(v));
      });
    } else {
      requestsList = <TransactionsList>[];
    }
    txnEndDate = json['txnEndDate'] ?? "";
    txnStartDate = json['txnStartDate'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    if (transactionsList != null) {
      data['transactionsList'] = transactionsList!.map((v) => v.toJson()).toList();
    }
    data['txnEndDate'] = txnEndDate;
    data['txnStartDate'] = txnStartDate;
    return data;
  }
}

class TransactionsList {
  String? amount;
  String? bookingId;
  String? createdOn;
  String? createdOn1;
  String? currentBalance;
  String? endTime;
  String? id;
  String? message;
  String? messageTransaction;
  String? orderId;
  String? paymentSource;
  String? receiverId;
  String? receiverImage;
  String? receiverName;
  String? receiverPhoneCode;
  String? receiverPhoneNumber;
  String? requestMessage;
  String? senderId;
  String? senderImage;
  String? senderName;
  String? senderPhoneCode;
  String? senderPhoneNumber;
  String? serviceType;
  String? startTime;
  int? status;
  String? succeededOn;
  String? totalAmount;
  String? transactionFee;
  String? transactionFeePercentage;
  String? transactionTime;
  String? transactionType;
  String? transactionUniqueId;

  TransactionsList(
      {this.amount,
      this.bookingId,
      this.createdOn,
      this.createdOn1,
      this.currentBalance,
      this.endTime,
      this.id,
      this.message,
      this.messageTransaction,
      this.orderId,
      this.paymentSource,
      this.receiverId,
      this.receiverImage,
      this.receiverName,
      this.receiverPhoneCode,
      this.receiverPhoneNumber,
      this.requestMessage,
      this.senderId,
      this.senderImage,
      this.senderName,
      this.senderPhoneCode,
      this.senderPhoneNumber,
      this.serviceType,
      this.startTime,
      this.status,
      this.succeededOn,
      this.totalAmount,
      this.transactionFee,
      this.transactionFeePercentage,
      this.transactionTime,
      this.transactionType,
      this.transactionUniqueId});

  TransactionsList.fromJson(Map<String, dynamic> json) {
    amount = json['amount'] ?? "";
    bookingId = json['bookingId'] ?? "";
    createdOn = json['createdOn'] ?? "";
    createdOn1 = json['createdOn1'] ?? "";
    currentBalance = json['currentBalance'] ?? "";
    endTime = json['endTime'] ?? "";
    id = json['id'] ?? "";
    message = json['message'] ?? "";
    messageTransaction = json['messageTransaction'] ?? "";
    orderId = json['orderId'] ?? "";
    paymentSource = json['paymentSource'] ?? "";
    receiverId = json['receiverId'] ?? "";
    receiverImage = json['receiverImage'] ?? "";
    receiverName = json['receiverName'] ?? "";
    receiverPhoneCode = json['receiverPhoneCode'] ?? "";
    receiverPhoneNumber = json['receiverPhoneNumber'] ?? "";
    requestMessage = json['requestMessage'] ?? "";
    senderId = json['senderId'] ?? "";
    senderImage = json['senderImage'] ?? "";
    senderName = json['senderName'] ?? "";
    senderPhoneCode = json['senderPhoneCode'] ?? "";
    senderPhoneNumber = json['senderPhoneNumber'] ?? "";
    serviceType = json['serviceType'] ?? "";
    startTime = json['startTime'] ?? "";
    status = json['status'] ?? 0;
    succeededOn = json['succeededOn'] ?? "";
    totalAmount = json['totalAmount'] ?? "";
    transactionFee = json['transactionFee'] ?? "";
    transactionFeePercentage = json['transactionFeePercentage'] ?? "";
    transactionTime = json['transactionTime'] ?? "";
    transactionType = json['transactionType'] ?? "";
    transactionUniqueId = json['transactionUniqueId'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['amount'] = amount;
    data['bookingId'] = bookingId;
    data['createdOn'] = createdOn;
    data['createdOn1'] = createdOn1;
    data['currentBalance'] = currentBalance;
    data['endTime'] = endTime;
    data['id'] = id;
    data['message'] = message;
    data['messageTransaction'] = messageTransaction;
    data['orderId'] = orderId;
    data['paymentSource'] = paymentSource;
    data['receiverId'] = receiverId;
    data['receiverImage'] = receiverImage;
    data['receiverName'] = receiverName;
    data['receiverPhoneCode'] = receiverPhoneCode;
    data['receiverPhoneNumber'] = receiverPhoneNumber;
    data['requestMessage'] = requestMessage;
    data['senderId'] = senderId;
    data['senderImage'] = senderImage;
    data['senderName'] = senderName;
    data['senderPhoneCode'] = senderPhoneCode;
    data['senderPhoneNumber'] = senderPhoneNumber;
    data['serviceType'] = serviceType;
    data['startTime'] = startTime;
    data['status'] = status;
    data['succeededOn'] = succeededOn;
    data['totalAmount'] = totalAmount;
    data['transactionFee'] = transactionFee;
    data['transactionFeePercentage'] = transactionFeePercentage;
    data['transactionTime'] = transactionTime;
    data['transactionType'] = transactionType;
    data['transactionUniqueId'] = transactionUniqueId;
    return data;
  }
}
