import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mone_me/models/add_bank_account_model.dart';
import 'package:mone_me/screens/home/notifier/home_notifier.dart';
import 'package:mone_me/screens/login/login_notifier/login_notifier.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/routes.dart';
import 'package:mone_me/utils/screen_config.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/utils/upper_case_formatter.dart';
import 'package:mone_me/utils/validators.dart';
import 'package:mone_me/widgets/adaptable_text.dart';
import 'package:mone_me/widgets/common_body.dart';
import 'package:mone_me/widgets/common_header.dart';
import 'package:mone_me/widgets/custom_toast.dart';
import 'package:mone_me/widgets/loader.dart';
import 'package:provider/provider.dart';

class AddBankAccountPage extends StatefulWidget {
  const AddBankAccountPage({Key? key}) : super(key: key);

  @override
  State<AddBankAccountPage> createState() => _AddBankAccountPageState();
}

class _AddBankAccountPageState extends State<AddBankAccountPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController accountNumberController = TextEditingController();
  TextEditingController reAccountNumberController = TextEditingController();
  TextEditingController routingNumberController = TextEditingController();
  TextEditingController bankNameController = TextEditingController();
  TextEditingController recipientNameController = TextEditingController();
  String userId = "", accountType = "";

  @override
  void initState() {
    userId = Provider.of<LoginNotifier>(context, listen: false).userId!;
    accountType = Provider.of<HomeNotifier>(context, listen: false).userData!.accountType!;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          body: Container(
            color: CupertinoColors.white,
            child: Stack(
              children: [
                SizedBox(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  child: Image.asset("assets/images/login_background_logo.png", fit: BoxFit.fill),
                ),
                Positioned(
                  child: Container(
                    height: ScreenConfig.height(context),
                    width: ScreenConfig.width(context),
                    color: Colors.transparent,
                    child: Column(
                      children: [
                        const CommonHeader(),
                        Expanded(
                          child: body(),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget body() {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.all(10),
      child: Row(
        children: [
          const CommonBody(),
          Expanded(
            child: Container(
              height: ScreenConfig.height(context),
              width: ScreenConfig.width(context),
              margin: const EdgeInsets.only(left: 3),
              child: addBankAccountWidget(context),
            ),
          )
        ],
      ),
    );
  }

  Widget addBankAccountWidget(BuildContext context) {
    return Card(
      shadowColor: cardShadow,
      shape: const RoundedRectangleBorder(borderRadius: detailsMainBorderRadius),
      elevation: 5,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: ScreenConfig.width(context),
            padding: const EdgeInsets.fromLTRB(10, 20, 20, 15),
            color: headerColor,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Material(
                  type: MaterialType.transparency,
                  child: IconButton(
                    icon: Image.asset('images/back.png', width: 17, height: 15),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
                Flexible(
                  child: AdaptableText(addBankAcc, style: showTextStyle, textMaxLines: 1),
                ),
              ],
            ),
          ),
          content(context),
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 15, 30, 20),
            child: Material(
              elevation: 5,
              borderRadius: payBorderRadius,
              child: ClipRRect(
                borderRadius: payBorderRadius,
                child: Container(
                  width: ScreenConfig.width(context),
                  decoration: const BoxDecoration(gradient: buttonGradient, borderRadius: payBorderRadius),
                  child: TextButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        if (accountNumberController.text.toString().trim() != reAccountNumberController.text.toString().trim()) {
                          showToast(accountNumbersNoMatch);
                        } else {
                          showLoadingDialog(context);
                          final viewModel = Provider.of<HomeNotifier>(context,listen: false);
                          var bankAccountModel = AddBankAccountModel(
                              userId: userId,
                              bankName: bankNameController.text.toString().trim(),
                              accountHolderName: recipientNameController.text.toString().trim(),
                              accountNumber: accountNumberController.text.toString().trim(),
                              routingNumber: routingNumberController.text.toString().trim(),
                              ifscCode: routingNumberController.text.toString().trim(),
                              accountType: accountType);
                          viewModel.addBankAccountAPI(bankAccountModel).then((value) {
                            Navigator.pop(context);
                            if (viewModel.addBankResponse != null) {
                              if (viewModel.addBankResponse!.responseStatus == 1) {
                                showToast(viewModel.addBankResponse!.result!);
                                Navigator.popAndPushNamed(context, AppRoutes.linkedBankAccounts);
                              } else {
                                showToast(viewModel.addBankResponse!.result!);
                              }
                            } else {
                              showToast(tryAgain);
                            }
                          });
                        }
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                      child: AdaptableText(proceed, style: paymentButtonsTextStyle, textMaxLines: 1),
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget content(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        controller: ScrollController(),
        physics: const ScrollPhysics(),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20, 10, 20, 25),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
                  child: TextFormField(
                    autofocus: true,
                    keyboardType: TextInputType.number,
                    controller: accountNumberController,
                    decoration: bankAccountDetailsDecorator,
                    style: textFormFieldStyle,
                    validator: minAccountNumberDigitsValidator,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    textInputAction: TextInputAction.next,
                    inputFormatters: <TextInputFormatter>[LengthLimitingTextInputFormatter(16), FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    controller: reAccountNumberController,
                    decoration: bankAccountDetailsDecorator.copyWith(hintText: reAccNumber),
                    style: textFormFieldStyle,
                    validator: minAccountNumberDigitsValidator,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    textInputAction: TextInputAction.next,
                    inputFormatters: <TextInputFormatter>[LengthLimitingTextInputFormatter(16), FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                  child: TextFormField(
                    keyboardType: TextInputType.text,
                    controller: routingNumberController,
                    decoration: bankAccountDetailsDecorator.copyWith(hintText: routingNumber),
                    style: textFormFieldStyle,
                    validator: minAccountNumberDigitsValidator,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    textCapitalization: TextCapitalization.characters,
                    textInputAction: TextInputAction.next,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                  child: TextFormField(
                    keyboardType: TextInputType.text,
                    controller: bankNameController,
                    decoration: bankAccountDetailsDecorator.copyWith(hintText: bankName),
                    inputFormatters: <TextInputFormatter>[CamelCaseTextFormatter()],
                    style: textFormFieldStyle,
                    validator: emptyTextValidator,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    textInputAction: TextInputAction.next,
                    textCapitalization: TextCapitalization.words,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                  child: TextFormField(
                    keyboardType: TextInputType.text,
                    controller: recipientNameController,
                    decoration: bankAccountDetailsDecorator.copyWith(hintText: recipientName),
                    inputFormatters: <TextInputFormatter>[CamelCaseTextFormatter()],
                    style: textFormFieldStyle,
                    validator: emptyTextValidator,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    textInputAction: TextInputAction.done,
                    textCapitalization: TextCapitalization.words,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
