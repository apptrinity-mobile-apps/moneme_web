import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mone_me/models/transaction_single_view_response_model.dart';
import 'package:mone_me/screens/home/notifier/home_notifier.dart';
import 'package:mone_me/screens/home/notifier/transactions_notifier.dart';
import 'package:mone_me/screens/login/login_notifier/login_notifier.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/routes.dart';
import 'package:mone_me/utils/screen_config.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/widgets/adaptable_text.dart';
import 'package:mone_me/widgets/common_body.dart';
import 'package:mone_me/widgets/common_header.dart';
import 'package:mone_me/widgets/list_loading_shimmer.dart';
import 'package:provider/provider.dart';

class TransferSuccessPage extends StatefulWidget {
  final String? transactionId;

  const TransferSuccessPage({Key? key, this.transactionId}) : super(key: key);

  @override
  State<TransferSuccessPage> createState() => _TransferSuccessPageState();
}

class _TransferSuccessPageState extends State<TransferSuccessPage> {
  late HomeNotifier viewModelHome;
  late TransactionsNotifier viewModelTransactions;
  String userId = "", _transactionId = "";
  TransactionsDetails? transactionDetails;
  bool isLoading = true;

  @override
  void initState() {
    userId = Provider.of<LoginNotifier>(context, listen: false).userId!;
    Future.delayed(Duration.zero, () {
      _transactionId = ModalRoute.of(context)!.settings.arguments! as String;
      final viewModelTransactions = Provider.of<TransactionsNotifier>(context, listen: false);
      viewModelTransactions.getSingleTransactionAPI(userId, _transactionId).then((value) {
        setState(() {
          if (viewModelTransactions.transactionDetails != null) {
            transactionDetails = viewModelTransactions.transactionDetails;
            isLoading = false;
          }
        });
      });
    });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    setState(() {});
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
          body: Container(
            color: CupertinoColors.white,
            child: Stack(
              children: [
                SizedBox(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  child: Image.asset("assets/images/login_background_logo.png", fit: BoxFit.fill),
                ),
                Positioned(
                  child: Container(
                    height: ScreenConfig.height(context),
                    width: ScreenConfig.width(context),
                    color: Colors.transparent,
                    child: Column(
                      children: [
                        const CommonHeader(),
                        Expanded(
                          child: body(),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget body() {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.all(10),
      child: Row(
        children: [
          const CommonBody(),
          Expanded(
            child: Container(
              height: ScreenConfig.height(context),
              width: ScreenConfig.width(context),
              margin: const EdgeInsets.only(left: 3),
              child: walletSuccessWidget(context),
            ),
          )
        ],
      ),
    );
  }

  Widget walletSuccessWidget(BuildContext context) {
    return Card(
      shadowColor: cardShadow,
      shape: const RoundedRectangleBorder(borderRadius: detailsMainBorderRadius),
      elevation: 5,
      child: Column(
        children: [
          Container(
            width: ScreenConfig.width(context),
            padding: const EdgeInsets.fromLTRB(20, 20, 20, 15),
            // color: headerColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Material(
                  type: MaterialType.transparency,
                  child: IconButton(
                    icon: Image.asset('images/back.png', width: 17, height: 15),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
              ],
            ),
          ),
          content(context),
        ],
      ),
    );
  }

  Widget content(BuildContext context) {
    return Expanded(
      child: isLoading
          ? listLoadingShimmer()
          : LayoutBuilder(builder: (context, constraints) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Container(
                    width: ScreenConfig.width(context),
                    alignment: Alignment.center,
                    padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                    margin: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 5, 0, 10),
                          child: Image.asset("assets/images/payment_success.png", height: 70, filterQuality: FilterQuality.medium),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(0),
                          child: AdaptableText(moneyAddedWalletSuccess, style: addMoneyWalletTextStyle, textMaxLines: 1),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                          child: AdaptableText("\u0024${commaSeparatedAmount.format(double.parse(transactionDetails!.amount!))}",
                              style: walletAmountTextStyle, textMaxLines: 1),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: ScreenConfig.width(context),
                    color: headerColor,
                    alignment: Alignment.center,
                    padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                    margin: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                    child: RichText(
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      text: TextSpan(
                        text: "",
                        children: <TextSpan>[
                          TextSpan(text: updatedBalance, style: walletTransactionBalanceTextStyle),
                          TextSpan(text: " - ", style: walletTransactionBalanceTextStyle),
                          TextSpan(
                              text: "\u0024${commaSeparatedAmount.format(double.parse(transactionDetails!.currentBalance!))}",
                              style: walletTransactionBalanceAmountTextStyle),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: ScreenConfig.width(context),
                    alignment: Alignment.center,
                    padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                    margin: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        // Flexible(child: AdaptableText("$orderId: ${details.orderId}", style: walletTransactionDetailsTextStyle, textMaxLines: 2),),
                        Flexible(
                          child: AdaptableText("$transactionId: ${transactionDetails!.transactionUniqueId}",
                              style: walletTransactionDetailsTextStyle, textMaxLines: 2),
                        ),
                        Flexible(
                          child: AdaptableText(transactionDetails!.succeededOn!, style: walletTransactionDetailsTextStyle, textMaxLines: 1),
                        ),
                      ],
                    ),
                  )
                ],
              );
            }),
    );
  }

  Widget actions(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 5, 20, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Flexible(
            child: Container(
              decoration: const BoxDecoration(color: transferredIconBackground, borderRadius: payBorderRadius),
              child: TextButton(
                onPressed: () {
                  Navigator.pushNamedAndRemoveUntil(context, AppRoutes.home, (routes) => false);
                },
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                  child: AdaptableText(home, style: paymentButtonsTextStyle, textMaxLines: 1),
                ),
              ),
            ),
          ),
          Flexible(
            child: Container(
              decoration: const BoxDecoration(color: receivedIconBackground, borderRadius: payBorderRadius),
              child: TextButton(
                onPressed: () {},
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                  child: AdaptableText(needHelp, style: paymentButtonsTextStyle, textMaxLines: 1),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<bool> _onBackPressed() async {
    await Navigator.pushNamedAndRemoveUntil(context, AppRoutes.home, (routes) => false);
    return true;
  }
}
