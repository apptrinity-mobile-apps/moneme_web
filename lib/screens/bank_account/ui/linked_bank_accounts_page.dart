import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mone_me/models/bank_account_model.dart';
import 'package:mone_me/screens/home/notifier/home_notifier.dart';
import 'package:mone_me/screens/login/login_notifier/login_notifier.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/routes.dart';
import 'package:mone_me/utils/screen_config.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/widgets/adaptable_text.dart';
import 'package:mone_me/widgets/common_body.dart';
import 'package:mone_me/widgets/common_header.dart';
import 'package:mone_me/widgets/custom_toast.dart';
import 'package:mone_me/widgets/list_loading_shimmer.dart';
import 'package:mone_me/widgets/loader.dart';
import 'package:provider/provider.dart';

class LinkedBankAccountsPage extends StatefulWidget {
  const LinkedBankAccountsPage({Key? key}) : super(key: key);

  @override
  State<LinkedBankAccountsPage> createState() => _LinkedBankAccountsPageState();
}

class _LinkedBankAccountsPageState extends State<LinkedBankAccountsPage> {
  int selectedBankPos = -1;
  bool banksListLoading = true;
  String userId = "";
  Bank? selectedBank;

  @override
  void initState() {
    userId = Provider.of<LoginNotifier>(context, listen: false).userId!;
    getAllBanks();
    super.initState();
  }

  getAllBanks() {
    setState(() {
      banksListLoading = true;
    });
    Provider.of<HomeNotifier>(context, listen: false).getAllBankAccountsAPI(userId).then((value) {
      banksListLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          body: Container(
            color: CupertinoColors.white,
            child: Stack(
              children: [
                SizedBox(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  child: Image.asset("assets/images/login_background_logo.png", fit: BoxFit.fill),
                ),
                Positioned(
                  child: Container(
                    height: ScreenConfig.height(context),
                    width: ScreenConfig.width(context),
                    color: Colors.transparent,
                    child: Column(
                      children: [
                        const CommonHeader(),
                        Expanded(
                          child: body(),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget body() {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.all(10),
      child: Row(
        children: [
          const CommonBody(),
          Expanded(
            child: Container(
              height: ScreenConfig.height(context),
              width: ScreenConfig.width(context),
              margin: const EdgeInsets.only(left: 3),
              child: moneyToBankWidget(context),
            ),
          )
        ],
      ),
    );
  }

  Widget moneyToBankWidget(BuildContext context) {
    return Card(
      shadowColor: cardShadow,
      shape: const RoundedRectangleBorder(borderRadius: detailsMainBorderRadius),
      elevation: 5,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: ScreenConfig.width(context),
            padding: const EdgeInsets.fromLTRB(20, 20, 20, 15),
            color: headerColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AdaptableText(sendMoneyToBank, style: showTextStyle, textMaxLines: 1),
                RichText(
                  text: TextSpan(text: "", children: <TextSpan>[
                    TextSpan(text: availableBalance, style: walletAvailableBalanceHeaderTextStyle),
                    TextSpan(
                      text: " \u0024${commaSeparatedAmount.format(Provider.of<HomeNotifier>(context, listen: true).getCurrentAmount!)}",
                      style: walletAvailableBalanceHeaderTextStyle.copyWith(
                        fontWeight: FontWeight.w700,
                        color: totalIconBackground,
                      ),
                    ),
                  ]),
                )
              ],
            ),
          ),
          content(context),
          selectedBankPos != -1
              ? Padding(
                  padding: const EdgeInsets.fromLTRB(30, 15, 30, 20),
                  child: Material(
                    elevation: 5,
                    borderRadius: payBorderRadius,
                    child: ClipRRect(
                      borderRadius: payBorderRadius,
                      child: Container(
                        width: ScreenConfig.width(context),
                        decoration: const BoxDecoration(gradient: buttonGradient, borderRadius: payBorderRadius),
                        child: TextButton(
                          onPressed: () {
                            Navigator.pushNamed(context, AppRoutes.transferToBank, arguments: selectedBank);
                          },
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                            child: AdaptableText(proceed, style: paymentButtonsTextStyle, textMaxLines: 1),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              : const SizedBox(),
        ],
      ),
    );
  }

  Widget content(BuildContext context) {
    var banksList = Provider.of<HomeNotifier>(context, listen: true).userBanksList();
    return Expanded(
      child: SingleChildScrollView(
        controller: ScrollController(),
        physics: const ScrollPhysics(),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20, 10, 20, 25),
          child: Column(
            children: [
              Container(
                color: profileBackground,
                margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                child: TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, AppRoutes.addBankAccount).then((value) {
                      getAllBanks();
                    });
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Flexible(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                            child: Image.asset("assets/images/bank_account_icon.png", height: 20, width: 20, color: transferredIconBackground),
                          ),
                        ),
                        Flexible(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                            child: AdaptableText(enterBankDetails, style: addNewBankTextStyle, textMaxLines: 1),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 5),
                  child: AdaptableText(savedAccounts, style: addNewBankTextStyle, textMaxLines: 1),
                ),
              ),
              banksList.isNotEmpty
                  ? ListView.separated(
                      shrinkWrap: true,
                      itemCount: banksList.length,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            setState(() {
                              if (selectedBankPos == -1) {
                                selectedBankPos = index;
                                selectedBank = banksList[index]!;
                              } else if (selectedBankPos == index) {
                                selectedBankPos = -1;
                                selectedBank = null;
                              } else {
                                selectedBankPos = index;
                                selectedBank = banksList[index]!;
                              }
                            });
                          },
                          child: Container(
                            color: selectedBankPos != index ? headerColor : receivedIconBackground.withOpacity(0.5),
                            padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(right: 10),
                                  width: 40,
                                  height: 40,
                                  decoration: const BoxDecoration(
                                      gradient: LinearGradient(
                                        begin: Alignment.topCenter,
                                        end: Alignment.bottomCenter,
                                        colors: <Color>[buttonGradientStart, buttonGradientEnd],
                                      ),
                                      shape: BoxShape.circle),
                                  child: Center(
                                    child: AdaptableText(banksList[index]!.bankName![0].toUpperCase(),
                                        style: addNewBankTextStyle.copyWith(
                                            letterSpacing: 1.19, fontWeight: FontWeight.w600, color: CupertinoColors.white, fontSize: 15),
                                        textMaxLines: 1,
                                        textOverflow: TextOverflow.ellipsis),
                                  ),
                                ),
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                                        child: AdaptableText(banksList[index]!.accountHolderName!,
                                            style: addNewBankTextStyle.copyWith(
                                                letterSpacing: 1.19,
                                                fontWeight: FontWeight.w600,
                                                color: selectedBankPos != index ? nameText : CupertinoColors.white,
                                                fontSize: 14),
                                            textMaxLines: 1,
                                            textOverflow: TextOverflow.ellipsis),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(5, 0, 5, 3),
                                        child: AdaptableText(
                                            "${banksList[index]!.bankName} A/c xxxx${banksList[index]!.accountNumber!.substring(banksList[index]!.accountNumber!.length - 4)}",
                                            style: addNewBankTextStyle.copyWith(
                                                letterSpacing: 1,
                                                fontWeight: FontWeight.w400,
                                                color: selectedBankPos != index ? profilePhoneText : CupertinoColors.white,
                                                fontSize: 13),
                                            textMaxLines: 1,
                                            textOverflow: TextOverflow.ellipsis),
                                      ),
                                    ],
                                  ),
                                ),
                                FittedBox(
                                  child: Material(
                                    type: MaterialType.transparency,
                                    child: IconButton(
                                      onPressed: () {
                                        showLoadingDialog(context);
                                        final viewModel = Provider.of<HomeNotifier>(context, listen: false);
                                        viewModel.deleteBankAccountAPI(userId, banksList[index]!.bankId!).then((value) {
                                          if (viewModel.deleteBankResponse != null) {
                                            if (viewModel.deleteBankResponse!.responseStatus == 1) {
                                              showToast(viewModel.deleteBankResponse!.result!);
                                              getAllBanks();
                                            } else {
                                              showToast(viewModel.deleteBankResponse!.result!);
                                            }
                                          } else {
                                            showToast(tryAgain);
                                          }
                                        });
                                      },
                                      icon: const Icon(
                                        Icons.delete_rounded,
                                        size: 20,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      },
                      separatorBuilder: (ctx, index) {
                        return const Divider(
                          height: 5,
                          color: Colors.transparent,
                        );
                      },
                    )
                  : banksListLoading
                      ? listLoadingShimmer()
                      : Padding(
                          padding: const EdgeInsets.only(top: 50, bottom: 50),
                          child: Center(
                            child: AdaptableText(noSavedAccounts, style: showTextStyle),
                          ),
                        ),
            ],
          ),
        ),
      ),
    );
  }
}
