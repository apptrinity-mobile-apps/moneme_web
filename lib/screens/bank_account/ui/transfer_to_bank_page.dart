import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mone_me/models/bank_account_model.dart';
import 'package:mone_me/models/wallet_to_bank_model.dart';
import 'package:mone_me/screens/home/notifier/home_notifier.dart';
import 'package:mone_me/screens/home/notifier/transactions_notifier.dart';
import 'package:mone_me/screens/login/login_notifier/login_notifier.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/decimal_input_format.dart';
import 'package:mone_me/utils/routes.dart';
import 'package:mone_me/utils/screen_config.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/utils/validators.dart';
import 'package:mone_me/widgets/adaptable_text.dart';
import 'package:mone_me/widgets/common_body.dart';
import 'package:mone_me/widgets/common_header.dart';
import 'package:mone_me/widgets/custom_toast.dart';
import 'package:mone_me/widgets/loader.dart';
import 'package:provider/provider.dart';

class TransferToBankPage extends StatefulWidget {
  final Bank? bankDetails;

  const TransferToBankPage({Key? key, this.bankDetails}) : super(key: key);

  @override
  State<TransferToBankPage> createState() => _TransferToBankPageState();
}

class _TransferToBankPageState extends State<TransferToBankPage> {
  late HomeNotifier viewModelHome;
  late TransactionsNotifier viewModelTransactions;
  final _formKey = GlobalKey<FormState>();
  TextEditingController amountController = TextEditingController();
  TextEditingController messageController = TextEditingController();
  String finalAmount = "0.00", transactionFees = "0.00", userId = "";
  Bank? bankDetails;

  @override
  void initState() {
    Provider.of<TransactionsNotifier>(context, listen: false).getTransactionFeesAPI();
    userId = Provider.of<LoginNotifier>(context, listen: false).userId!;
    amountController.addListener(_perCalculation);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    setState(() {});
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    viewModelTransactions = Provider.of<TransactionsNotifier>(context, listen: true);
    bankDetails = ModalRoute.of(context)!.settings.arguments as Bank;
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          body: Container(
            color: CupertinoColors.white,
            child: Stack(
              children: [
                SizedBox(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  child: Image.asset("assets/images/login_background_logo.png", fit: BoxFit.fill),
                ),
                Positioned(
                  child: Container(
                    height: ScreenConfig.height(context),
                    width: ScreenConfig.width(context),
                    color: Colors.transparent,
                    child: Column(
                      children: [
                        const CommonHeader(),
                        Expanded(
                          child: body(),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget body() {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.all(10),
      child: Row(
        children: [
          const CommonBody(),
          Expanded(
            child: Container(
              height: ScreenConfig.height(context),
              width: ScreenConfig.width(context),
              margin: const EdgeInsets.only(left: 3),
              child: transferToBankWidget(context),
            ),
          )
        ],
      ),
    );
  }

  Widget transferToBankWidget(BuildContext context) {
    return Card(
      shadowColor: cardShadow,
      shape: const RoundedRectangleBorder(borderRadius: detailsMainBorderRadius),
      elevation: 5,
      child: Column(
        children: [
          Container(
            width: ScreenConfig.width(context),
            padding: const EdgeInsets.fromLTRB(20, 20, 20, 15),
            color: headerColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Material(
                  type: MaterialType.transparency,
                  child: IconButton(
                    icon: Image.asset('images/back.png', width: 17, height: 15),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
              ],
            ),
          ),
          content(context),
          actions(context)
        ],
      ),
    );
  }

  Widget content(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 5, 20, 25),
        child: ListView(
          shrinkWrap: true,
          children: [
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 15, 10, 0),
                child: AdaptableText(bankDetails!.accountHolderName!,
                    style: addNewBankTextStyle.copyWith(letterSpacing: 1.19, fontWeight: FontWeight.w600, color: nameText, fontSize: 17),
                    textMaxLines: 1,
                    textOverflow: TextOverflow.ellipsis),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: AdaptableText(
                    "${bankDetails!.bankName} A/c xxxx${bankDetails!.accountNumber!.substring(bankDetails!.accountNumber!.length - 4)}",
                    style: addNewBankTextStyle.copyWith(letterSpacing: 1, fontWeight: FontWeight.w400, color: profilePhoneText, fontSize: 16),
                    textMaxLines: 1,
                    textOverflow: TextOverflow.ellipsis),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 5),
              child: RichText(
                text: TextSpan(text: "", children: <TextSpan>[
                  TextSpan(text: availableBalance, style: walletAvailableBalanceHeaderTextStyle),
                  TextSpan(
                      text: " \u0024${commaSeparatedAmount.format(Provider.of<HomeNotifier>(context, listen: true).getCurrentAmount!)}",
                      style: walletAvailableBalanceHeaderTextStyle.copyWith(
                        fontWeight: FontWeight.w700,
                        color: totalIconBackground,
                      )),
                ]),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Form(
                key: _formKey,
                child: TextFormField(
                    onChanged: (string) {
                      String removeComma = string;
                      if (string.contains(',')) {
                        removeComma = string.replaceAll(',', '');
                      }
                      int x;
                      try {
                        x = int.parse(removeComma);
                        if (x > maxAmount) {
                          x = maxAmount;
                        }
                        String sendAmount = formatNumberForAdding(x.toString().replaceAll(',', ''));
                        amountController.value = TextEditingValue(
                          text: sendAmount.toString(),
                          selection: TextSelection.collapsed(offset: sendAmount.length),
                        );
                      } catch (error) {
                        debugPrint(error.toString());
                      }
                    },
                    controller: amountController,
                    autofocus: true,
                    textInputAction: TextInputAction.done,
                    maxLines: 1,
                    keyboardType: TextInputType.number,
                    inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                    decoration: InputDecoration(
                        prefixText: "\u0024",
                        fillColor: CupertinoColors.white,
                        filled: true,
                        border: OutlineInputBorder(
                          borderSide: const BorderSide(color: totalIconBackground, width: 1),
                          borderRadius: BorderRadius.circular(0),
                        ),
                        isDense: true,
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(color: totalIconBackground, width: 1),
                          borderRadius: BorderRadius.circular(0),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: const BorderSide(color: totalIconBackground, width: 1),
                          borderRadius: BorderRadius.circular(0),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.redAccent, width: 1),
                          borderRadius: BorderRadius.circular(0),
                        ),
                        focusedErrorBorder: OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.redAccent, width: 1),
                          borderRadius: BorderRadius.circular(0),
                        ),
                        contentPadding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
                        hintStyle: TextStyle(
                            letterSpacing: 1.56, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: nameText.withOpacity(0.5), fontSize: 15),
                        hintText: '0.00'),
                    style: const TextStyle(letterSpacing: 1.56, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: nameText, fontSize: 15),
                    validator: amountValidator,
                    autovalidateMode: AutovalidateMode.onUserInteraction),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 5, 10, 0),
              child: Text(
                '$transactionFee \u0024$transactionFees',
                style: addMoneyTextStyle.copyWith(fontWeight: FontWeight.w500),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 5),
              child: Text(
                '$totalAmount \u0024$finalAmount',
                style: addMoneyTextStyle.copyWith(color: totalIconBackground, fontWeight: FontWeight.w500),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: TextFormField(
                controller: messageController,
                textInputAction: TextInputAction.newline,
                minLines: 3,
                maxLines: 7,
                keyboardType: TextInputType.multiline,
                decoration: InputDecoration(
                    fillColor: CupertinoColors.white,
                    filled: true,
                    border: OutlineInputBorder(
                      borderSide: const BorderSide(color: totalIconBackground, width: 1),
                      borderRadius: BorderRadius.circular(0),
                    ),
                    isDense: true,
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: totalIconBackground, width: 1),
                      borderRadius: BorderRadius.circular(0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: totalIconBackground, width: 1),
                      borderRadius: BorderRadius.circular(0),
                    ),
                    errorBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.redAccent, width: 1),
                      borderRadius: BorderRadius.circular(0),
                    ),
                    focusedErrorBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.redAccent, width: 1),
                      borderRadius: BorderRadius.circular(0),
                    ),
                    contentPadding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
                    hintStyle: TextStyle(
                        letterSpacing: 1.56, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: nameText.withOpacity(0.5), fontSize: 15),
                    hintText: messageHint),
                style: const TextStyle(letterSpacing: 1.56, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: nameText, fontSize: 15),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget actions(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 5, 20, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 5, 30, 5),
            child: AdaptableText(moneyWillBeTransferredToBank, style: headerTextStyle),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 5, 20, 10),
            child: Material(
              elevation: 5,
              borderRadius: payBorderRadius,
              child: ClipRRect(
                borderRadius: payBorderRadius,
                child: Container(
                  width: ScreenConfig.width(context),
                  decoration: const BoxDecoration(gradient: buttonGradient, borderRadius: payBorderRadius),
                  child: TextButton(
                    onPressed: () {
                      // Navigator.pushNamed(context, AppRoutes.bankTransferSuccess, arguments: "62ab2276a3e5879f183d1051");
                      if (_formKey.currentState!.validate()) {
                        showLoadingDialog(context);
                        if (amountController.text.contains(',')) {
                          amountController.text =
                              amountController.text.replaceAll(',', '');
                        }
                        if (transactionFees.contains(',')) {
                          transactionFees =
                              transactionFees.replaceAll(',', '');
                        }
                        if (finalAmount.contains(',')) {
                          finalAmount = finalAmount.replaceAll(',', '');
                        }
                        var walletToBankModel = WalletToBankModel(
                          userId: userId,
                          amount: double.parse(amountController.text.trim().toString()),
                          message: messageController.text.trim().toString(),
                          messageTransaction: "transaction",
                          paymentSource: "wallet",
                          transactionType: "debit",
                          transactionFee: double.parse(transactionFees),
                          transactionFeePercentage: viewModelTransactions.getWithdrawFee,
                          bankAccountId: bankDetails!.bankId!,
                          senderId: userId,
                        );
                        final viewModel = Provider.of<TransactionsNotifier>(context, listen: false);
                        viewModel.walletToBankAPI(walletToBankModel).then((value) {
                          Navigator.pop(context);
                          if (viewModel.walletToBankResponse != null) {
                            if (viewModel.walletToBankResponse!.responseStatus == 1) {
                              showToast(viewModel.walletToBankResponse!.result!);
                              Navigator.pushNamed(context, AppRoutes.bankTransferSuccess, arguments: viewModel.walletToBankResponse!.transactionId!);
                            } else {
                              showToast(viewModel.walletToBankResponse!.result!);
                            }
                          } else {
                            showToast(tryAgain);
                          }
                        });
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                      child: AdaptableText(pay, style: paymentButtonsTextStyle, textMaxLines: 1),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _perCalculation() {
    debugPrint(amountController.text.toString());
    if (amountController.text == "") {
      setState(() {
        transactionFees = "0.00";
        finalAmount = "0.00";
      });
    } else {
      setState(() {
        String removeComma = amountController.text;
        if (amountController.text.contains(',')) {
          removeComma = amountController.text.replaceAll(',', '');
        }
        double val = double.tryParse(removeComma)! * viewModelTransactions.getDepositFee;
        transactionFees = (val / 100.0).toStringAsFixed(2);
        finalAmount = (double.parse(removeComma) + double.parse(transactionFees.toString())).toStringAsFixed(2);
        transactionFees = commaSeparatedAmount.format(double.parse(transactionFees));
        finalAmount = commaSeparatedAmount.format(double.parse(finalAmount));
      });
    }
  }
}
