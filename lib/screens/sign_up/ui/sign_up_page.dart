import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:mone_me/models/countries_response_model.dart';
import 'package:mone_me/models/languages_response_model.dart';
import 'package:mone_me/models/user_sign_up_model.dart';
import 'package:mone_me/screens/login/login_notifier/login_notifier.dart';
import 'package:mone_me/utils/routes.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/inputphone_number_formats.dart';
import 'package:mone_me/utils/screen_config.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/utils/upper_case_formatter.dart';
import 'package:mone_me/utils/validators.dart';
import 'package:mone_me/widgets/adaptable_text.dart';
import 'package:mone_me/widgets/country_picker_dropdown.dart';
import 'package:mone_me/widgets/custom_toast.dart';
import 'package:mone_me/widgets/loader.dart';
import 'package:mone_me/widgets/phone_dropdown.dart';
import 'package:provider/provider.dart';
import 'package:pinput/pinput.dart';
import 'package:url_launcher/url_launcher.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  late LoginNotifier viewModel;
  final _formKey = GlobalKey<FormState>();
  final _languagesDropdownKey = GlobalKey<FormFieldState>();
  final _countriesDropdownKey = GlobalKey<FormFieldState>();
  final _pinPutKey = GlobalKey<FormState>();
  FocusNode nameFocus = FocusNode();
  FocusNode businessNameFocus = FocusNode();
  TextEditingController businessNameController = TextEditingController();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  TextEditingController otpController = TextEditingController();
  bool isShowPassword = false, isShowConfirmPassword = false, showOtpPage = false;
  final defaultPinTheme = PinTheme(
    width: 56,
    height: 60,
    margin: const EdgeInsets.fromLTRB(5, 0, 5, 0),
    textStyle: pinPutTextStyle,
    decoration: BoxDecoration(color: spinnerDropDown.withOpacity(0.5), borderRadius: payBorderRadius),
  );
  List<DropdownMenuItem<CountriesList>> countriesMenuItems = [];
  List<DropdownMenuItem<LanguagesList>> languagesMenuItems = [];
  CountriesList countriesInitialValue = CountriesList(name: selectCountry);
  LanguagesList languagesInitialValue = LanguagesList(name: selectLanguage);
  String phoneCode = "", input_phonenumber_hint = "";
  int accountType = 0;
  final int _personal = 0, _business = 1;

  @override
  void initState() {
    phoneCode = '+1';
    input_phonenumber_hint = inputPhoneNumberFormatByCountry('9999999999', phoneCode);
    countriesList();
    languagesList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    viewModel = Provider.of<LoginNotifier>(context, listen: true);
    return SafeArea(
      child: WillPopScope(
        onWillPop: onBackPressed,
        child: Scaffold(
          backgroundColor: CupertinoColors.white,
          body: Row(
            children: [
              Expanded(
                child: Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  decoration: const BoxDecoration(
                    image: DecorationImage(image: AssetImage("assets/images/login_background_logo.png"), fit: BoxFit.fill),
                  ),
                  child: Center(
                    child: SingleChildScrollView(
                      controller: ScrollController(),
                      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                        Center(
                          child: Image.asset("assets/images/app_logo_vertical.png",
                              height: ScreenConfig.height(context) / 2, width: ScreenConfig.width(context) / 2),
                        ),
                        const SizedBox(
                          height: 200,
                        ),
                        SizedBox(
                          height: 100,
                          child: ListView.builder(
                              controller: ScrollController(),
                              scrollDirection: Axis.horizontal,
                              shrinkWrap: true,
                              itemCount: viewModel.getSocialMediasList!.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  margin: const EdgeInsets.only(right: 10),
                                  child: InkWell(
                                    onTap: () {
                                      final Uri uri = Uri.parse(viewModel.getSocialMediasList![index].urlLink!);
                                      launchUrl(uri);
                                    },
                                    child: Image.network(
                                      viewModel.getSocialMediasList![index].image!,
                                      width: 40,
                                      height: 40,
                                    ),
                                  ),
                                );
                              }),
                        ),
                      ]),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  decoration: const BoxDecoration(
                    image: DecorationImage(image: AssetImage("assets/images/login_background_details.png"), fit: BoxFit.fill),
                  ),
                  child: Center(
                    child: SingleChildScrollView(
                      controller: ScrollController(),
                      child: Container(
                        width: ScreenConfig.width(context) / 3,
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: viewModel.isOtpPageShowing
                            ? Column(
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [welcomeHeadersOtp(), otpField(), submitOrResend()],
                              )
                            : Column(
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [welcomeHeaders(), createUserDetailsFields(), submitOrSignIn()],
                              ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget welcomeHeaders() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
            child: AdaptableText(createAccount, style: welcomeTextStyle, textMaxLines: 1),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 5),
            child: AdaptableText(signUpToContinue, style: signInContinueTextStyle, textMaxLines: 1),
          ),
        ],
      ),
    );
  }

  Widget createUserDetailsFields() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Flexible(
                child: InkWell(
                  onTap: () {
                    setState(() {
                      accountType = _personal;
                      _formKey.currentState!.reset();
                      _languagesDropdownKey.currentState!.reset();
                      _countriesDropdownKey.currentState!.reset();
                      nameFocus.requestFocus();
                    });
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: Row(mainAxisAlignment: MainAxisAlignment.start, mainAxisSize: MainAxisSize.min, children: <Widget>[
                      Radio<int>(
                        value: _personal,
                        groupValue: accountType,
                        onChanged: (value) {
                          setState(() {
                            accountType = value!;
                            _formKey.currentState!.reset();
                            _languagesDropdownKey.currentState!.reset();
                            _countriesDropdownKey.currentState!.reset();
                            nameFocus.requestFocus();
                          });
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: AdaptableText(
                          personal,
                          style: signInContinueTextStyle,
                        ),
                      ),
                    ]),
                  ),
                ),
              ),
              Flexible(
                child: InkWell(
                  onTap: () {
                    setState(() {
                      accountType = _business;
                      _formKey.currentState!.reset();
                      _languagesDropdownKey.currentState!.reset();
                      _countriesDropdownKey.currentState!.reset();
                      businessNameFocus.requestFocus();
                    });
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: Row(mainAxisAlignment: MainAxisAlignment.start, mainAxisSize: MainAxisSize.min, children: <Widget>[
                      Radio<int>(
                        value: _business,
                        groupValue: accountType,
                        onChanged: (value) {
                          setState(() {
                            accountType = value!;
                            _formKey.currentState!.reset();
                            _languagesDropdownKey.currentState!.reset();
                            _countriesDropdownKey.currentState!.reset();
                            businessNameFocus.requestFocus();
                          });
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: AdaptableText(
                          business,
                          style: signInContinueTextStyle,
                        ),
                      ),
                    ]),
                  ),
                ),
              ),
            ],
          ),
          Form(
            key: _formKey,
            child: Column(
              children: [
                accountType == 1
                    ? Container(
                        margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                        child: TextFormField(
                            autofocus: true,
                            focusNode: businessNameFocus,
                            controller: businessNameController,
                            textInputAction: TextInputAction.next,
                            inputFormatters: <TextInputFormatter>[CamelCaseTextFormatter()],
                            maxLines: 1,
                            keyboardType: TextInputType.name,
                            decoration: firstNameDecoration.copyWith(hintText: companyName),
                            style: textFormFieldStyle,
                            validator: emptyTextValidator,
                            autovalidateMode: AutovalidateMode.onUserInteraction),
                      )
                    : const SizedBox(),
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: TextFormField(
                      autofocus: true,
                      focusNode: nameFocus,
                      controller: firstNameController,
                      textInputAction: TextInputAction.next,
                      inputFormatters: <TextInputFormatter>[CamelCaseTextFormatter()],
                      maxLines: 1,
                      keyboardType: TextInputType.name,
                      decoration: accountType == 0 ? firstNameDecoration : firstNameDecoration.copyWith(hintText: contactPersonFirstName),
                      style: textFormFieldStyle,
                      validator: emptyTextValidator,
                      autovalidateMode: AutovalidateMode.onUserInteraction),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: TextFormField(
                      controller: lastNameController,
                      textInputAction: TextInputAction.next,
                      inputFormatters: <TextInputFormatter>[CamelCaseTextFormatter()],
                      maxLines: 1,
                      keyboardType: TextInputType.name,
                      decoration: accountType == 0 ? lastNameDecoration : lastNameDecoration.copyWith(hintText: contactPersonLastName),
                      style: textFormFieldStyle,
                      validator: emptyTextValidator,
                      autovalidateMode: AutovalidateMode.onUserInteraction),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: TextFormField(
                      controller: emailController,
                      textInputAction: TextInputAction.next,
                      maxLines: 1,
                      keyboardType: TextInputType.emailAddress,
                      decoration: emailDecoration,
                      style: textFormFieldStyle,
                      validator: emailValidator,
                      autovalidateMode: AutovalidateMode.onUserInteraction),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: TextFormField(
                      controller: phoneController,
                      textInputAction: TextInputAction.next,
                      maxLines: 1,
                      keyboardType: TextInputType.number,
                      onChanged: (_) {
                        phoneController.text = inputPhoneNumberFormatByCountry(phoneController.text, phoneCode);
                        phoneController.value = TextEditingValue(
                          text: phoneController.text.toString(),
                          selection: TextSelection.collapsed(offset: phoneController.text.length),
                        );
                      },
                      decoration: InputDecoration(
                        fillColor: CupertinoColors.white,
                        filled: true,
                        border: border,
                        isDense: true,
                        enabledBorder: border,
                        focusedBorder: border,
                        errorBorder: errorBorder,
                        focusedErrorBorder: errorBorder,
                        contentPadding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                        hintStyle: textFormFieldHintStyle,
                        hintText: input_phonenumber_hint,
                        prefixIcon: Container(
                          width: 90,
                          alignment: Alignment.center,
                          margin: const EdgeInsets.fromLTRB(15, 0, 0, 0),
                          child: IntrinsicHeight(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Flexible(
                                  child: Provider.of<LoginNotifier>(context, listen: true).getCountriesList().isNotEmpty
                                      ? countriesDropDown()
                                      : const SizedBox(),
                                ),
                                // const VerticalDivider(thickness: 1.5, color: spinnerDropDown)
                              ],
                            ),
                          ),
                        ),
                      ),
                      style: textFormFieldStyle,
                      validator: phoneValidator,
                      autovalidateMode: AutovalidateMode.onUserInteraction),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: TextFormField(
                      maxLines: 1,
                      controller: passwordController,
                      textInputAction: TextInputAction.next,
                      obscureText: isShowPassword ? false : true,
                      obscuringCharacter: '*',
                      decoration: InputDecoration(
                        fillColor: CupertinoColors.white,
                        filled: true,
                        border: border,
                        isDense: true,
                        enabledBorder: border,
                        focusedBorder: border,
                        errorBorder: errorBorder,
                        focusedErrorBorder: errorBorder,
                        contentPadding: textFormFieldPadding,
                        hintStyle: textFormFieldHintStyle,
                        hintText: password,
                        prefixIcon: Container(
                          width: 90,
                          alignment: Alignment.center,
                          margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                          // child: Image.asset("assets/images/email_icon.png", height: 15, width: 20),
                          child: const Icon(Icons.lock_outline, size: 32, color: defaultText),
                        ),
                        suffixIcon: Container(
                          margin: const EdgeInsets.fromLTRB(10, 0, 20, 0),
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                isShowPassword = !isShowPassword;
                              });
                            },
                            child: isShowPassword
                                ? const Icon(Icons.visibility_off_outlined, size: 25)
                                : const Icon(Icons.visibility_outlined, size: 25),
                          ),
                        ),
                      ),
                      style: textFormFieldStyle,
                      validator: passwordValidator,
                      autovalidateMode: AutovalidateMode.onUserInteraction),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: TextFormField(
                      maxLines: 1,
                      controller: confirmPasswordController,
                      textInputAction: TextInputAction.next,
                      obscureText: isShowConfirmPassword ? false : true,
                      obscuringCharacter: '*',
                      decoration: InputDecoration(
                        fillColor: CupertinoColors.white,
                        filled: true,
                        border: border,
                        isDense: true,
                        enabledBorder: border,
                        focusedBorder: border,
                        errorBorder: errorBorder,
                        focusedErrorBorder: errorBorder,
                        contentPadding: textFormFieldPadding,
                        hintStyle: textFormFieldHintStyle,
                        hintText: confirmPassword,
                        prefixIcon: Container(
                          width: 90,
                          alignment: Alignment.center,
                          margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                          // child: Image.asset("assets/images/email_icon.png", height: 15, width: 20),
                          child: const Icon(Icons.lock_outline, size: 32, color: defaultText),
                        ),
                        suffixIcon: Container(
                          margin: const EdgeInsets.fromLTRB(10, 0, 20, 0),
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                isShowConfirmPassword = !isShowConfirmPassword;
                              });
                            },
                            child: isShowConfirmPassword
                                ? const Icon(Icons.visibility_off_outlined, size: 25)
                                : const Icon(Icons.visibility_outlined, size: 25),
                          ),
                        ),
                      ),
                      style: textFormFieldStyle,
                      validator: passwordValidator,
                      autovalidateMode: AutovalidateMode.onUserInteraction),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: DropdownButtonFormField<LanguagesList>(
                    key: _languagesDropdownKey,
                    items: languagesMenuItems,
                    isExpanded: true,
                    decoration: languageDecoration,
                    style: textFormFieldStyle,
                    validator: languagesDropDownValidator,
                    onChanged: (LanguagesList? value) {
                      setState(() {
                        languagesInitialValue = value!;
                      });
                    },
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: DropdownButtonFormField<CountriesList>(
                    key: _countriesDropdownKey,
                    items: countriesMenuItems,
                    isExpanded: true,
                    decoration: countryDecoration,
                    style: textFormFieldStyle,
                    validator: countriesDropDownValidator,
                    onChanged: (CountriesList? value) {
                      setState(() {
                        countriesInitialValue = value!;
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget submitOrSignIn() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
      child: Column(
        children: [
          Material(
            elevation: 5,
            borderRadius: payBorderRadius,
            child: ClipRRect(
              borderRadius: payBorderRadius,
              child: Container(
                width: ScreenConfig.width(context),
                decoration: const BoxDecoration(gradient: buttonGradient),
                child: TextButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (passwordController.text.trim() == confirmPasswordController.text.trim()) {
                        showLoadingDialog(context);
                        await viewModel.verifyMobileNumberAPI(phoneController.text.toString(), emailController.text.toString()).then((value) {
                          Navigator.of(context).pop();
                          if (viewModel.verifyMobileResponseModel != null) {
                            if (viewModel.verifyMobileResponseModel!.responseStatus == 1) {
                              showToast(viewModel.verifyMobileResponseModel!.result!);
                              viewModel.showOtpPage(true);
                            } else {
                              showToast(viewModel.verifyMobileResponseModel!.result!);
                            }
                          } else {
                            showToast(tryAgain);
                          }
                        });
                      } else {
                        showToast(passwordsNotMatch);
                      }
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                    child: AdaptableText(next, style: submitButtonTextStyle, textMaxLines: 1),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
            child: RichText(
              text: TextSpan(
                style: signInContinueTextStyle,
                children: <TextSpan>[
                  TextSpan(text: alreadyMember, style: signInContinueTextStyle),
                  TextSpan(text: ' ', style: signInContinueTextStyle),
                  TextSpan(
                      text: signIn,
                      style: signUpContinueTextStyle,
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          Navigator.of(context).pop();
                        }),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget welcomeHeadersOtp() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
            child: AdaptableText(otpVerification, style: welcomeTextStyle, textMaxLines: 1),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 5),
            child: AdaptableText(otpPleaseVerify, style: signInContinueTextStyle, textMaxLines: 2),
          ),
        ],
      ),
    );
  }

  Widget otpField() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Form(
        key: _pinPutKey,
        child: Column(
          children: [
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                child: AdaptableText(enterOtp, style: signInContinueTextStyle, textMaxLines: 1),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Pinput(
                animationCurve: Curves.linear,
                animationDuration: const Duration(milliseconds: 160),
                pinAnimationType: PinAnimationType.slide,
                separator: const SizedBox(width: 1),
                showCursor: false,
                length: 6,
                closeKeyboardWhenCompleted: true,
                controller: otpController,
                defaultPinTheme: defaultPinTheme,
                focusedPinTheme: defaultPinTheme.copyWith(height: 68, width: 64),
                errorPinTheme: defaultPinTheme.copyWith(
                  decoration: BoxDecoration(
                    color: spinnerDropDown.withOpacity(0.5),
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.allow(RegExp(r'[0-9]')), LengthLimitingTextInputFormatter(6)],
                validator: pinPutValidator,
                pinputAutovalidateMode: PinputAutovalidateMode.onSubmit,
                obscuringCharacter: '*',
                obscureText: true,
                onChanged: (value) {},
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget submitOrResend() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
            child: TextButton(
              onPressed: () async {
                showLoadingDialog(context);
                await viewModel.verifyMobileNumberAPI(phoneController.text.toString(), emailController.text.toString()).then((value) {
                  Navigator.of(context).pop();
                  if (viewModel.verifyMobileResponseModel != null) {
                    if (viewModel.verifyMobileResponseModel!.responseStatus == 1) {
                      _pinPutKey.currentState!.reset();
                      showToast(viewModel.verifyMobileResponseModel!.result!);
                    } else {
                      showToast(viewModel.verifyMobileResponseModel!.result!);
                    }
                  } else {
                    showToast(tryAgain);
                  }
                });
              },
              child: Padding(
                padding: const EdgeInsets.all(15),
                child: AdaptableText(resendCode, style: resendTextStyle, textMaxLines: 1),
              ),
            ),
          ),
          Material(
            elevation: 5,
            borderRadius: payBorderRadius,
            child: ClipRRect(
              borderRadius: payBorderRadius,
              child: Container(
                width: ScreenConfig.width(context),
                decoration: const BoxDecoration(gradient: buttonGradient),
                child: TextButton(
                  onPressed: () async {
                    if (_pinPutKey.currentState!.validate()) {
                      showLoadingDialog(context);
                      var _accountType = "";
                      if (accountType == 0) {
                        _accountType = "individual";
                      } else if (accountType == 1) {
                        _accountType = "business";
                      }
                      var signUpModel = UserSignUpModel(
                          email: emailController.text.trim().toString(),
                          firstName: firstNameController.text.trim().toString(),
                          lastName: lastNameController.text.trim().toString(),
                          phoneCode: phoneCode,
                          accountType: _accountType,
                          bussinessName: businessNameController.text.trim().toString(),
                          phoneNumber: phoneController.text.trim().toString(),
                          password: passwordController.text.trim().toString(),
                          confirmNewPassword: confirmPasswordController.text.trim().toString(),
                          languageId: languagesInitialValue.id,
                          countryId: countriesInitialValue.id,
                          profilePicture: "");
                      await viewModel.createUserAPI(signUpModel).then((value) {
                        Navigator.of(context).pop();
                        if (viewModel.createUserResponseModel != null) {
                          if (viewModel.createUserResponseModel!.responseStatus! == 1) {
                            showToast(viewModel.createUserResponseModel!.result!);
                            viewModel.showOtpPage(false);
                            Navigator.pushNamedAndRemoveUntil(context, AppRoutes.login, (route) => false);
                          } else {
                            showToast(viewModel.createUserResponseModel!.result!);
                          }
                        } else {
                          showToast(tryAgain);
                        }
                      });
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                    child: AdaptableText(submit, style: submitButtonTextStyle, textMaxLines: 1),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget countriesDropDown() {
    return CountryPickerDropdown(
      isDense: true,
      isExpanded: false,
      itemBuilder: buildDropdownItem,
      onValuePicked: (CountriesList country) {
        setState(() {
          phoneCode = "";
          phoneCode = country.phoneCode!;
          input_phonenumber_hint = inputPhoneNumberFormatByCountry('9999999999', phoneCode);
          phoneController.text = inputPhoneNumberFormatByCountry(phoneController.text, phoneCode);
        });
      },
      countryList: Provider.of<LoginNotifier>(context, listen: true).getCountriesList(),
    );
  }

  countriesList() {
    countriesMenuItems = [];
    countriesMenuItems
        .add(DropdownMenuItem(child: Text(selectCountry, style: textFormFieldStyle, textAlign: TextAlign.center), value: countriesInitialValue));
    for (var element in Provider.of<LoginNotifier>(context, listen: false).getCountriesList()) {
      countriesMenuItems.add(DropdownMenuItem(child: Text(element.name!, style: textFormFieldStyle, textAlign: TextAlign.center), value: element));
    }
  }

  languagesList() {
    languagesMenuItems = [];
    languagesMenuItems.add(
        DropdownMenuItem(child: AdaptableText(selectLanguage, style: textFormFieldStyle, textAlign: TextAlign.center), value: languagesInitialValue));
    for (var element in Provider.of<LoginNotifier>(context, listen: false).getLanguagesList()) {
      languagesMenuItems.add(DropdownMenuItem(child: Text(element.name!, style: textFormFieldStyle, textAlign: TextAlign.center), value: element));
    }
  }

  Future<bool> onBackPressed() async {
    if (showOtpPage) {
      showOtpPage = false;
      setState(() {});
      return false;
    } else {
      Navigator.of(context).pop();
      return true;
    }
  }
}
