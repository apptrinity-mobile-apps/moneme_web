import 'package:flutter/material.dart';
import 'package:mone_me/screens/login/login_notifier/login_notifier.dart';
import 'package:mone_me/utils/routes.dart';
import 'package:mone_me/utils/screen_config.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  late LoginNotifier viewModel;

  @override
  void initState() {
    viewModel = Provider.of<LoginNotifier>(context, listen: false);
    // initial apis
    viewModel.getSocialMediaLinksAPI();
    viewModel.getAllLanguagesAPI();
    viewModel.getAllCountriesAPI();
    viewModel.getUserDetails();
    Future.delayed(const Duration(milliseconds: 500), () {
      if (viewModel.isLogged) {
        Navigator.pushNamedAndRemoveUntil(context, AppRoutes.home, (route) => false);
      } else {
        Navigator.pushNamedAndRemoveUntil(context, AppRoutes.login, (route) => false);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          body: Container(
            height: ScreenConfig.height(context),
            width: ScreenConfig.width(context),
            decoration: const BoxDecoration(
              image: DecorationImage(image: AssetImage("assets/images/login_background_logo.png"), fit: BoxFit.fill),
            ),
            child: Center(
              child: Image.asset("assets/images/app_logo_vertical.png",
                  height: ScreenConfig.height(context) / 2, width: ScreenConfig.width(context) / 2),
            ),
          ),
        ),
      ),
    );
  }
}
