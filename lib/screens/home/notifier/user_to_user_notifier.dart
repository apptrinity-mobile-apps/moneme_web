import 'package:flutter/cupertino.dart';
import 'package:mone_me/models/base_response_model.dart';
import 'package:mone_me/models/user_to_user_response_model.dart';
import 'package:mone_me/services/repositories.dart';

class UserToUserNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;

  UserToUserResponseModel? _userToUserResponseModel;

  UserToUserResponseModel? userToUserResponseModel() {
    return _userToUserResponseModel;
  }

  UserProfileDetails? userToUserProfileDetails() {
    UserProfileDetails? details;
    if (_userToUserResponseModel != null) {
      if (_userToUserResponseModel!.responseStatus == 1) {
        details = _userToUserResponseModel!.userProfileDetails!;
      }
    }
    return details;
  }

  List<UserTransactionsList>? userToUserTransactionsList() {
    List<UserTransactionsList>? list = [];
    if (_userToUserResponseModel != null) {
      if (_userToUserResponseModel!.responseStatus == 1) {
        if (_userToUserResponseModel!.userTransactionsList!.isNotEmpty) {
          list = _userToUserResponseModel!.userTransactionsList!;
        }
      }
    }
    return list;
  }

  Future<UserToUserResponseModel?> getFriendRecentTransactionsAPI(String userId, String friendId) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().getFriendRecentTransactions(userId, friendId);
      if (response != null) {
        _userToUserResponseModel = UserToUserResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("getFriendRecentTransactionsAPI $_isFetching $_isHavingData $_userToUserResponseModel");
    notifyListeners();
    return _userToUserResponseModel;
  }

  BaseResponseModel? _blockResponseModel;

  BaseResponseModel? get blockResponseModel => _blockResponseModel;

  Future<BaseResponseModel?> blockUserAPI(String userId, String blockUserId) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().blockUser(userId, blockUserId);
      if (response != null) {
        _blockResponseModel = BaseResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("blockUserAPI $_isFetching $_isHavingData $_blockResponseModel");
    notifyListeners();
    return _blockResponseModel;
  }

  BaseResponseModel? _unBlockResponseModel;

  BaseResponseModel? get unBlockResponseModel => _unBlockResponseModel;

  Future<BaseResponseModel?> unBlockUserAPI(String userId, String blockUserId) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().unBlockUser(userId, blockUserId);
      if (response != null) {
        _unBlockResponseModel = BaseResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("blockUserAPI $_isFetching $_isHavingData $_unBlockResponseModel");
    notifyListeners();
    return _unBlockResponseModel;
  }
}
