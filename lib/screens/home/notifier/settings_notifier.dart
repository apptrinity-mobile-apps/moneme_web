import 'package:flutter/cupertino.dart';
import 'package:mone_me/models/base_response_model.dart';
import 'package:mone_me/models/update_profile_model.dart';
import 'package:mone_me/services/repositories.dart';

class SettingsNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;
  BaseResponseModel? _createPinResponseModel;

  BaseResponseModel? get createPinResponseModel => _createPinResponseModel;

  Future<BaseResponseModel?> createSecurePinAPI(String userId, String pin) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().createSecurePin(userId, pin);
      if (response != null) {
        _createPinResponseModel = BaseResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("createSecurePinAPI $_isFetching $_isHavingData $_createPinResponseModel");
    notifyListeners();
    return _createPinResponseModel;
  }

  BaseResponseModel? _profileUpdateResponse;

  BaseResponseModel? get profileUpdateResponse => _profileUpdateResponse;

  Future<BaseResponseModel?> updateProfileAPI(UpdateProfileModel profileModel) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().updateProfile(profileModel);
      if (response != null) {
        _profileUpdateResponse = BaseResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("updateProfileAPI $_isFetching $_isHavingData $_profileUpdateResponse");
    notifyListeners();
    return _profileUpdateResponse;
  }
}
