import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mone_me/models/add_bank_account_model.dart';
import 'package:mone_me/models/bank_account_model.dart';
import 'package:mone_me/models/base_response_model.dart';
import 'package:mone_me/models/dashboard_response_model.dart';
import 'package:mone_me/models/friends_response_model.dart';
import 'package:mone_me/models/saved_cards_response_model.dart';
import 'package:mone_me/models/user_search_response_model.dart';
import 'package:mone_me/services/repositories.dart';
import 'package:mone_me/utils/strings.dart';

class HomeNotifier with ChangeNotifier {
  String _innerPage = "", amountToBeAddedToWallet = "";
  int radioGroupValue = -1;
  bool _isFetching = false, _isHavingData = false, _friendsListLoading = true, _balancesLoading = true;
  DashboardResponseModel? _dashboardResponseModel;
  FriendsResponseModel? _friendsResponseModel;
  UserSearchResponseModel? _searchResponseModel;
  CreditCardsList? cardItem;

  String get innerPage => _innerPage;

  DashboardResponseModel? get dashboardResponseModel => _dashboardResponseModel;

  bool get friendsListLoading => _friendsListLoading;

  bool get balancesLoading => _balancesLoading;

  bool? get hasSecurePin {
    var pinAvailable = false;
    if (dashboardResponseModel != null) {
      if (dashboardResponseModel!.responseStatus == 1) {
        if (dashboardResponseModel!.userProfileDetails != null) {
          pinAvailable = dashboardResponseModel!.userProfileDetails!.transferSecurePin!;
        }
      }
    }
    return pinAvailable;
  }

  double? get getTransferredAmount {
    var amount = 0.0;
    if (dashboardResponseModel != null) {
      if (dashboardResponseModel!.responseStatus == 1) {
        if (dashboardResponseModel!.userProfileDetails != null) {
          amount = dashboardResponseModel!.userProfileDetails!.transferredBalance!;
        }
      }
    }
    return amount;
  }

  double? get getReceivedAmount {
    var amount = 0.0;
    if (dashboardResponseModel != null) {
      if (dashboardResponseModel!.responseStatus == 1) {
        if (dashboardResponseModel!.userProfileDetails != null) {
          amount = dashboardResponseModel!.userProfileDetails!.receivedBalance!;
        }
      }
    }
    return amount;
  }

  double? get getCurrentAmount {
    var amount = 0.0;
    if (dashboardResponseModel != null) {
      if (dashboardResponseModel!.responseStatus == 1) {
        if (dashboardResponseModel!.userProfileDetails != null) {
          amount = dashboardResponseModel!.userProfileDetails!.currentBalance!;
        }
      }
    }
    return amount;
  }

  List<UniqueFriendsList?> allUniqueFriendsList() {
    List<UniqueFriendsList?> list = [];
    if (_friendsResponseModel != null) {
      if (_friendsResponseModel!.responseStatus == 1) {
        list = _friendsResponseModel!.uniqueFriendsList!;
      }
    }
    return list;
  }

  List<UniqueFriendsList?> allUniqueBusinessesList() {
    List<UniqueFriendsList?> list = [];
    if (_friendsResponseModel != null) {
      if (_friendsResponseModel!.responseStatus == 1) {
        list = _friendsResponseModel!.businessFriendsList!;
      }
    }
    return list;
  }

  List<UsersList?> getUsersSearchList() {
    List<UsersList?> list = [];
    if (_searchResponseModel != null) {
      if (_searchResponseModel!.responseStatus == 1) {
        list = _searchResponseModel!.usersList!;
      }
    }
    return list;
  }

  setInnerPage(String page) {
    _innerPage = page;
    notifyListeners();
  }

  int? getRadioGroupValue() {
    int? groupValue = -1;
    groupValue = radioGroupValue;
    return groupValue;
  }

  CreditCardsList? getCardRadioGroupValue() {
    return cardItem!;
  }

  setCardRadioGroupValue(CreditCardsList? card) {
    cardItem = card;
    notifyListeners();
  }

  setRadioGroupValue(int value) {
    radioGroupValue = value;
    notifyListeners();
  }

  setAmountToBeAddedToWallet(String amount) {
    amountToBeAddedToWallet = amount;
    notifyListeners();
  }

  String? getAmountToBeAddedToWallet() {
    return amountToBeAddedToWallet;
  }

  UserProfileDetails? get userData {
    UserProfileDetails? details;
    if (_dashboardResponseModel != null) {
      if (_dashboardResponseModel!.responseStatus == 1) {
        details = _dashboardResponseModel!.userProfileDetails;
      }
    }
    return details;
  }

  String? get userName {
    String? name = "";
    if (_dashboardResponseModel != null) {
      if (_dashboardResponseModel!.responseStatus == 1) {
        if (_dashboardResponseModel!.userProfileDetails != null) {
          name = _dashboardResponseModel!.userProfileDetails!.userName!;
        }
      }
    } else {
      name = appName;
    }
    return name;
  }

  String? get userPhoneNumber {
    String? phone = "";
    if (_dashboardResponseModel != null) {
      if (_dashboardResponseModel!.responseStatus == 1) {
        if (_dashboardResponseModel!.userProfileDetails != null) {
          phone = "${_dashboardResponseModel!.userProfileDetails!.phoneCode!} ${_dashboardResponseModel!.userProfileDetails!.phoneNumber!}";
        }
      }
    }
    return phone;
  }

  String? get userEmail {
    String? email = "";
    if (_dashboardResponseModel != null) {
      if (_dashboardResponseModel!.responseStatus == 1) {
        if (_dashboardResponseModel!.userProfileDetails != null) {
          email = _dashboardResponseModel!.userProfileDetails!.email!;
        }
      }
    }
    return email;
  }

  String? get userProfileImage {
    String? image = "";
    if (_dashboardResponseModel != null) {
      if (_dashboardResponseModel!.responseStatus == 1) {
        if (_dashboardResponseModel!.userProfileDetails != null) {
          image = _dashboardResponseModel!.userProfileDetails!.profilePicture!;
        }
      }
    }
    return image;
  }

  Future<DashboardResponseModel?> getDashboardAPI(String userId, String timezone) async {
    _balancesLoading = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().viewDashboard(userId, timezone);
      if (response != null) {
        _dashboardResponseModel = DashboardResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _balancesLoading = false;
    debugPrint("getDashboardAPI $_balancesLoading $_isHavingData $_dashboardResponseModel");
    notifyListeners();
    return _dashboardResponseModel;
  }

  Future<FriendsResponseModel?> getAllFriendsAPI(String userId) async {
    _friendsListLoading = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().getAllFriends(userId);
      if (response != null) {
        _friendsResponseModel = FriendsResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _friendsListLoading = false;
    debugPrint("getAllFriendsAPI $_friendsListLoading $_isHavingData $_friendsResponseModel");
    notifyListeners();
    return _friendsResponseModel;
  }

  BankModelResponseModel? _banksModelResponse;

  BankModelResponseModel? get banksResponse {
    return _banksModelResponse;
  }

  List<Bank?> userBanksList() {
    List<Bank?> list = [];
    if (_banksModelResponse != null) {
      if (_banksModelResponse!.responseStatus == 1) {
        if (_banksModelResponse!.banksList!.isNotEmpty) {
          list = _banksModelResponse!.banksList!;
        }
      }
    }
    return list;
  }

  Future<BankModelResponseModel?> getAllBankAccountsAPI(String userId) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().getAllBanks(userId);
      if (response != null) {
        _banksModelResponse = BankModelResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("getAllFriendsAPI $_isFetching $_isHavingData $_banksModelResponse");
    notifyListeners();
    return _banksModelResponse;
  }

  BaseResponseModel? _addBankResponse;

  BaseResponseModel? get addBankResponse {
    return _addBankResponse;
  }

  Future<BaseResponseModel?> addBankAccountAPI(AddBankAccountModel model) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().addBankAccount(model);
      if (response != null) {
        _addBankResponse = BaseResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("getAllFriendsAPI $_isFetching $_isHavingData $_addBankResponse");
    notifyListeners();
    return _addBankResponse;
  }

  BaseResponseModel? _deleteBankResponse;

  BaseResponseModel? get deleteBankResponse {
    return _deleteBankResponse;
  }

  Future<BaseResponseModel?> deleteBankAccountAPI(String userId, String bankAccountId) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().deleteBankAccount(userId, bankAccountId);
      if (response != null) {
        _deleteBankResponse = BaseResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("deleteBankAccountAPI $_isFetching $_isHavingData $_deleteBankResponse");
    notifyListeners();
    return _deleteBankResponse;
  }

  Future<UserSearchResponseModel?> searchUserByPhoneAPI(String userId, String phoneNumber) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().searchUserByPhone(userId, phoneNumber);
      if (response != null) {
        _searchResponseModel = UserSearchResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("searchUserByPhoneAPI $_isFetching $_isHavingData $_searchResponseModel");
    notifyListeners();
    return _searchResponseModel;
  }
  
  // stripe
  BaseResponseModel? _viewCustomerStripeResponse;

  BaseResponseModel? get viewCustomerStripeResponse {
    return _viewCustomerStripeResponse;
  }

  Future<BaseResponseModel?> viewCustomerStripeAPI(String userId) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().viewCustomerStripe(userId);
      if (response != null) {
        _viewCustomerStripeResponse = BaseResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("deleteBankAccountAPI $_isFetching $_isHavingData $_viewCustomerStripeResponse");
    notifyListeners();
    return _viewCustomerStripeResponse;
  }

  BaseResponseModel? _updateCustomerStripeResponse;

  BaseResponseModel? get updateCustomerStripeResponse {
    return _updateCustomerStripeResponse;
  }

  Future<BaseResponseModel?> updateCustomerStripeAPI(String userId, String stripeId) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().updateCustomerStripe(userId, stripeId);
      if (response != null) {
        _updateCustomerStripeResponse = BaseResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("deleteBankAccountAPI $_isFetching $_isHavingData $_updateCustomerStripeResponse");
    notifyListeners();
    return _updateCustomerStripeResponse;
  }
}
