import 'package:flutter/cupertino.dart';
import 'package:mone_me/models/add_amount_to_wallet_model.dart';
import 'package:mone_me/models/add_card_model.dart';
import 'package:mone_me/models/add_transaction_model.dart';
import 'package:mone_me/models/base_response_model.dart';
import 'package:mone_me/models/saved_cards_response_model.dart';
import 'package:mone_me/models/transaction_fees_response_model.dart';
import 'package:mone_me/models/transaction_history_response_model.dart';
import 'package:mone_me/models/transaction_single_view_response_model.dart';
import 'package:mone_me/models/user_to_user_response_model.dart' as user_to_user_model;
import 'package:mone_me/models/wallet_to_bank_model.dart';
import 'package:mone_me/services/repositories.dart';
import 'package:mone_me/models/friend_transactions_response_model.dart' as friends_profile_transactions;

class TransactionsNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;
  TransactionHistoryResponseModel? _allTransactions, _allTransferredTransactions, _allReceivedTransactions, _allRequestsTransactions;
  BaseResponseModel? _amountAddedWalletResponse, _cardAddedResponse, _addTransactionResponse;
  TransactionSingleViewResponseModel? _transactionSingleViewResponseModel;
  friends_profile_transactions.FriendTransactionsResponseModel? _friendTransactionsResponseModel;
  SavedCardsResponseModel? _savedCardsResponseModel;
  TransactionsFeesResponseModel? _transactionsFeesResponseModel;
  String? transactionAmount = "";

  BaseResponseModel? get amountAddedWalletResponse => _amountAddedWalletResponse;

  BaseResponseModel? get cardAddedResponse => _cardAddedResponse;

  BaseResponseModel? get addTransactionResponse => _addTransactionResponse;

  TransactionsFeesResponseModel? get transactionsFeesResponseModel => _transactionsFeesResponseModel;

  TransactionHistoryResponseModel? get allTransactions {
    TransactionHistoryResponseModel? model;
    if (_allTransactions != null) {
      model = _allTransactions;
    }
    return model;
  }

  double get getDepositFee {
    double fees = 0.00;
    if (transactionsFeesResponseModel != null) {
      if (transactionsFeesResponseModel!.responseStatus == 1) {
        if (transactionsFeesResponseModel!.transactionFeeDetails != null) {
          fees = double.parse(transactionsFeesResponseModel!.transactionFeeDetails!.depositFee!);
        }
      }
    }
    return fees;
  }

  double get getWithdrawFee {
    double fees = 0.00;
    if (transactionsFeesResponseModel != null) {
      if (transactionsFeesResponseModel!.responseStatus == 1) {
        if (transactionsFeesResponseModel!.transactionFeeDetails != null) {
          fees = double.parse(transactionsFeesResponseModel!.transactionFeeDetails!.withdrawFee!);
        }
      }
    }
    return fees;
  }

  List<TransactionsList?> allTransactionsList() {
    List<TransactionsList?> list = [];
    if (_allTransactions != null) {
      if (_allTransactions!.responseStatus == 1) {
        list = _allTransactions!.transactionsList!;
      }
    }
    return list;
  }

  List<TransactionsList?> allTransferredTransactionsList() {
    List<TransactionsList?> list = [];
    if (_allTransferredTransactions != null) {
      if (_allTransferredTransactions!.responseStatus == 1) {
        list = _allTransferredTransactions!.transactionsList!;
      }
    }
    return list;
  }

  List<TransactionsList?> allReceivedTransactionsList() {
    List<TransactionsList?> list = [];
    if (_allReceivedTransactions != null) {
      if (_allReceivedTransactions!.responseStatus == 1) {
        list = _allReceivedTransactions!.transactionsList!;
      }
    }
    return list;
  }

  List<TransactionsList?> allRequestsList() {
    List<TransactionsList?> list = [];
    if (_allRequestsTransactions != null) {
      if (_allRequestsTransactions!.responseStatus == 1) {
        list = _allRequestsTransactions!.requestsList!;
      }
    }
    return list;
  }

  List<TransactionsList?> allRequestedList() {
    List<TransactionsList?> list = [];
    if (_allRequestsTransactions != null) {
      if (_allRequestsTransactions!.responseStatus == 1) {
        list = _allRequestsTransactions!.requestedList!;
      }
    }
    return list;
  }

  TransactionsDetails? get transactionDetails {
    TransactionsDetails? details;
    if (_transactionSingleViewResponseModel != null) {
      if (_transactionSingleViewResponseModel!.responseStatus == 1) {
        details = _transactionSingleViewResponseModel!.transactionsDetails!;
      }
    }
    return details;
  }

  FriendsProfileDetails? get transactionProfileDetails {
    FriendsProfileDetails? details;
    if (_transactionSingleViewResponseModel != null) {
      if (_transactionSingleViewResponseModel!.responseStatus == 1) {
        details = _transactionSingleViewResponseModel!.friendsProfileDetails!;
      }
    }
    return details;
  }

  List<friends_profile_transactions.UserTransactionsList?> friendsTransferredTransactionsList() {
    List<friends_profile_transactions.UserTransactionsList?> list = [];
    if (_friendTransactionsResponseModel != null) {
      if (_friendTransactionsResponseModel!.responseStatus == 1) {
        list = _friendTransactionsResponseModel!.userTransactionsList!;
      }
    }
    return list;
  }

  friends_profile_transactions.UserProfileDetails? get friendsProfile {
    friends_profile_transactions.UserProfileDetails? profileDetails;
    if (_friendTransactionsResponseModel != null) {
      if (_friendTransactionsResponseModel!.responseStatus == 1) {
        profileDetails = _friendTransactionsResponseModel!.userProfileDetails!;
      }
    }
    return profileDetails;
  }

  List<CreditCardsList?> get getSavedCards {
    List<CreditCardsList?> cards = [];
    if (_savedCardsResponseModel != null) {
      if (_savedCardsResponseModel!.responseStatus == 1) {
        cards = _savedCardsResponseModel!.creditCardsList!;
      }
    }
    return cards;
  }

  setTransactionAmount(String amount) {
    transactionAmount = amount;
    notifyListeners();
  }

  String? get getTransactionAmount => transactionAmount;

  Future<TransactionHistoryResponseModel?> geAllTransactionsAPI(String userId, String startTime, String endTime) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().geAllTransactions(userId, startTime, endTime);
      if (response != null) {
        _allTransactions = TransactionHistoryResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("geAllTransactionsAPI $_isFetching $_isHavingData $_allTransactions");
    notifyListeners();
    return _allTransactions;
  }

  Future<TransactionHistoryResponseModel?> getAllTransferredTransactionsAPI(String userId, String startTime, String endTime) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().getAllTransferredTransactions(userId, startTime, endTime);
      if (response != null) {
        _allTransferredTransactions = TransactionHistoryResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("getAllTransferredTransactionsAPI $_isFetching $_isHavingData $_allTransferredTransactions");
    notifyListeners();
    return _allTransferredTransactions;
  }

  Future<TransactionHistoryResponseModel?> getAllReceivedTransactionsAPI(String userId, String startTime, String endTime) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().getAllReceivedTransactions(userId, startTime, endTime);
      if (response != null) {
        _allReceivedTransactions = TransactionHistoryResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("getAllReceivedTransactionsAPI $_isFetching $_isHavingData $_allReceivedTransactions");
    notifyListeners();
    return _allReceivedTransactions;
  }

  Future<TransactionHistoryResponseModel?> getAllRequestsTransactionsAPI(String userId, String startTime, String endTime) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().getAllRequestsTransactions(userId, startTime, endTime);
      if (response != null) {
        _allRequestsTransactions = TransactionHistoryResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("getAllReceivedTransactionsAPI $_isFetching $_isHavingData $_allRequestsTransactions");
    notifyListeners();
    return _allRequestsTransactions;
  }

  Future<BaseResponseModel?> addAmountToWalletAPI(AddAmountToWalletModel amountToWalletModel) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().addAmountToWallet(amountToWalletModel);
      if (response != null) {
        _amountAddedWalletResponse = BaseResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("addAmountToWalletAPI $_isFetching $_isHavingData $_amountAddedWalletResponse");
    notifyListeners();
    return _amountAddedWalletResponse;
  }

  Future<TransactionSingleViewResponseModel?> getSingleTransactionAPI(String userId, String transactionId) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().getSingleTransaction(userId, transactionId);
      if (response != null) {
        _transactionSingleViewResponseModel = TransactionSingleViewResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("getSingleTransactionAPI $_isFetching $_isHavingData $_transactionSingleViewResponseModel");
    notifyListeners();
    return _transactionSingleViewResponseModel;
  }

  Future<SavedCardsResponseModel?> getSavedCardsAPI(String userId) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().getSavedCards(userId);
      if (response != null) {
        _savedCardsResponseModel = SavedCardsResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("getSavedCardsAPI $_isFetching $_isHavingData $_savedCardsResponseModel");
    notifyListeners();
    return _savedCardsResponseModel;
  }

  Future<BaseResponseModel?> addCardAPI(AddCardModel cardModel) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().addCard(cardModel);
      if (response != null) {
        _cardAddedResponse = BaseResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("addCardAPI $_isFetching $_isHavingData $_cardAddedResponse");
    notifyListeners();
    return _cardAddedResponse;
  }

  Future<BaseResponseModel?> addTransactionAPI(AddTransactionModel addTransactionModel) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().addTransaction(addTransactionModel);
      if (response != null) {
        _addTransactionResponse = BaseResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("addTransactionAPI $_isFetching $_isHavingData $_addTransactionResponse");
    notifyListeners();
    return _addTransactionResponse;
  }

  Future<TransactionsFeesResponseModel?> getTransactionFeesAPI() async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().getTransactionFees();
      if (response != null) {
        _transactionsFeesResponseModel = TransactionsFeesResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("getTransactionFeesAPI $_isFetching $_isHavingData $_transactionsFeesResponseModel");
    notifyListeners();
    return _transactionsFeesResponseModel;
  }

  BaseResponseModel? _walletToBankResponse;

  BaseResponseModel? get walletToBankResponse => _walletToBankResponse;

  Future<BaseResponseModel?> walletToBankAPI(WalletToBankModel walletToBankModel) async {
    _isFetching = true;
    _isHavingData = false;
    try {
      dynamic response = await Repository().walletToBank(walletToBankModel);
      if (response != null) {
        _walletToBankResponse = BaseResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("walletToBankAPI $_isFetching $_isHavingData $_walletToBankResponse");
    notifyListeners();
    return _walletToBankResponse;
  }
}
