import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mone_me/models/countries_response_model.dart';
import 'package:mone_me/models/languages_response_model.dart';
import 'package:mone_me/models/update_profile_model.dart';
import 'package:mone_me/screens/home/notifier/home_notifier.dart';
import 'package:mone_me/screens/home/notifier/settings_notifier.dart';
import 'package:mone_me/screens/login/login_notifier/login_notifier.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/screen_config.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/utils/upper_case_formatter.dart';
import 'package:mone_me/utils/validators.dart';
import 'package:mone_me/widgets/adaptable_text.dart';
import 'package:mone_me/widgets/common_body.dart';
import 'package:mone_me/widgets/common_header.dart';
import 'package:mone_me/widgets/custom_toast.dart';
import 'package:mone_me/widgets/loader.dart';
import 'package:provider/provider.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final _languagesDropdownKey = GlobalKey<FormFieldState>();
  final _countriesDropdownKey = GlobalKey<FormFieldState>();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  List<DropdownMenuItem<CountriesList>> countriesMenuItems = [];
  List<DropdownMenuItem<LanguagesList>> languagesMenuItems = [];
  CountriesList countriesInitialValue = CountriesList(name: selectCountry);
  LanguagesList languagesInitialValue = LanguagesList(name: selectLanguage);
  String userId = "", accountType = "", businessName = "", profilePicture = "";

  @override
  void initState() {
    userId = Provider.of<LoginNotifier>(context, listen: false).userId!;
    countriesList();
    languagesList();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final viewModel = Provider.of<HomeNotifier>(context, listen: true);
    if (viewModel.userData != null) {
      firstNameController.text = viewModel.userData!.firstName!.trim();
      lastNameController.text = viewModel.userData!.lastName!.trim();
      accountType = viewModel.userData!.accountType!;
      businessName = viewModel.userData!.bussinessName!;
      profilePicture = viewModel.userData!.profilePicture!;
      if (countriesMenuItems.isNotEmpty) {
        for (var element in countriesMenuItems) {
          if (viewModel.userData!.countryId! == element.value!.id) {
            countriesInitialValue = element.value!;
          }
        }
      }
      if (languagesMenuItems.isNotEmpty) {
        for (var element in languagesMenuItems) {
          if (viewModel.userData!.languageId! == element.value!.id) {
            languagesInitialValue = element.value!;
          }
        }
      }
    }
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          body: Container(
            color: CupertinoColors.white,
            child: Stack(
              children: [
                SizedBox(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  child: Image.asset("assets/images/login_background_logo.png", fit: BoxFit.fill),
                ),
                Positioned(
                  child: Container(
                    height: ScreenConfig.height(context),
                    width: ScreenConfig.width(context),
                    color: Colors.transparent,
                    child: Column(
                      children: [
                        const CommonHeader(),
                        Expanded(
                          child: body(),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget body() {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.all(10),
      child: Row(
        children: [
          const CommonBody(),
          Expanded(
            child: Container(
              height: ScreenConfig.height(context),
              width: ScreenConfig.width(context),
              margin: const EdgeInsets.only(left: 3),
              child: profileWidget(context),
            ),
          )
        ],
      ),
    );
  }

  Widget profileWidget(BuildContext context) {
    return Card(
      shadowColor: cardShadow,
      shape: const RoundedRectangleBorder(borderRadius: detailsMainBorderRadius),
      elevation: 5,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: ScreenConfig.width(context),
            padding: const EdgeInsets.fromLTRB(20, 20, 20, 15),
            color: headerColor,
            child: AdaptableText(profile, style: showTextStyle, textMaxLines: 1),
          ),
          content(context),
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 15, 30, 20),
            child: Material(
              elevation: 5,
              borderRadius: payBorderRadius,
              child: ClipRRect(
                borderRadius: payBorderRadius,
                child: Container(
                  width: ScreenConfig.width(context),
                  decoration: const BoxDecoration(gradient: buttonGradient, borderRadius: payBorderRadius),
                  child: TextButton(
                    onPressed: () {
                      showLoadingDialog(context);
                      final viewModel = Provider.of<SettingsNotifier>(context, listen: false);
                      var profileModel = UpdateProfileModel(
                          userId: userId,
                          firstName: firstNameController.text.trim().toString(),
                          lastName: lastNameController.text.trim().toString(),
                          countryId: countriesInitialValue.id,
                          languageId: languagesInitialValue.id,
                          accountType: accountType,
                          bussinessName: businessName,
                          profilePicture: profilePicture);
                      viewModel.updateProfileAPI(profileModel).then((value) {
                        Navigator.pop(context);
                        if (viewModel.profileUpdateResponse != null) {
                          if (viewModel.profileUpdateResponse!.responseStatus == 1) {
                            Provider.of<HomeNotifier>(context, listen: false).getDashboardAPI(userId, DateTime.now().timeZoneName);
                            showToast(viewModel.profileUpdateResponse!.result!);
                          } else {
                            showToast(viewModel.profileUpdateResponse!.result!);
                          }
                        } else {
                          showToast(tryAgain);
                        }
                      });
                    },
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                      child: AdaptableText(save, style: paymentButtonsTextStyle, textMaxLines: 1),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget content(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 5, 20, 25),
        child: ListView(
          padding: const EdgeInsets.only(left: 10, right: 10),
          controller: ScrollController(),
          physics: const BouncingScrollPhysics(),
          shrinkWrap: true,
          children: [
            Container(
              color: profileBackground,
              margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: Row(
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      ClipRRect(
                        borderRadius: peopleProfileBorderRadius,
                        child: Container(
                          width: 75,
                          height: 75,
                          margin: const EdgeInsets.fromLTRB(15, 2, 15, 2),
                          decoration: const BoxDecoration(
                            borderRadius: peopleProfileBorderRadius,
                            color: buttonGradientStart,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                offset: Offset(1, 1),
                                blurRadius: 1,
                              ),
                            ],
                          ),
                          child: Provider.of<HomeNotifier>(context).userProfileImage! == ""
                              ? Center(
                                  child: AdaptableText(Provider.of<HomeNotifier>(context).userName![0].toUpperCase(),
                                      style: peopleNameListIconTextStyle),
                                )
                              : ClipRRect(
                                  borderRadius: peopleProfileBorderRadius,
                                  child: Image.network(
                                    Provider.of<HomeNotifier>(context).userProfileImage!,
                                    fit: BoxFit.fill,
                                    errorBuilder: (ctx, _, __) {
                                      return Center(
                                        child: AdaptableText(Provider.of<HomeNotifier>(context).userName![0].toUpperCase(),
                                            style: peopleNameListIconTextStyle),
                                      );
                                    },
                                  ),
                                ),
                        ),
                      ),
                      TextButton(
                        onPressed: () {},
                        child: AdaptableText(
                          edit,
                          style: peopleNameListTextStyle.copyWith(fontSize: 12),
                          textMaxLines: 1,
                          textAlign: TextAlign.center,
                          textOverflow: TextOverflow.ellipsis,
                        ),
                      )
                    ],
                  ),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.only(bottom: 30),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AdaptableText(
                          Provider.of<HomeNotifier>(context).userName!,
                          style: const TextStyle(
                              letterSpacing: 1.2, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: optionsColor, fontSize: 16),
                          textMaxLines: 1,
                          textAlign: TextAlign.left,
                          textOverflow: TextOverflow.ellipsis,
                        ),
                        AdaptableText(
                          Provider.of<HomeNotifier>(context).userPhoneNumber!,
                          style: const TextStyle(
                              letterSpacing: 0.9, fontFamily: "Aller", fontWeight: FontWeight.w400, color: optionsColor, fontSize: 14),
                          textMaxLines: 1,
                          textAlign: TextAlign.left,
                          textOverflow: TextOverflow.ellipsis,
                        ),
                        AdaptableText(
                          Provider.of<HomeNotifier>(context).userEmail!,
                          style: const TextStyle(
                              letterSpacing: 0.9, fontFamily: "Aller", fontWeight: FontWeight.w400, color: optionsColor, fontSize: 14),
                          textMaxLines: 1,
                          textAlign: TextAlign.left,
                          textOverflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  ))
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: TextFormField(
                  autofocus: true,
                  controller: firstNameController,
                  textInputAction: TextInputAction.next,
                  inputFormatters: <TextInputFormatter>[CamelCaseTextFormatter()],
                  maxLines: 1,
                  keyboardType: TextInputType.name,
                  decoration: InputDecoration(
                    fillColor: CupertinoColors.white,
                    filled: true,
                    border: border.copyWith(
                      borderRadius: amountsIconBorderRadius,
                      borderSide: const BorderSide(color: profileFieldsBorder),
                    ),
                    isDense: true,
                    enabledBorder: border.copyWith(
                      borderRadius: amountsIconBorderRadius,
                      borderSide: const BorderSide(color: profileFieldsBorder),
                    ),
                    focusedBorder: border.copyWith(
                      borderRadius: amountsIconBorderRadius,
                      borderSide: const BorderSide(color: profileFieldsBorder),
                    ),
                    errorBorder: errorBorder.copyWith(borderRadius: amountsIconBorderRadius),
                    focusedErrorBorder: errorBorder.copyWith(borderRadius: amountsIconBorderRadius),
                    contentPadding: textFormFieldPadding.copyWith(left: 10, right: 10, bottom: 15, top: 15),
                    hintStyle: textFormFieldHintStyle,
                    hintText: firstName,
                    prefixIcon: Container(
                      width: 40,
                      alignment: Alignment.center,
                      margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                      // child: Image.asset("assets/images/user_icon.png", height: 15, width: 20),
                      child: const Icon(Icons.person_outline, size: 24, color: profileFieldsBorder),
                    ),
                  ),
                  style: textFormFieldStyle,
                  validator: emptyTextValidator,
                  autovalidateMode: AutovalidateMode.onUserInteraction),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: TextFormField(
                  controller: lastNameController,
                  textInputAction: TextInputAction.next,
                  inputFormatters: <TextInputFormatter>[CamelCaseTextFormatter()],
                  maxLines: 1,
                  keyboardType: TextInputType.name,
                  decoration: InputDecoration(
                    fillColor: CupertinoColors.white,
                    filled: true,
                    border: border.copyWith(
                      borderRadius: amountsIconBorderRadius,
                      borderSide: const BorderSide(color: profileFieldsBorder),
                    ),
                    isDense: true,
                    enabledBorder: border.copyWith(
                      borderRadius: amountsIconBorderRadius,
                      borderSide: const BorderSide(color: profileFieldsBorder),
                    ),
                    focusedBorder: border.copyWith(
                      borderRadius: amountsIconBorderRadius,
                      borderSide: const BorderSide(color: profileFieldsBorder),
                    ),
                    errorBorder: errorBorder.copyWith(borderRadius: amountsIconBorderRadius),
                    focusedErrorBorder: errorBorder.copyWith(borderRadius: amountsIconBorderRadius),
                    contentPadding: textFormFieldPadding.copyWith(left: 10, right: 10, bottom: 15, top: 15),
                    hintStyle: textFormFieldHintStyle,
                    hintText: lastName,
                    prefixIcon: Container(
                      width: 40,
                      alignment: Alignment.center,
                      margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                      // child: Image.asset("assets/images/user_icon.png", height: 15, width: 20),
                      child: const Icon(Icons.person_outline, size: 24, color: profileFieldsBorder),
                    ),
                  ),
                  style: textFormFieldStyle),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: DropdownButtonFormField<LanguagesList>(
                key: _languagesDropdownKey,
                items: languagesMenuItems,
                value: languagesInitialValue,
                isExpanded: true,
                decoration: InputDecoration(
                  fillColor: CupertinoColors.white,
                  filled: true,
                  border: border.copyWith(
                    borderRadius: amountsIconBorderRadius,
                    borderSide: const BorderSide(color: profileFieldsBorder),
                  ),
                  isDense: true,
                  enabledBorder: border.copyWith(
                    borderRadius: amountsIconBorderRadius,
                    borderSide: const BorderSide(color: profileFieldsBorder),
                  ),
                  focusedBorder: border.copyWith(
                    borderRadius: amountsIconBorderRadius,
                    borderSide: const BorderSide(color: profileFieldsBorder),
                  ),
                  errorBorder: errorBorder.copyWith(borderRadius: amountsIconBorderRadius),
                  focusedErrorBorder: errorBorder.copyWith(borderRadius: amountsIconBorderRadius),
                  contentPadding: const EdgeInsets.fromLTRB(0, 15, 10, 15),
                  hintStyle: textFormFieldHintStyle,
                  hintText: language,
                  prefixIcon: Container(
                    width: 40,
                    alignment: Alignment.center,
                    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                    // child: Image.asset("assets/images/email_icon.png", height: 15, width: 20),
                    child: const Icon(Icons.language, size: 24, color: profileFieldsBorder),
                  ),
                ),
                style: textFormFieldStyle,
                validator: languagesDropDownValidator,
                onChanged: (LanguagesList? value) {
                  setState(() {
                    languagesInitialValue = value!;
                  });
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: DropdownButtonFormField<CountriesList>(
                key: _countriesDropdownKey,
                items: countriesMenuItems,
                value: countriesInitialValue,
                isExpanded: true,
                decoration: InputDecoration(
                  fillColor: CupertinoColors.white,
                  filled: true,
                  border: border.copyWith(
                    borderRadius: amountsIconBorderRadius,
                    borderSide: const BorderSide(color: profileFieldsBorder),
                  ),
                  isDense: true,
                  enabledBorder: border.copyWith(
                    borderRadius: amountsIconBorderRadius,
                    borderSide: const BorderSide(color: profileFieldsBorder),
                  ),
                  focusedBorder: border.copyWith(
                    borderRadius: amountsIconBorderRadius,
                    borderSide: const BorderSide(color: profileFieldsBorder),
                  ),
                  errorBorder: errorBorder.copyWith(borderRadius: amountsIconBorderRadius),
                  focusedErrorBorder: errorBorder.copyWith(borderRadius: amountsIconBorderRadius),
                  contentPadding: const EdgeInsets.fromLTRB(0, 15, 10, 15),
                  hintStyle: textFormFieldHintStyle,
                  hintText: country,
                  prefixIcon: Container(
                    width: 40,
                    alignment: Alignment.center,
                    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                    // child: Image.asset("assets/images/email_icon.png", height: 15, width: 20),
                    child: const Icon(Icons.flag_outlined, size: 24, color: profileFieldsBorder),
                  ),
                ),
                style: textFormFieldStyle,
                validator: countriesDropDownValidator,
                onChanged: (CountriesList? value) {
                  setState(() {
                    countriesInitialValue = value!;
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  countriesList() {
    countriesMenuItems = [];
    countriesMenuItems
        .add(DropdownMenuItem(child: Text(selectCountry, style: textFormFieldStyle, textAlign: TextAlign.center), value: countriesInitialValue));
    for (var element in Provider.of<LoginNotifier>(context, listen: false).getCountriesList()) {
      countriesMenuItems.add(DropdownMenuItem(child: Text(element.name!, style: textFormFieldStyle, textAlign: TextAlign.center), value: element));
    }
  }

  languagesList() {
    languagesMenuItems = [];
    languagesMenuItems.add(
        DropdownMenuItem(child: AdaptableText(selectLanguage, style: textFormFieldStyle, textAlign: TextAlign.center), value: languagesInitialValue));
    for (var element in Provider.of<LoginNotifier>(context, listen: false).getLanguagesList()) {
      languagesMenuItems.add(DropdownMenuItem(child: Text(element.name!, style: textFormFieldStyle, textAlign: TextAlign.center), value: element));
    }
  }
}
