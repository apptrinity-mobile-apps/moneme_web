import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mone_me/models/user_to_user_model.dart';
import 'package:mone_me/models/user_to_user_response_model.dart';
import 'package:mone_me/screens/home/notifier/user_to_user_notifier.dart';
import 'package:mone_me/screens/login/login_notifier/login_notifier.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/screen_config.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/utils/transaction_type_enum.dart';
import 'package:mone_me/widgets/adaptable_text.dart';
import 'package:mone_me/widgets/common_body.dart';
import 'package:mone_me/widgets/common_header.dart';
import 'package:mone_me/widgets/custom_toast.dart';
import 'package:mone_me/widgets/list_loading_shimmer.dart';
import 'package:provider/provider.dart';

class UserToUserTransactionsPage extends StatefulWidget {
  const UserToUserTransactionsPage({Key? key}) : super(key: key);

  @override
  State<UserToUserTransactionsPage> createState() => _UserToUserTransactionsPageState();
}

class _UserToUserTransactionsPageState extends State<UserToUserTransactionsPage> with SingleTickerProviderStateMixin {
  String userId = "", friendId = "";
  bool isLoading = true, isUserBlocked = false, transactionsLoading = true;
  int selectedTab = -1;
  final ScrollController transactionsController = ScrollController();
  AnimationController? expandController;
  Animation<double>? animation;

  @override
  void initState() {
    userId = Provider.of<LoginNotifier>(context, listen: false).userId!;
    Future.delayed(Duration.zero, () {
      var args = ModalRoute.of(context)!.settings.arguments! as UserToUserModel;
      friendId = args.friendId!;
      getTransactions();
    });
    super.initState();
  }

  getTransactions() {
    setState(() {
      transactionsLoading = true;
    });
    Provider.of<UserToUserNotifier>(context, listen: false).getFriendRecentTransactionsAPI(userId, friendId).then((value) {
      transactionsLoading = false;
    });
  }

  ///Setting up the animation
  void prepareAnimations() {
    expandController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    animation = CurvedAnimation(
      parent: expandController!,
      curve: Curves.fastOutSlowIn,
    );
  }

  @override
  Widget build(BuildContext context) {
    var args = ModalRoute.of(context)!.settings.arguments! as UserToUserModel;
    final list = Provider.of<UserToUserNotifier>(context, listen: true).userToUserTransactionsList();
    selectedTab = args.tabSelected!;
    if (list!.isNotEmpty) {
      WidgetsBinding.instance!.addPostFrameCallback((_) {
        final position = transactionsController.position.maxScrollExtent;
        transactionsController.animateTo(
          position,
          duration: const Duration(microseconds: 30),
          curve: Curves.easeOut,
        );
      });
    }
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          body: Container(
            color: CupertinoColors.white,
            child: Stack(
              children: [
                SizedBox(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  child: Image.asset("assets/images/login_background_logo.png", fit: BoxFit.fill),
                ),
                Positioned(
                  child: Container(
                    height: ScreenConfig.height(context),
                    width: ScreenConfig.width(context),
                    color: Colors.transparent,
                    child: Column(
                      children: [
                        const CommonHeader(),
                        Expanded(
                          child: body(),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget body() {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.all(10),
      child: Row(
        children: [
          CommonBody(
            selectedMenu: selectedTab,
          ),
          Expanded(
            child: Container(
              height: ScreenConfig.height(context),
              width: ScreenConfig.width(context),
              margin: const EdgeInsets.only(left: 3),
              child: profileTransactionDetailsWidget(context),
            ),
          )
        ],
      ),
    );
  }

  Widget profileTransactionDetailsWidget(BuildContext context) {
    final viewModel = Provider.of<UserToUserNotifier>(context, listen: true);
    setState(() {
      if (viewModel.userToUserResponseModel() != null) {
        isUserBlocked = viewModel.userToUserResponseModel()!.userBlockFlag!;
      } else {
        isUserBlocked = false;
      }
    });
    return Card(
      shadowColor: cardShadow,
      shape: const RoundedRectangleBorder(borderRadius: detailsBorderRadius),
      elevation: 5,
      child: Column(children: [
        header(),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [profileDetail(), transactionsList()],
            ),
          ),
        ),
        isUserBlocked ? const SizedBox() : actions(context)
      ]),
    );
  }

  Widget header() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 20, 20, 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(Icons.keyboard_backspace_rounded, color: buttonGradientEnd, size: 24),
            splashRadius: 20,
          ),
          PopupMenuButton<int>(
            shape: const RoundedRectangleBorder(
              side: BorderSide(color: receivedIconBackground),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10),
              ),
            ),
            offset: const Offset(-7, 20),
            padding: const EdgeInsets.all(5),
            onSelected: (value) => onSelected(context, value),
            itemBuilder: (context) => [
              PopupMenuItem(
                  height: 25,
                  padding: const EdgeInsets.fromLTRB(10, 7, 7, 7),
                  child: Text(isUserBlocked ? unBlock : block),
                  textStyle: popMenuHeaderTextStyle,
                  value: 0),
            ],
            child: Image.asset("assets/images/menu.png", color: buttonGradientEnd, height: 21, width: 24),
          )
        ],
      ),
    );
  }

  Widget profileDetail() {
    var profileDetails = Provider.of<UserToUserNotifier>(context, listen: true).userToUserProfileDetails();
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ClipRRect(
            borderRadius: payBorderRadius,
            child: Container(
              width: 75,
              height: 75,
              margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
              decoration: const BoxDecoration(borderRadius: payBorderRadius, color: buttonGradientStart),
              child: profileDetails == null
                  ? Center(
                      child: AdaptableText(appName[0].toUpperCase(), style: peopleNameListIconTextStyle),
                    )
                  : profileDetails.profilePicture == ""
                      ? Center(
                          child: AdaptableText(profileDetails.userName == "" ? appName[0].toUpperCase() : profileDetails.userName![0].toUpperCase(),
                              style: peopleNameListIconTextStyle),
                        )
                      : ClipRRect(
                          borderRadius: payBorderRadius,
                          child: Image.network(
                            profileDetails.profilePicture!,
                            fit: BoxFit.fill,
                          ),
                        ),
            ),
          ),
          Center(
            child: AdaptableText(
                profileDetails == null
                    ? appName.toUpperCase()
                    : profileDetails.userName == ""
                        ? appName.toUpperCase()
                        : profileDetails.userName!.toUpperCase(),
                style: profileNameTextStyle,
                textMaxLines: 1),
          ),
          Center(
            child: AdaptableText(
                profileDetails == null
                    ? phoneNumber
                    : profileDetails.phoneNumber == ""
                        ? phoneNumber
                        : profileDetails.phoneNumber!,
                style: profilePhoneTextStyle,
                textMaxLines: 1),
          ),
        ],
      ),
    );
  }

  Widget transactionsList() {
    var list = Provider.of<UserToUserNotifier>(context, listen: true).userToUserTransactionsList();
    return Flexible(
      fit: FlexFit.loose,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
        child: list!.isNotEmpty
            ? ListView.builder(
                controller: transactionsController,
                shrinkWrap: true,
                itemCount: list.length,
                itemBuilder: (context, index) {
                  return dateBasedItems(list[index]);
                })
            : transactionsLoading
                ? listLoadingShimmer()
                : const SizedBox(),
      ),
    );
  }

  Widget dateBasedItems(UserTransactionsList? transactionsList) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(8, 15, 8, 10),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Expanded(
                child: Divider(color: dottedLine, thickness: 1),
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: AdaptableText(transactionsList!.date!, style: paymentDateSeparatorTextStyle, textMaxLines: 1),
                ),
              ),
              const Expanded(
                child: Divider(color: dottedLine, thickness: 1),
              ),
            ],
          ),
        ),
        ListView.builder(
            controller: ScrollController(),
            shrinkWrap: true,
            itemCount: transactionsList.transactionsList!.length,
            itemBuilder: (context, index) {
              return transactionsList.transactionsList![index].transactionType == TransactionTypes.debit.name
                  ? transferredWidget(context, transactionsList.transactionsList![index])
                  : receivedWidget(context, transactionsList.transactionsList![index]);
            })
      ],
    );
  }

  Widget receivedWidget(BuildContext context, TransactionsList? list) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 2),
      child: Row(
        children: [
          Expanded(
            child: Card(
              shadowColor: cardShadow,
              margin: const EdgeInsets.fromLTRB(5, 0, 5, 0),
              shape: const RoundedRectangleBorder(borderRadius: paymentTransactionsRadius),
              elevation: 3,
              color: CupertinoColors.white,
              child: InkWell(
                borderRadius: paymentTransactionsRadius,
                onTap: () async {},
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      list!.messageTransaction != "message"
                          ? const Align(
                              alignment: Alignment.centerRight,
                              child: Icon(Icons.arrow_right_alt_rounded, color: receivedIconBackground, size: 20),
                            )
                          : const SizedBox(),
                      list.messageTransaction != "message"
                          ? Padding(
                              padding: const EdgeInsets.fromLTRB(0, 5, 0, 3),
                              child: AdaptableText("\u0024${list.amount!.toStringAsFixed(2)}", style: paymentAmountTextStyle, textMaxLines: 1),
                            )
                          : const SizedBox(),
                      list.message != ""
                          ? SizedBox(
                              child: Text(
                                list.message!,
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: paymentSuccessDetailsTextStyle.copyWith(fontSize: 14, color: recommendedAmountBorder),
                              ),
                            )
                          : const SizedBox(),
                      Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(right: 3),
                          child: Image.asset("assets/images/paid_icon.png", height: 15, width: 15),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 3),
                          child: Row(
                            children: [
                              if (list.messageTransaction == "transaction" && list.transactionType == "debit") ...[
                                AdaptableText(
                                  "You paid ",
                                  style: paymentDateTextStyle,
                                ),
                              ] else if (list.messageTransaction == "request" && list.transactionType == "debit") ...[
                                AdaptableText(
                                  "You requested ",
                                  style: paymentDateTextStyle,
                                ),
                              ] else if (list.messageTransaction == "request" && list.transactionType == "credit") ...[
                                AdaptableText(
                                  "You were requested ",
                                  style: paymentDateTextStyle,
                                ),
                              ] else if (list.messageTransaction == "message")
                                ...[]
                              else ...[
                                AdaptableText(
                                  "You were paid ",
                                  style: paymentDateTextStyle,
                                ),
                              ],
                              AdaptableText(
                                list.succeededOn!,
                                style: paymentDateTextStyle,
                              )
                            ],
                          ),
                        ),
                      ]),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 3, 0, 5),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Image.asset("assets/images/paid_icon.png", height: 15, width: 15),
                            Flexible(
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                                child: AdaptableText("$youArePaid ${list.succeededOn!}", style: paymentDateTextStyle, textMaxLines: 1),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          const Expanded(
            child: SizedBox(),
          )
        ],
      ),
    );
  }

  Widget transferredWidget(BuildContext context, TransactionsList? list) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 2),
      child: Row(
        children: [
          const Expanded(
            child: SizedBox(),
          ),
          Expanded(
            child: Card(
              shadowColor: cardShadow,
              margin: const EdgeInsets.fromLTRB(5, 0, 5, 0),
              shape: const RoundedRectangleBorder(borderRadius: paymentTransactionsRadius),
              elevation: 3,
              color: CupertinoColors.white,
              child: InkWell(
                borderRadius: paymentTransactionsRadius,
                onTap: () async {},
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      list!.messageTransaction != "message"
                          ? const Align(
                        alignment: Alignment.centerRight,
                        child: Icon(Icons.arrow_right_alt_rounded, color: receivedIconBackground, size: 20),
                      )
                          : const SizedBox(),
                      list.messageTransaction != "message"
                          ? Padding(
                        padding: const EdgeInsets.fromLTRB(0, 5, 0, 3),
                        child: AdaptableText("\u0024${list.amount!.toStringAsFixed(2)}", style: paymentAmountTextStyle, textMaxLines: 1),
                      )
                          : const SizedBox(),
                      list.message != ""
                          ? SizedBox(
                        child: Text(
                          list.message!,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: paymentSuccessDetailsTextStyle.copyWith(fontSize: 14, color: recommendedAmountBorder),
                        ),
                      )
                          : const SizedBox(),
                      Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(right: 3),
                          child: Image.asset("assets/images/paid_icon.png", height: 15, width: 15),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 3),
                          child: Row(
                            children: [
                              if (list.messageTransaction == "transaction" && list.transactionType == "debit") ...[
                                AdaptableText(
                                  "You paid ",
                                  style: paymentDateTextStyle,
                                ),
                              ] else if (list.messageTransaction == "request" && list.transactionType == "debit") ...[
                                AdaptableText(
                                  "You requested ",
                                  style: paymentDateTextStyle,
                                ),
                              ] else if (list.messageTransaction == "request" && list.transactionType == "credit") ...[
                                AdaptableText(
                                  "You were requested ",
                                  style: paymentDateTextStyle,
                                ),
                              ] else if (list.messageTransaction == "message")
                                ...[]
                              else ...[
                                  AdaptableText(
                                    "You were paid ",
                                    style: paymentDateTextStyle,
                                  ),
                                ],
                              AdaptableText(
                                list.succeededOn!,
                                style: paymentDateTextStyle,
                              )
                            ],
                          ),
                        ),
                      ]),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 3, 0, 5),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Image.asset("assets/images/paid_icon.png", height: 15, width: 15),
                            Flexible(
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                                child: AdaptableText("$youArePaid ${list.succeededOn!}", style: paymentDateTextStyle, textMaxLines: 1),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget actions(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 10, 20, 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Flexible(
            child: Material(
              elevation: 5,
              borderRadius: payBorderRadius,
              child: ClipRRect(
                borderRadius: payBorderRadius,
                child: Container(
                  decoration: const BoxDecoration(color: transferredIconBackground, borderRadius: payBorderRadius),
                  child: TextButton(
                    onPressed: () {},
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                      child: AdaptableText(pay, style: paymentButtonsTextStyle, textMaxLines: 1),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Flexible(
            child: Material(
              elevation: 5,
              borderRadius: payBorderRadius,
              child: ClipRRect(
                borderRadius: payBorderRadius,
                child: Container(
                  decoration: const BoxDecoration(color: shareBackground, borderRadius: payBorderRadius),
                  child: TextButton(
                    onPressed: () {},
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                      child: AdaptableText(request, style: paymentButtonsTextStyle, textMaxLines: 1),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Flexible(
            child: Material(
              elevation: 5,
              borderRadius: payBorderRadius,
              child: ClipRRect(
                borderRadius: payBorderRadius,
                child: Container(
                  decoration: const BoxDecoration(color: receivedIconBackground, borderRadius: payBorderRadius),
                  child: TextButton(
                    onPressed: () {},
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                      child: AdaptableText(messageHint, style: paymentButtonsTextStyle, textMaxLines: 1),
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  onSelected(BuildContext context, int item) {
    switch (item) {
      case 0:
        blockUserDialog(context);
        break;
    }
  }

  blockUserDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(block, style: logoutHeaderTextStyle),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            content:
                Text(isUserBlocked ? "Do you want to $unBlock this user?" : "Do you want to $block this user?", style: logoutContentHeaderTextStyle),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: TextButton(
                  onPressed: () {
                    final viewModel = Provider.of<UserToUserNotifier>(context, listen: false);
                    if (isUserBlocked) {
                      viewModel.unBlockUserAPI(userId, friendId).then((value) {
                        if (viewModel.unBlockResponseModel != null) {
                          if (viewModel.unBlockResponseModel!.responseStatus == 1) {
                            showToast(viewModel.unBlockResponseModel!.result!);
                            getTransactions();
                            Navigator.of(context).pop();
                          } else {
                            showToast(viewModel.unBlockResponseModel!.result!);
                          }
                        } else {
                          showToast(tryAgain);
                        }
                      });
                    } else {
                      viewModel.blockUserAPI(userId, friendId).then((value) {
                        if (viewModel.blockResponseModel != null) {
                          if (viewModel.blockResponseModel!.responseStatus == 1) {
                            showToast(viewModel.blockResponseModel!.result!);
                            getTransactions();
                            Navigator.of(context).pop();
                          } else {
                            showToast(viewModel.blockResponseModel!.result!);
                          }
                        } else {
                          showToast(tryAgain);
                        }
                      });
                    }
                  },
                  child: Text(yes, style: logoutButtonHeaderTextStyle),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(no, style: logoutButtonHeaderTextStyle),
                ),
              )
            ],
          );
        });
  }
}
