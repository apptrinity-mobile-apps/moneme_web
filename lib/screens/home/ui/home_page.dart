import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:mone_me/models/transaction_history_response_model.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/payment_source_enum.dart';
import 'package:mone_me/utils/routes.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/utils/transaction_type_enum.dart';
import 'package:mone_me/widgets/adaptable_text.dart';
import 'package:mone_me/widgets/common_body.dart';
import 'package:mone_me/widgets/common_header.dart';
import 'package:mone_me/screens/home/notifier/home_notifier.dart';
import 'package:mone_me/screens/home/notifier/transactions_notifier.dart';
import 'package:mone_me/screens/login/login_notifier/login_notifier.dart';
import 'package:mone_me/utils/screen_config.dart';
import 'package:mone_me/widgets/list_loading_shimmer.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {
  late HomeNotifier viewModelHome;
  late TransactionsNotifier viewModelTransactions;
  TextEditingController searchController = TextEditingController();
  TextEditingController dialogStartDateController = TextEditingController();
  TextEditingController dialogEndDateController = TextEditingController();
  String userId = "", startDate = "", endDate = "", startDateDisplay = "", endDateDisplay = "";
  bool allTransactionsLoading = true,
      transferredTransactionsLoading = true,
      receivedTransactionsLoading = true,
      requestsTransactionsLoading = true,
      requestedTransactionsLoading = true,
      showFilterDates = false;
  int btnToday = 0, btnYesterday = 0;
  DateTime? transactionsStartDate;

  @override
  void initState() {
    initialApis();
    //StripeServices.init();
    super.initState();
  }

  initialApis() {
    Future.delayed(Duration.zero, () async {
      final homeViewModel = Provider.of<HomeNotifier>(context, listen: false);
      await Provider.of<LoginNotifier>(context, listen: false).getUserDetails();
      userId = Provider.of<LoginNotifier>(context, listen: false).userId!;
      homeViewModel.getDashboardAPI(userId, DateTime.now().timeZoneName);
      homeViewModel.getAllFriendsAPI(userId);
      getTransactions();
      /*homeViewModel.viewCustomerStripeAPI(userId).then((value) {
        if (homeViewModel.viewCustomerStripeResponse != null) {
          if (homeViewModel.viewCustomerStripeResponse!.responseStatus == 1) {
            debugPrint("stripeid " + homeViewModel.viewCustomerStripeResponse!.toJson().toString());
            if (homeViewModel.viewCustomerStripeResponse!.stripeCustomerId == null || homeViewModel.viewCustomerStripeResponse!.stripeCustomerId == "") {
              StripeServices.createCustomerStripe(Uri.parse('${StripeServices.apiBase}/customers'), homeViewModel.userName!, homeViewModel.userEmail!)
                  .then((value) {
                debugPrint("stripeid-- " + value.id!);
                homeViewModel.updateCustomerStripeAPI(userId, value.id!).then((value) {});
              });
            }
          }
        }
      });*/
    });
  }

  getTransactions() {
    setState(() {
      allTransactionsLoading = true;
      transferredTransactionsLoading = true;
      receivedTransactionsLoading = true;
      requestsTransactionsLoading = true;
      requestedTransactionsLoading = true;
    });
    final viewModel = Provider.of<TransactionsNotifier>(context, listen: false);
    Provider.of<TransactionsNotifier>(context, listen: false).geAllTransactionsAPI(userId, startDate, endDate).then((value) {
      allTransactionsLoading = false;
      var dateNow = DateTime.now();
      var outputFormat = DateFormat('yyyy-MM-dd');
      var date = outputFormat.format(dateNow);
      if (viewModel.allTransactions != null) {
        if (viewModel.allTransactionsList().isNotEmpty) {
          transactionsStartDate = DateFormat('MM-dd-yyyy').parse(viewModel.allTransactions!.txnStartDate!);
        } else {
          transactionsStartDate = DateFormat("MM-dd-yyyy").parse(date);
        }
      } else {
        transactionsStartDate = DateFormat("MM-dd-yyyy").parse(date);
      }
    });
    Provider.of<TransactionsNotifier>(context, listen: false).getAllTransferredTransactionsAPI(userId, startDate, endDate).then((value) {
      transferredTransactionsLoading = false;
    });
    Provider.of<TransactionsNotifier>(context, listen: false).getAllReceivedTransactionsAPI(userId, startDate, endDate).then((value) {
      receivedTransactionsLoading = false;
    });
    Provider.of<TransactionsNotifier>(context, listen: false).getAllRequestsTransactionsAPI(userId, startDate, endDate).then((value) {
      requestsTransactionsLoading = false;
      requestedTransactionsLoading = false;
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    viewModelHome = Provider.of<HomeNotifier>(context, listen: true);
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          body: Container(
            color: CupertinoColors.white,
            child: Stack(
              children: [
                SizedBox(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  child: Image.asset("assets/images/login_background_logo.png", fit: BoxFit.fill),
                ),
                Positioned(
                  child: Container(
                    height: ScreenConfig.height(context),
                    width: ScreenConfig.width(context),
                    color: Colors.transparent,
                    child: Column(
                      children: [
                        const CommonHeader(),
                        Expanded(
                          child: body(),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget body() {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.all(10),
      child: Row(
        children: [
          const CommonBody(),
          Expanded(
            child: Container(
              height: ScreenConfig.height(context),
              width: ScreenConfig.width(context),
              margin: const EdgeInsets.only(left: 3),
              child: transactionsHistoryWidget(context),
            ),
          )
        ],
      ),
    );
  }

  /*Widget dynamicFragments() {
    debugPrint("page---" + viewModelHome.innerPage);
    if (viewModelHome.innerPage == InnerPages.transactionHistory.name) {
      return transactionsHistoryWidget(context);
    } else if (viewModelHome.innerPage == InnerPages.profileTransactions.name) {
      return profileTransactionDetailsWidget(context);
    } else if (viewModelHome.innerPage == InnerPages.addMoneySend.name) {
      return addMoneyForSendingWidget(context);
    } else if (viewModelHome.innerPage == InnerPages.confirmOTPAfterAmount.name) {
      return addMoneyForSendingOtpWidget(context);
    } else if (viewModelHome.innerPage == InnerPages.myWallet.name) {
      return walletWidget(context);
    } else if (viewModelHome.innerPage == InnerPages.addMoneyWallet.name) {
      return addMoneyToWalletWidget(context);
    } else if (viewModelHome.innerPage == InnerPages.addMoneyWalletSuccess.name) {
      return addMoneyWalletSuccessWidget(context);
    } else if (viewModelHome.innerPage == InnerPages.transactionDetails.name) {
      return transactionDetailsWidget(context);
    } else {
      return transactionsHistoryWidget(context);
    }
  }*/

  Widget transactionsHistoryWidget(BuildContext context) {
    var allTransactions = Provider.of<TransactionsNotifier>(context, listen: true).allTransactionsList();
    var transferredTransactions = Provider.of<TransactionsNotifier>(context, listen: true).allTransferredTransactionsList();
    var receivedTransactions = Provider.of<TransactionsNotifier>(context, listen: true).allReceivedTransactionsList();
    var requestedTransactions = Provider.of<TransactionsNotifier>(context, listen: true).allRequestedList();
    var requestsTransactions = Provider.of<TransactionsNotifier>(context, listen: true).allRequestsList();
    return Card(
      shadowColor: cardShadow,
      shape: const RoundedRectangleBorder(borderRadius: detailsMainBorderRadius),
      elevation: 5,
      child: Column(
        children: [
          Container(
            width: ScreenConfig.width(context),
            padding: const EdgeInsets.fromLTRB(20, 20, 20, 15),
            color: headerColor,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: AdaptableText(transactionsHistory, style: showTextStyle, textMaxLines: 1),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Material(
                    type: MaterialType.transparency,
                    child: InkWell(
                      onTap: () {
                        showFilterDialog();
                      },
                      child: Image.asset("images/date_settings.png", height: 35, width: 35),
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 25),
              child: DefaultTabController(
                length: 5,
                child: Padding(
                  padding: const EdgeInsets.only(left: 5, right: 10),
                  child: Scaffold(
                    backgroundColor: CupertinoColors.white,
                    appBar: PreferredSize(
                      preferredSize: const Size.fromHeight(kToolbarHeight - 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: TabBar(
                          padding: const EdgeInsets.only(left: 15, right: 15),
                          // indicatorWeight: 0.5,
                          isScrollable: true,
                          indicatorSize: TabBarIndicatorSize.label,
                          indicatorColor: tabsHeaderColor,
                          labelStyle: tabSelectedTextStyle,
                          unselectedLabelStyle: tabUnSelectedTextStyle,
                          labelColor: tabsHeaderColor,
                          unselectedLabelColor: tabsHeaderColor.withOpacity(0.5),
                          labelPadding: const EdgeInsets.only(left: 5, right: 5),
                          tabs: const <Widget>[
                            Tab(text: all),
                            Tab(text: transferred),
                            Tab(text: received),
                            Tab(text: requested),
                            Tab(text: myRequests)
                          ],
                        ),
                      ),
                    ),
                    body: TabBarView(
                      children: <Widget>[
                        allTransactions.isNotEmpty
                            ? ListView.builder(
                                padding: const EdgeInsets.only(left: 10, right: 10),
                                controller: ScrollController(),
                                shrinkWrap: true,
                                itemCount: allTransactions.length,
                                itemBuilder: (context, index) {
                                  return allTransactionsListItem(allTransactions[index]);
                                })
                            : allTransactionsLoading
                                ? listLoadingShimmer()
                                : Center(
                                    child: AdaptableText(noTransactions, style: showTextStyle),
                                  ),
                        transferredTransactions.isNotEmpty
                            ? ListView.builder(
                                padding: const EdgeInsets.only(left: 10, right: 10),
                                controller: ScrollController(),
                                shrinkWrap: true,
                                itemCount: transferredTransactions.length,
                                itemBuilder: (context, index) {
                                  return transferredTransactionsListItem(transferredTransactions[index]);
                                })
                            : transferredTransactionsLoading
                                ? listLoadingShimmer()
                                : Center(
                                    child: AdaptableText(noTransferredTransactions, style: showTextStyle),
                                  ),
                        receivedTransactions.isNotEmpty
                            ? ListView.builder(
                                padding: const EdgeInsets.only(left: 10, right: 10),
                                controller: ScrollController(),
                                shrinkWrap: true,
                                itemCount: receivedTransactions.length,
                                itemBuilder: (context, index) {
                                  return receivedTransactionsListItem(receivedTransactions[index]);
                                })
                            : receivedTransactionsLoading
                                ? listLoadingShimmer()
                                : Center(
                                    child: AdaptableText(noReceivedTransactions, style: showTextStyle),
                                  ),
                        requestedTransactions.isNotEmpty
                            ? ListView.builder(
                                padding: const EdgeInsets.only(left: 10, right: 10),
                                controller: ScrollController(),
                                shrinkWrap: true,
                                itemCount: requestedTransactions.length,
                                itemBuilder: (context, index) {
                                  return requestedTransactionsListItem(requestedTransactions[index]);
                                })
                            : requestsTransactionsLoading
                                ? listLoadingShimmer()
                                : Center(
                                    child: AdaptableText(noRequests, style: showTextStyle),
                                  ),
                        requestsTransactions.isNotEmpty
                            ? ListView.builder(
                                padding: const EdgeInsets.only(left: 10, right: 10),
                                controller: ScrollController(),
                                shrinkWrap: true,
                                itemCount: requestsTransactions.length,
                                itemBuilder: (context, index) {
                                  return requestsTransactionsListItem(requestsTransactions[index]);
                                })
                            : requestedTransactionsLoading
                                ? listLoadingShimmer()
                                : Center(
                                    child: AdaptableText(noMyRequests, style: showTextStyle),
                                  ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  allTransactionsListItem(TransactionsList? transaction) {
    return InkWell(
      onTap: () {
        transaction!.serviceType != null && transaction.serviceType == 'parking'
            ? /*Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) => parkingdetailscreen(
                                                  bookingId: transaction!.bookingId,
                                                ),
                                              ),
                                            )*/
            // TODO navigate parking details screen
            null
            : Navigator.pushNamed(context, AppRoutes.transactionDetails, arguments: transaction.id);
      },
      child: Container(
        width: double.infinity,
        margin: const EdgeInsets.all(5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 2,
              child: Row(children: <Widget>[
                transaction!.serviceType != null && transaction.serviceType == 'parking'
                    ? Container(
                        height: 54,
                        width: 54,
                        decoration: const BoxDecoration(
                          color: transferredIconBackground,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(2),
                            topRight: Radius.circular(2),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        child: ClipRRect(
                          borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(2),
                            topRight: Radius.circular(2),
                            bottomRight: Radius.circular(20),
                          ),
                          child: Image.asset(
                            'images/car_parking.png',
                            fit: BoxFit.fill,
                            errorBuilder: (context, exception, stackTrace) {
                              return const Center(
                                child: AdaptableText(
                                  "Parking payment",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 18, fontFamily: 'Aller', fontWeight: FontWeight.w700, color: Colors.white),
                                ),
                              );
                            },
                          ),
                        ),
                      )
                    : Container(
                        height: 54,
                        width: 54,
                        decoration: const BoxDecoration(
                          color: transferredIconBackground,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(2),
                            topRight: Radius.circular(2),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        child: ClipRRect(
                          borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(2),
                            topRight: Radius.circular(2),
                            bottomRight: Radius.circular(20),
                          ),
                          child: Image.network(
                            transaction.paymentSource! == PaymentSource.wallet.name && transaction.transactionType! == TransactionTypes.credit.name
                                ? transaction.senderImage!.isNotEmpty
                                    ? transaction.senderImage!
                                    : transaction.senderName![0]
                                : transaction.receiverImage!.isNotEmpty
                                    ? transaction.receiverImage!
                                    : transaction.receiverName![0],
                            errorBuilder: (context, exception, stacktrace) {
                              return Center(
                                child: AdaptableText(
                                  transaction.paymentSource! == PaymentSource.wallet.name &&
                                          transaction.transactionType! == TransactionTypes.debit.name
                                      ? transaction.senderName![0]
                                      : transaction.receiverName![0],
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(fontSize: 18, fontFamily: 'Aller', fontWeight: FontWeight.w700, color: Colors.white),
                                ),
                              );
                            },
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 12),
                        child: AdaptableText(
                          transaction.paymentSource! == PaymentSource.wallet.name && transaction.transactionType! == TransactionTypes.credit.name ||
                                  transaction.paymentSource! == PaymentSource.user.name &&
                                      transaction.transactionType! == TransactionTypes.debit.name ||
                                  transaction.paymentSource! == PaymentSource.user.name &&
                                      transaction.transactionType! == TransactionTypes.credit.name
                              ? transaction.receiverName!
                              : transaction.paymentSource! == PaymentSource.wallet.name && transaction.transactionType! == TransactionTypes.debit.name
                                  ? transaction.senderName!
                                  : transaction.serviceType != null && transaction.serviceType! == 'parking'
                                      ? "Parking"
                                      : "",
                          style: const TextStyle(fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w500, color: nameText),
                          textMaxLines: 1,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 12),
                        child: AdaptableText(
                          transaction.paymentSource == PaymentSource.wallet.name && transaction.transactionType == TransactionTypes.credit.name
                              ? transferredToWallet
                              : transaction.paymentSource == PaymentSource.wallet.name && transaction.transactionType == TransactionTypes.debit.name
                                  ? transferredFromWallet
                                  : transaction.paymentSource == PaymentSource.user.name &&
                                          transaction.transactionType == TransactionTypes.credit.name
                                      ? received
                                      : transaction.paymentSource == PaymentSource.user.name &&
                                              transaction.transactionType == TransactionTypes.debit.name
                                          ? transferred
                                          : transaction.serviceType != null && transaction.serviceType == 'parking'
                                              ? transferredFromWallet
                                              : "",
                          style: TextStyle(
                            fontSize: 12,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                            color: nameText.withOpacity(0.40),
                          ),
                          textMaxLines: 1,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 12),
                        child: AdaptableText(
                          "\u0024${commaSeparatedAmount.format(double.parse(transaction.currentBalance!))}",
                          style: TextStyle(
                            fontSize: 14,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                            color: nameText.withOpacity(0.40),
                          ),
                          textMaxLines: 1,
                        ),
                      )
                    ],
                  ),
                )
              ]),
            ),
            Expanded(
              child: Row(children: <Widget>[
                Flexible(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 0),
                        child: AdaptableText(
                          transaction.createdOn.toString(),
                          style: TextStyle(
                            fontSize: 11,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                            color: nameText.withOpacity(0.60),
                          ),
                          textMaxLines: 1,
                          textAlign: TextAlign.right,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 0),
                        alignment: Alignment.topRight,
                        child: RichText(
                          maxLines: 1,
                          text: TextSpan(children: [
                            TextSpan(
                              text: transaction.transactionType == TransactionTypes.credit.name ? "+" : "-",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: 'Aller',
                                  fontWeight: FontWeight.w700,
                                  color: transaction.transactionType == TransactionTypes.debit.name ? Colors.red : addMoneyText),
                            ),
                            TextSpan(
                              text: transaction.amount != null ? "\u0024${commaSeparatedAmount.format(double.parse(transaction.amount!))}" : "00.00",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: 'Aller',
                                  fontWeight: FontWeight.w700,
                                  color: transaction.transactionType == TransactionTypes.debit.name ? Colors.red : addMoneyText),
                            ),
                          ]),
                        ),
                      ),
                    ],
                  ),
                )
              ]),
            ),
          ],
        ),
      ),
    );
  }

  transferredTransactionsListItem(TransactionsList? transaction) {
    return InkWell(
      onTap: () {
        transaction!.serviceType != null && transaction.serviceType == 'parking'
            ? /*Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) => parkingdetailscreen(
                                                  bookingId: transaction!.bookingId,
                                                ),
                                              ),
                                            )*/
            // TODO navigate parking details screen
            null
            : Navigator.pushNamed(context, AppRoutes.transactionDetails, arguments: transaction.id);
      },
      child: Container(
        width: double.infinity,
        margin: const EdgeInsets.all(5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 2,
              child: Row(children: <Widget>[
                transaction!.serviceType != null && transaction.serviceType == 'parking'
                    ? Container(
                        height: 54,
                        width: 54,
                        decoration: const BoxDecoration(
                          color: transferredIconBackground,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(2),
                            topRight: Radius.circular(2),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        child: ClipRRect(
                          borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(2),
                            topRight: Radius.circular(2),
                            bottomRight: Radius.circular(20),
                          ),
                          child: Image.asset(
                            'images/car_parking.png',
                            fit: BoxFit.fill,
                            errorBuilder: (context, exception, stackTrace) {
                              return const Center(
                                child: AdaptableText(
                                  "Parking payment",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 18, fontFamily: 'Aller', fontWeight: FontWeight.w700, color: Colors.white),
                                ),
                              );
                            },
                          ),
                        ),
                      )
                    : Container(
                        height: 54,
                        width: 54,
                        decoration: const BoxDecoration(
                          color: transferredIconBackground,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(2),
                            topRight: Radius.circular(2),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        child: ClipRRect(
                          borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(2),
                            topRight: Radius.circular(2),
                            bottomRight: Radius.circular(20),
                          ),
                          child: Image.network(
                            transaction.paymentSource! == PaymentSource.wallet.name && transaction.transactionType! == TransactionTypes.credit.name
                                ? transaction.senderImage!.isNotEmpty
                                    ? transaction.senderImage!
                                    : transaction.senderName![0]
                                : transaction.receiverImage!.isNotEmpty
                                    ? transaction.receiverImage!
                                    : transaction.receiverName![0],
                            errorBuilder: (context, exception, stacktrace) {
                              return Center(
                                child: AdaptableText(
                                  transaction.paymentSource! == PaymentSource.wallet.name &&
                                          transaction.transactionType! == TransactionTypes.debit.name
                                      ? transaction.senderName![0]
                                      : transaction.receiverName![0],
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(fontSize: 18, fontFamily: 'Aller', fontWeight: FontWeight.w700, color: Colors.white),
                                ),
                              );
                            },
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 12),
                        child: AdaptableText(
                          transaction.paymentSource! == PaymentSource.wallet.name && transaction.transactionType! == TransactionTypes.credit.name ||
                                  transaction.paymentSource! == PaymentSource.user.name &&
                                      transaction.transactionType! == TransactionTypes.debit.name ||
                                  transaction.paymentSource! == PaymentSource.user.name &&
                                      transaction.transactionType! == TransactionTypes.credit.name
                              ? transaction.receiverName!
                              : transaction.paymentSource! == PaymentSource.wallet.name && transaction.transactionType! == TransactionTypes.debit.name
                                  ? transaction.senderName!
                                  : transaction.serviceType != null && transaction.serviceType! == 'parking'
                                      ? "Parking"
                                      : "",
                          style: const TextStyle(fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w500, color: nameText),
                          textMaxLines: 1,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 12),
                        child: AdaptableText(
                          transaction.paymentSource == PaymentSource.wallet.name && transaction.transactionType == TransactionTypes.credit.name
                              ? transferredToWallet
                              : transaction.paymentSource == PaymentSource.wallet.name && transaction.transactionType == TransactionTypes.debit.name
                                  ? transferredFromWallet
                                  : transaction.paymentSource == PaymentSource.user.name &&
                                          transaction.transactionType == TransactionTypes.credit.name
                                      ? received
                                      : transaction.paymentSource == PaymentSource.user.name &&
                                              transaction.transactionType == TransactionTypes.debit.name
                                          ? transferred
                                          : transaction.serviceType != null && transaction.serviceType == 'parking'
                                              ? transferredFromWallet
                                              : "",
                          style: TextStyle(
                            fontSize: 12,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                            color: nameText.withOpacity(0.40),
                          ),
                          textMaxLines: 1,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 12),
                        child: AdaptableText(
                          "\u0024${commaSeparatedAmount.format(double.parse(transaction.currentBalance!))}",
                          style: TextStyle(
                            fontSize: 14,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                            color: nameText.withOpacity(0.40),
                          ),
                          textMaxLines: 1,
                        ),
                      )
                    ],
                  ),
                )
              ]),
            ),
            Expanded(
              child: Row(children: <Widget>[
                Flexible(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 0),
                        child: AdaptableText(
                          transaction.createdOn.toString(),
                          style: TextStyle(
                            fontSize: 11,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                            color: nameText.withOpacity(0.60),
                          ),
                          textMaxLines: 1,
                          textAlign: TextAlign.right,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 0),
                        alignment: Alignment.topRight,
                        child: RichText(
                          maxLines: 1,
                          text: TextSpan(children: [
                            TextSpan(
                              text: transaction.transactionType == TransactionTypes.credit.name ? "+" : "-",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: 'Aller',
                                  fontWeight: FontWeight.w700,
                                  color: transaction.transactionType == TransactionTypes.debit.name ? Colors.red : addMoneyText),
                            ),
                            TextSpan(
                              text: transaction.amount != null ? "\u0024${commaSeparatedAmount.format(double.parse(transaction.amount!))}" : "00.00",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: 'Aller',
                                  fontWeight: FontWeight.w700,
                                  color: transaction.transactionType == TransactionTypes.debit.name ? Colors.red : addMoneyText),
                            ),
                          ]),
                        ),
                      ),
                    ],
                  ),
                )
              ]),
            ),
          ],
        ),
      ),
    );
  }

  receivedTransactionsListItem(TransactionsList? transaction) {
    return InkWell(
      onTap: () {
        transaction!.serviceType != null && transaction.serviceType == 'parking'
            ? /*Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) => parkingdetailscreen(
                                                  bookingId: transaction!.bookingId,
                                                ),
                                              ),
                                            )*/
            // TODO navigate parking details screen
            null
            : Navigator.pushNamed(context, AppRoutes.transactionDetails, arguments: transaction.id);
      },
      child: Container(
        width: double.infinity,
        margin: const EdgeInsets.all(5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 2,
              child: Row(children: <Widget>[
                transaction!.serviceType != null && transaction.serviceType == 'parking'
                    ? Container(
                        height: 54,
                        width: 54,
                        decoration: const BoxDecoration(
                          color: transferredIconBackground,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(2),
                            topRight: Radius.circular(2),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        child: ClipRRect(
                          borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(2),
                            topRight: Radius.circular(2),
                            bottomRight: Radius.circular(20),
                          ),
                          child: Image.asset(
                            'images/car_parking.png',
                            fit: BoxFit.fill,
                            errorBuilder: (context, exception, stackTrace) {
                              return const Center(
                                child: AdaptableText(
                                  "Parking payment",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 18, fontFamily: 'Aller', fontWeight: FontWeight.w700, color: Colors.white),
                                ),
                              );
                            },
                          ),
                        ),
                      )
                    : Container(
                        height: 54,
                        width: 54,
                        decoration: const BoxDecoration(
                          color: transferredIconBackground,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(2),
                            topRight: Radius.circular(2),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        child: ClipRRect(
                          borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(2),
                            topRight: Radius.circular(2),
                            bottomRight: Radius.circular(20),
                          ),
                          child: Image.network(
                            transaction.paymentSource! == PaymentSource.wallet.name && transaction.transactionType! == TransactionTypes.credit.name
                                ? transaction.senderImage!.isNotEmpty
                                    ? transaction.senderImage!
                                    : transaction.senderName![0]
                                : transaction.receiverImage!.isNotEmpty
                                    ? transaction.receiverImage!
                                    : transaction.receiverName![0],
                            errorBuilder: (context, exception, stacktrace) {
                              return Center(
                                child: AdaptableText(
                                  transaction.paymentSource! == PaymentSource.wallet.name &&
                                          transaction.transactionType! == TransactionTypes.debit.name
                                      ? transaction.senderName![0]
                                      : transaction.receiverName![0],
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(fontSize: 18, fontFamily: 'Aller', fontWeight: FontWeight.w700, color: Colors.white),
                                ),
                              );
                            },
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 12),
                        child: AdaptableText(
                          transaction.paymentSource! == PaymentSource.wallet.name && transaction.transactionType! == TransactionTypes.credit.name ||
                                  transaction.paymentSource! == PaymentSource.user.name &&
                                      transaction.transactionType! == TransactionTypes.debit.name ||
                                  transaction.paymentSource! == PaymentSource.user.name &&
                                      transaction.transactionType! == TransactionTypes.credit.name
                              ? transaction.receiverName!
                              : transaction.paymentSource! == PaymentSource.wallet.name && transaction.transactionType! == TransactionTypes.debit.name
                                  ? transaction.senderName!
                                  : transaction.serviceType != null && transaction.serviceType! == 'parking'
                                      ? "Parking"
                                      : "",
                          style: const TextStyle(fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w500, color: nameText),
                          textMaxLines: 1,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 12),
                        child: AdaptableText(
                          transaction.paymentSource == PaymentSource.wallet.name && transaction.transactionType == TransactionTypes.credit.name
                              ? transferredToWallet
                              : transaction.paymentSource == PaymentSource.wallet.name && transaction.transactionType == TransactionTypes.debit.name
                                  ? transferredFromWallet
                                  : transaction.paymentSource == PaymentSource.user.name &&
                                          transaction.transactionType == TransactionTypes.credit.name
                                      ? received
                                      : transaction.paymentSource == PaymentSource.user.name &&
                                              transaction.transactionType == TransactionTypes.debit.name
                                          ? transferred
                                          : transaction.serviceType != null && transaction.serviceType == 'parking'
                                              ? transferredFromWallet
                                              : "",
                          style: TextStyle(
                            fontSize: 12,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                            color: nameText.withOpacity(0.40),
                          ),
                          textMaxLines: 1,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 12),
                        child: AdaptableText(
                          "\u0024${commaSeparatedAmount.format(double.parse(transaction.currentBalance!))}",
                          style: TextStyle(
                            fontSize: 14,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                            color: nameText.withOpacity(0.40),
                          ),
                          textMaxLines: 1,
                        ),
                      )
                    ],
                  ),
                )
              ]),
            ),
            Expanded(
              child: Row(children: <Widget>[
                Flexible(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 0),
                        child: AdaptableText(
                          transaction.createdOn.toString(),
                          style: TextStyle(
                            fontSize: 11,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                            color: nameText.withOpacity(0.60),
                          ),
                          textMaxLines: 1,
                          textAlign: TextAlign.right,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 0),
                        alignment: Alignment.topRight,
                        child: RichText(
                          maxLines: 1,
                          text: TextSpan(children: [
                            TextSpan(
                              text: transaction.transactionType == TransactionTypes.credit.name ? "+" : "-",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: 'Aller',
                                  fontWeight: FontWeight.w700,
                                  color: transaction.transactionType == TransactionTypes.debit.name ? Colors.red : addMoneyText),
                            ),
                            TextSpan(
                              text: transaction.amount != null ? "\u0024${commaSeparatedAmount.format(double.parse(transaction.amount!))}" : "00.00",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: 'Aller',
                                  fontWeight: FontWeight.w700,
                                  color: transaction.transactionType == TransactionTypes.debit.name ? Colors.red : addMoneyText),
                            ),
                          ]),
                        ),
                      ),
                    ],
                  ),
                )
              ]),
            ),
          ],
        ),
      ),
    );
  }

  requestedTransactionsListItem(TransactionsList? transaction) {
    return InkWell(
      onTap: () {
        transaction!.status != null && transaction.status == 1
            ? Navigator.pushNamed(context, AppRoutes.transactionDetails, arguments: transaction.id)
            : /*Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => SendMoneyScreen(
                    friendId: transaction!.receiverId,
                    sendType: "pay",
                    requestTransactionId: transaction!.transactionUniqueId,
                    requestMessage: transaction!.message,
                    requestedAmount: transaction!.amount.toString(),
                  ),
                ),
              ).then((value) {
                getTransactions();
              })*/
            // TODO send money screen
            null;
      },
      child: Container(
        width: double.infinity,
        margin: const EdgeInsets.all(5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 2,
              child: Row(children: <Widget>[
                Container(
                  height: 54,
                  width: 54,
                  decoration: const BoxDecoration(
                    color: transferredIconBackground,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      bottomLeft: Radius.circular(2),
                      topRight: Radius.circular(2),
                      bottomRight: Radius.circular(20),
                    ),
                  ),
                  child: ClipRRect(
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(20),
                      bottomLeft: Radius.circular(2),
                      topRight: Radius.circular(2),
                      bottomRight: Radius.circular(20),
                    ),
                    child: Image.network(
                      transaction!.receiverImage!.isNotEmpty ? transaction.receiverImage! : transaction.receiverName![0],
                      errorBuilder: (context, exception, stackTrace) {
                        return Center(
                          child: AdaptableText(
                            transaction.receiverName![0],
                            textAlign: TextAlign.center,
                            style: const TextStyle(fontSize: 18, fontFamily: 'Aller', fontWeight: FontWeight.w700, color: Colors.white),
                          ),
                        );
                      },
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 12),
                        child: AdaptableText(
                          transaction.receiverName!,
                          style: const TextStyle(fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w500, color: nameText),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 12),
                        child: AdaptableText(
                          transaction.status == 0 ? "Pending" : "Paid",
                          style: TextStyle(
                            fontSize: 12,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                            color: nameText.withOpacity(0.40),
                          ),
                        ),
                      ),
                      transaction.status == 0
                          ? const SizedBox()
                          : Container(
                              margin: const EdgeInsets.only(left: 12),
                              child: AdaptableText(
                                "\u0024${commaSeparatedAmount.format(double.parse(transaction.currentBalance!))}",
                                style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w400,
                                  color: nameText.withOpacity(0.40),
                                ),
                              ),
                            )
                    ],
                  ),
                )
              ]),
            ),
            Expanded(
              child: Row(children: <Widget>[
                Flexible(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 0),
                        child: AdaptableText(
                          transaction.createdOn.toString(),
                          textMaxLines: 1,
                          style: TextStyle(
                            fontSize: 11,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                            color: nameText.withOpacity(0.60),
                          ),
                          textAlign: TextAlign.right,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 0),
                        alignment: Alignment.topRight,
                        child: RichText(
                          text: TextSpan(children: [
                            TextSpan(
                              text: transaction.status == 1 ? "-" : "",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: 'Aller',
                                  fontWeight: FontWeight.w700,
                                  color: transaction.status == 1 ? Colors.red : Colors.grey),
                            ),
                            TextSpan(
                              text: transaction.amount != null ? "\u0024${commaSeparatedAmount.format(double.parse(transaction.amount!))}" : "00.00",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: 'Aller',
                                  fontWeight: FontWeight.w700,
                                  color: transaction.status == 1 ? Colors.red : Colors.grey),
                            ),
                          ]),
                        ),
                      ),
                    ],
                  ),
                )
              ]),
            ),
          ],
        ),
      ),
    );
  }

  requestsTransactionsListItem(TransactionsList? transaction) {
    return InkWell(
      onTap: () {
        transaction!.status != null && transaction.status == 1
            ? Navigator.pushNamed(context, AppRoutes.transactionDetails, arguments: transaction.id)
            : const SizedBox();
      },
      child: Container(
        width: double.infinity,
        margin: const EdgeInsets.all(5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 2,
              child: Row(children: <Widget>[
                Container(
                  height: 54,
                  width: 54,
                  decoration: const BoxDecoration(
                    color: transferredIconBackground,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      bottomLeft: Radius.circular(2),
                      topRight: Radius.circular(2),
                      bottomRight: Radius.circular(20),
                    ),
                  ),
                  child: ClipRRect(
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(20),
                      bottomLeft: Radius.circular(2),
                      topRight: Radius.circular(2),
                      bottomRight: Radius.circular(20),
                    ),
                    child: Image.network(
                      transaction!.receiverImage!.isNotEmpty ? transaction.receiverImage! : transaction.receiverName![0],
                      errorBuilder: (context, exception, stackTrace) {
                        return Center(
                          child: AdaptableText(
                            transaction.receiverName![0],
                            textAlign: TextAlign.center,
                            style: const TextStyle(fontSize: 18, fontFamily: 'AllerBold', fontWeight: FontWeight.w700, color: Colors.white),
                          ),
                        );
                      },
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 12),
                        child: AdaptableText(
                          transaction.receiverName!,
                          style: const TextStyle(fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w500, color: nameText),
                          textMaxLines: 1,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 12),
                        child: AdaptableText(
                          transaction.status == 0 ? "Pending" : "Paid",
                          style: TextStyle(
                            fontSize: 12,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                            color: nameText.withOpacity(0.40),
                          ),
                        ),
                      ),
                      transaction.status == 0
                          ? const SizedBox()
                          : Container(
                              margin: const EdgeInsets.only(left: 12),
                              child: AdaptableText(
                                "\u0024${commaSeparatedAmount.format(double.parse(transaction.currentBalance!))}",
                                style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w400,
                                  color: nameText.withOpacity(0.40),
                                ),
                              ),
                            )
                    ],
                  ),
                )
              ]),
            ),
            Expanded(
              child: Row(children: <Widget>[
                Flexible(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 0),
                        child: AdaptableText(
                          transaction.createdOn.toString(),
                          style: TextStyle(
                            fontSize: 11,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                            color: nameText.withOpacity(0.60),
                          ),
                          textMaxLines: 1,
                          textAlign: TextAlign.right,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 0),
                        alignment: Alignment.topRight,
                        child: RichText(
                          maxLines: 1,
                          text: TextSpan(children: [
                            TextSpan(
                              text: transaction.status == 1 ? "+" : "",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: 'Aller',
                                  fontWeight: FontWeight.w700,
                                  color: transaction.status == 1 ? addMoneyText : Colors.grey),
                            ),
                            TextSpan(
                              text: transaction.amount != null ? "\u0024${commaSeparatedAmount.format(double.parse(transaction.amount!))}" : "00.00",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: 'Aller',
                                  fontWeight: FontWeight.w700,
                                  color: transaction.status == 1 ? addMoneyText : Colors.grey),
                            ),
                          ]),
                        ),
                      ),
                    ],
                  ),
                )
              ]),
            ),
          ],
        ),
      ),
    );
  }

  showFilterDialog() {
    OutlineInputBorder border = OutlineInputBorder(
      borderSide: const BorderSide(color: dottedLine, width: 1.5),
      borderRadius: BorderRadius.circular(5),
    );
    showDialog(
        context: context,
        builder: (_) {
          return StatefulBuilder(builder: (ctx, _setState) {
            return AlertDialog(
              contentPadding: const EdgeInsets.all(15),
              content: SizedBox(
                // height: 500,
                width: 300,
                child: SingleChildScrollView(
                  controller: ScrollController(),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(10, 10, 10, 5),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: OutlinedButton(
                                    onPressed: () {
                                      _setState(() {
                                        btnToday = 1;
                                        btnYesterday = 0;
                                      });
                                      DateTime now = DateTime.now();
                                      var formatter = DateFormat('MM/dd/yyyy');
                                      String formattedDate = formatter.format(now);
                                      dialogStartDateController.text = formatter.format(now);
                                      dialogEndDateController.text = formatter.format(now);
                                      var outputFormatServer = DateFormat('MM-dd-yyyy');
                                      startDate = outputFormatServer.format(now);
                                      endDate = outputFormatServer.format(now);
                                      debugPrint(formattedDate);
                                    },
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(btnToday == 0 ? Colors.white : transferredIconBackground),
                                      shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(5),
                                        ),
                                      ),
                                    ),
                                    child: AdaptableText(
                                      "Today",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: 'Aller',
                                          fontWeight: FontWeight.w700,
                                          color: btnToday == 0 ? nameText : Colors.white),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  width: 15,
                                ),
                                Expanded(
                                  flex: 1,
                                  child: OutlinedButton(
                                    onPressed: () {
                                      _setState(() {
                                        btnToday = 0;
                                        btnYesterday = 1;
                                      });
                                      DateTime yesterday = DateTime.now().subtract(const Duration(days: 1));
                                      var formatter = DateFormat('MM/dd/yyyy');
                                      String formattedDate = formatter.format(yesterday);
                                      dialogStartDateController.text = formatter.format(yesterday);
                                      dialogEndDateController.text = formatter.format(yesterday);
                                      var outputFormatServer = DateFormat('MM-dd-yyyy');
                                      startDate = outputFormatServer.format(yesterday);
                                      endDate = outputFormatServer.format(yesterday);
                                      debugPrint(formattedDate);
                                    },
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(btnYesterday == 0 ? Colors.white : transferredIconBackground),
                                      shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(5),
                                        ),
                                      ),
                                    ),
                                    child: AdaptableText(
                                      "Yesterday",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: 'Aller',
                                          fontWeight: FontWeight.w700,
                                          color: btnYesterday == 0 ? nameText : Colors.white),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            const Padding(
                              padding: EdgeInsets.only(top: 10, bottom: 5),
                              child: AdaptableText(
                                'From Date',
                                style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w600, color: totalIconBackground, fontSize: 14),
                              ),
                            ),
                            TextFormField(
                                controller: dialogStartDateController,
                                maxLines: 1,
                                textInputAction: TextInputAction.done,
                                textCapitalization: TextCapitalization.sentences,
                                obscureText: false,
                                autovalidateMode: AutovalidateMode.onUserInteraction,
                                decoration: InputDecoration(
                                  fillColor: Colors.white,
                                  filled: true,
                                  border: border,
                                  isDense: true,
                                  enabledBorder: border,
                                  focusedBorder: border,
                                  errorBorder: border,
                                  focusedErrorBorder: border,
                                  contentPadding: const EdgeInsets.all(10),
                                  hintStyle: TextStyle(
                                      letterSpacing: 0,
                                      fontFamily: "Poppins",
                                      fontWeight: FontWeight.w400,
                                      color: color26323A.withOpacity(0.5),
                                      fontSize: 15),
                                  hintText: "MM/DD/YYYY",
                                  suffixIconConstraints: const BoxConstraints(maxHeight: 40, maxWidth: 40),
                                  suffixIcon: const Padding(
                                    child: Icon(Icons.calendar_today_outlined),
                                    padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                                  ),
                                ),
                                style: const TextStyle(
                                    letterSpacing: 0, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: color26323A, fontSize: 15),
                                readOnly: true,
                                enabled: true,
                                onTap: () async {
                                  debugPrint("value.currentBalance22--" +
                                      transactionsStartDate!.day.toString() +
                                      "--" +
                                      transactionsStartDate!.month.toString() +
                                      "--" +
                                      transactionsStartDate!.year.toString());
                                  DateTimeRange dateRange = DateTimeRange(
                                    start: DateTime(
                                      transactionsStartDate!.year,
                                      transactionsStartDate!.month,
                                      transactionsStartDate!.day,
                                    ),
                                    end: DateTime.now(),
                                  );
                                  DateTimeRange? picked = await showDateRangePicker(
                                    context: context,
                                    initialDateRange: dateRange,
                                    firstDate: DateTime(transactionsStartDate!.year, transactionsStartDate!.month, transactionsStartDate!.day),
                                    lastDate: DateTime.now(),
                                  );
                                  if (picked != null) {
                                    _setState(() {
                                      var inputFormat = DateFormat('yyyy-MM-dd');
                                      var inputStartDate = inputFormat.parse(picked.start.toString().split(' ')[0]);
                                      debugPrint("saved inputDate start date $inputStartDate");
                                      var outputFormatServer = DateFormat('MM-dd-yyyy');
                                      var outputFormat = DateFormat('MM/dd/yyyy');
                                      startDate = outputFormatServer.format(inputStartDate);
                                      var outputStartDate = outputFormat.format(inputStartDate);
                                      startDateDisplay = outputStartDate;
                                      debugPrint("saved outputDate start date $outputStartDate");
                                      dialogStartDateController.text = outputStartDate;
                                      var inputEndDate = inputFormat.parse(picked.end.toString().split(' ')[0]);
                                      debugPrint("saved inputDate start date $inputStartDate");
                                      endDate = outputFormatServer.format(inputEndDate);
                                      var outputEndDate = outputFormat.format(inputEndDate);
                                      endDateDisplay = outputEndDate;
                                      debugPrint("saved outputDate start date $outputEndDate");
                                      dialogEndDateController.text = outputEndDate;
                                    });
                                  }
                                }),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(10, 10, 10, 5),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Padding(
                              padding: EdgeInsets.only(top: 5, bottom: 5),
                              child: AdaptableText(
                                'To Date',
                                style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w600, color: totalIconBackground, fontSize: 14),
                              ),
                            ),
                            TextFormField(
                              controller: dialogEndDateController,
                              maxLines: 1,
                              textInputAction: TextInputAction.done,
                              textCapitalization: TextCapitalization.sentences,
                              obscureText: false,
                              autovalidateMode: AutovalidateMode.onUserInteraction,
                              decoration: InputDecoration(
                                fillColor: Colors.white,
                                filled: true,
                                border: border,
                                isDense: true,
                                enabledBorder: border,
                                focusedBorder: border,
                                errorBorder: border,
                                focusedErrorBorder: border,
                                contentPadding: const EdgeInsets.all(10),
                                hintStyle: TextStyle(
                                    letterSpacing: 0,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.w400,
                                    color: color26323A.withOpacity(0.5),
                                    fontSize: 15),
                                hintText: "MM/DD/YYYY",
                                suffixIconConstraints: const BoxConstraints(maxHeight: 40, maxWidth: 40),
                              ),
                              style: const TextStyle(
                                  letterSpacing: 0, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: color26323A, fontSize: 15),
                              readOnly: true,
                              enabled: true,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(10, 20, 10, 0),
                        child: TextButton(
                          onPressed: () {
                            _setState(() {
                              dialogStartDateController.clear();
                              dialogEndDateController.clear();
                              startDate = "";
                              endDate = "";
                              startDateDisplay = "";
                              endDateDisplay = "";
                              btnToday = 0;
                              btnYesterday = 0;
                            });
                          },
                          child: const AdaptableText(
                            clearFilter,
                            style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w400, color: transferredIconBackground, fontSize: 14),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              if (startDateDisplay != "" || endDateDisplay != "") {
                                showFilterDates = true;
                              }
                            });
                            Navigator.of(context).pop();
                            getTransactions();
                          },
                          child: Container(
                            alignment: Alignment.center,
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(50),
                                bottomLeft: Radius.circular(10),
                                topRight: Radius.circular(10),
                                bottomRight: Radius.circular(50),
                              ),
                              gradient: LinearGradient(
                                colors: <Color>[buttonGradientStart, buttonGradientEnd],
                              ),
                            ),
                            padding: const EdgeInsets.fromLTRB(15, 7, 15, 7),
                            child: const AdaptableText(
                              apply,
                              style: TextStyle(color: Colors.white, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          });
        });
  }
}
