import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/routes.dart';
import 'package:mone_me/utils/screen_config.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/widgets/adaptable_text.dart';
import 'package:mone_me/widgets/common_body.dart';
import 'package:mone_me/widgets/common_header.dart';
import 'package:mone_me/widgets/custom_toast.dart';

class ActivateVideoConsultPage extends StatefulWidget {
  const ActivateVideoConsultPage({Key? key}) : super(key: key);

  @override
  State<ActivateVideoConsultPage> createState() => _ActivateVideoConsultPageState();
}

class _ActivateVideoConsultPageState extends State<ActivateVideoConsultPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          body: Container(
            color: CupertinoColors.white,
            child: Stack(
              children: [
                SizedBox(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  child: Image.asset("assets/images/login_background_logo.png", fit: BoxFit.fill),
                ),
                Positioned(
                  child: Container(
                    height: ScreenConfig.height(context),
                    width: ScreenConfig.width(context),
                    color: Colors.transparent,
                    child: Column(
                      children: [const CommonHeader(), Expanded(child: body())],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget body() {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.all(10),
      child: Row(
        children: [
          const CommonBody(),
          Expanded(
            child: Container(
              height: ScreenConfig.height(context),
              width: ScreenConfig.width(context),
              margin: const EdgeInsets.only(left: 3),
              child: activateVideoWidget(context),
            ),
          )
        ],
      ),
    );
  }

  Widget activateVideoWidget(BuildContext context) {
    return Card(
      shadowColor: cardShadow,
      shape: const RoundedRectangleBorder(borderRadius: detailsMainBorderRadius),
      elevation: 5,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: ScreenConfig.width(context),
            padding: const EdgeInsets.fromLTRB(20, 20, 20, 15),
            color: headerColor,
            child: Center(
              child: AdaptableText(videoConsultation, style: showTextStyle, textMaxLines: 1),
            ),
          ),
          content(context),
          Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 20),
            child: TextButton(
              onPressed: () {
                Navigator.pushNamedAndRemoveUntil(context, AppRoutes.home, (routes) => false);
              },
              child: AdaptableText(home.toUpperCase(),
                  style: showTextStyle.copyWith(letterSpacing: 1, color: buttonGradientStart, fontWeight: FontWeight.w400), textMaxLines: 1),
            ),
          )
        ],
      ),
    );
  }

  Widget content(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 5, 20, 25),
        child: ListView(
          shrinkWrap: true,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
              child: AdaptableText(
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
                style: recommendedTextStyle.copyWith(letterSpacing: 0.9, color: optionsColor, fontWeight: FontWeight.w400, fontSize: 14),
                textMaxLines: 15,
                textOverflow: TextOverflow.ellipsis,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 15, 20, 10),
              child: Material(
                elevation: 5,
                borderRadius: payBorderRadius,
                child: ClipRRect(
                  borderRadius: payBorderRadius,
                  child: Container(
                    width: ScreenConfig.width(context),
                    decoration: const BoxDecoration(gradient: buttonGradient, borderRadius: payBorderRadius),
                    child: TextButton(
                      onPressed: () {
                        showToast("$videoConsultation activated successfully");
                        Navigator.pushNamedAndRemoveUntil(context, AppRoutes.home, (routes) => false);
                      },
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child: AdaptableText(activate, style: paymentButtonsTextStyle, textMaxLines: 1),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Wrap(alignment: WrapAlignment.center, children: [
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: AdaptableText(cancel, style: showTextStyle.copyWith(fontWeight: FontWeight.w400), textMaxLines: 1),
              ),
            ]),
          ],
        ),
      ),
    );
  }
}
