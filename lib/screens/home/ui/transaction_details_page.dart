import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mone_me/models/transaction_single_view_response_model.dart';
import 'package:mone_me/screens/home/notifier/transactions_notifier.dart';
import 'package:mone_me/screens/login/login_notifier/login_notifier.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/inputphone_number_formats.dart';
import 'package:mone_me/utils/routes.dart';
import 'package:mone_me/utils/screen_config.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/widgets/adaptable_text.dart';
import 'package:mone_me/widgets/common_body.dart';
import 'package:mone_me/widgets/common_header.dart';
import 'package:mone_me/widgets/dashed_line.dart';
import 'package:mone_me/widgets/flutter_ticker_widget.dart';
import 'package:mone_me/widgets/list_loading_shimmer.dart';
import 'package:provider/provider.dart';
import 'package:screenshot/screenshot.dart';

class TransactionDetailsPage extends StatefulWidget {
  final String? transactionId;

  const TransactionDetailsPage({Key? key, this.transactionId}) : super(key: key);

  @override
  State<TransactionDetailsPage> createState() => _TransactionDetailsPageState();
}

class _TransactionDetailsPageState extends State<TransactionDetailsPage> {
  String userId = "", _transactionId = "";
  double transactionFees = 0.00;
  TransactionsDetails? _transactionDetails;
  bool isLoading = true;
  ScreenshotController screenshotController = ScreenshotController();

  @override
  void initState() {
    userId = Provider.of<LoginNotifier>(context, listen: false).userId!;
    Future.delayed(Duration.zero, () {
      _transactionId = ModalRoute.of(context)!.settings.arguments! as String;
      final viewModelTransactions = Provider.of<TransactionsNotifier>(context, listen: false);
      viewModelTransactions.getSingleTransactionAPI(userId, _transactionId).then((value) {
        setState(() {
          if (viewModelTransactions.transactionDetails != null) {
            _transactionDetails = viewModelTransactions.transactionDetails;
            _perCalculation();
            isLoading = false;
          }
        });
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          body: Container(
            color: CupertinoColors.white,
            child: Stack(
              children: [
                SizedBox(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  child: Image.asset("assets/images/login_background_logo.png", fit: BoxFit.fill),
                ),
                Positioned(
                  child: Container(
                    height: ScreenConfig.height(context),
                    width: ScreenConfig.width(context),
                    color: Colors.transparent,
                    child: Column(
                      children: [
                        const CommonHeader(),
                        Expanded(
                          child: body(),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget body() {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.all(10),
      child: Row(
        children: [
          const CommonBody(),
          Expanded(
            child: Container(
              height: ScreenConfig.height(context),
              width: ScreenConfig.width(context),
              margin: const EdgeInsets.only(left: 3),
              child: transactionDetailsWidget(context),
            ),
          )
        ],
      ),
    );
  }

  Widget transactionDetailsWidget(BuildContext context) {
    return Card(
      shadowColor: cardShadow,
      shape: const RoundedRectangleBorder(borderRadius: detailsMainBorderRadius),
      elevation: 5,
      child: Column(
        children: [
          Container(
            width: ScreenConfig.width(context),
            padding: const EdgeInsets.fromLTRB(20, 20, 20, 15),
            // color: headerColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Material(
                  type: MaterialType.transparency,
                  child: IconButton(
                    icon: Image.asset('images/back.png', width: 17, height: 15),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: isLoading
                ? listLoadingShimmer()
                : Screenshot(
                    controller: screenshotController,
                    child: SingleChildScrollView(
                      controller: ScrollController(),
                      child: Container(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        margin: const EdgeInsets.only(left: 20, right: 20),
                        height: ScreenConfig.height(context),
                        child: Stack(children: [
                          Positioned(
                            top: 45,
                            left: 0,
                            right: 0,
                            child: FlutterTicketWidget(
                              width: ScreenConfig.width(context),
                              height: ScreenConfig.height(context) / 1.4,
                              isCornerRounded: true,
                              color: paymentSuccessBackground,
                              child: const SizedBox(),
                            ),
                          ),
                          Positioned(top: 0, left: 0, right: 0, child: details(context))
                        ]),
                      ),
                    ),
                  ),
          ),
          actions(context)
        ],
      ),
    );
  }

  Widget details(BuildContext context) {
    var profileDetails = Provider.of<TransactionsNotifier>(context, listen: true).transactionProfileDetails;
    var transaction = Provider.of<TransactionsNotifier>(context, listen: true).transactionDetails;
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ClipRRect(
            borderRadius: peopleProfileBorderRadius,
            child: Container(
              width: 75,
              height: 75,
              margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
              decoration: const BoxDecoration(borderRadius: peopleProfileBorderRadius, color: buttonGradientStart),
              child: profileDetails!.profilePicture! == ""
                  ? profileDetails.userName == ""
                      ? Center(
                          child: AdaptableText(appName[0].toUpperCase(), style: peopleNameListIconTextStyle),
                        )
                      : Center(
                          child: AdaptableText(profileDetails.userName![0].toUpperCase(), style: peopleNameListIconTextStyle),
                        )
                  : ClipRRect(
                      borderRadius: peopleProfileBorderRadius,
                      child: Image.network(
                        profileDetails.profilePicture!,
                        fit: BoxFit.fill,
                        errorBuilder: (ctx, _, __) {
                          return Center(
                            child: AdaptableText(profileDetails.userName![0].toUpperCase(), style: peopleNameListIconTextStyle),
                          );
                        },
                      ),
                    ),
            ),
          ),
          Center(
            child: AdaptableText(profileDetails.userName == "" ? appName.toUpperCase() : profileDetails.userName!.toUpperCase(),
                style: profileNameTextStyle, textMaxLines: 1),
          ),
          Center(
            child: AdaptableText(
                profileDetails.phoneNumber == ""
                    ? ""
                    : phoneNumberFormatByCountry(profileDetails.phoneCode! + profileDetails.phoneNumber!, profileDetails.phoneCode!),
                style: profilePhoneTextStyle,
                textMaxLines: 1),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 25, 25, 10),
              child: AdaptableText("\u0024${commaSeparatedAmount.format(double.parse(transaction!.amount!))}".toUpperCase(),
                  style: paymentSuccessAmountTextStyle, textMaxLines: 1),
            ),
          ),
          Center(
            child: transaction.message! != ""
                ? Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Colors.white),
                    padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                    child: AdaptableText(
                      transaction.message!,
                      textMaxLines: 2,
                      textOverflow: TextOverflow.ellipsis,
                      style: transactionMessageTextStyle,
                    ),
                  )
                : const SizedBox(),
          ),
          Center(
            child: transactionFees != 0
                ? Padding(
                    padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                    child: AdaptableText(
                      "$transactionFee ${commaSeparatedAmount.format(transactionFees)}",
                      textMaxLines: 2,
                      textOverflow: TextOverflow.ellipsis,
                      style: transactionMessageTextStyle.copyWith(fontSize: 15, color: nameText.withOpacity(0.6)),
                    ),
                  )
                : const SizedBox(),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Column(
                children: [
                  transaction.messageTransaction == "request"
                      ? Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Image.asset("assets/images/paid_icon.png", height: 15, width: 15),
                              Flexible(
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                                  child:
                                      AdaptableText("$requested on - ${transaction.createdOn!}", style: paymentSuccessDateTextStyle, textMaxLines: 1),
                                ),
                              )
                            ],
                          ),
                        )
                      : const SizedBox(),
                  transaction.requestMessage != ""
                      ? Container(
                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Colors.white),
                          padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                          margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                          child: AdaptableText(
                            transaction.requestMessage!,
                            textMaxLines: 2,
                            textOverflow: TextOverflow.ellipsis,
                            style: transactionMessageTextStyle,
                          ),
                        )
                      : const SizedBox(),
                  Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Image.asset("assets/images/paid_icon.png", height: 15, width: 15),
                        Flexible(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                            child: AdaptableText("$completed on - ${transaction.succeededOn!}", style: paymentSuccessDateTextStyle, textMaxLines: 1),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.fromLTRB(0, 30, 0, 10),
            child: MySeparator(color: dottedLine),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: AdaptableText(transactionDetails, style: paymentSuccessMainHeaderTextStyle, textMaxLines: 1),
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AdaptableText(transactionId, style: paymentSuccessHeadersTextStyle, textMaxLines: 1),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 5, 0, 3),
                    child: AdaptableText("${transaction.transactionUniqueId}", style: paymentSuccessDetailsTextStyle, textMaxLines: 1),
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AdaptableText(to, style: paymentSuccessHeadersTextStyle, textMaxLines: 1),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 5, 0, 2),
                    child: AdaptableText("${transaction.receiverName}", style: paymentSuccessDetailsTextStyle, textMaxLines: 1),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 2, 0, 5),
                    child: AdaptableText(
                        phoneNumberFormatByCountry(transaction.receiverPhoneCode! + transaction.receiverPhoneNumber!, transaction.receiverPhoneCode!),
                        style: paymentSuccessDetailsTextStyle,
                        textMaxLines: 1),
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AdaptableText(from, style: paymentSuccessHeadersTextStyle, textMaxLines: 1),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 5, 0, 2),
                    child: AdaptableText("${transaction.senderName}", style: paymentSuccessDetailsTextStyle, textMaxLines: 1),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 2, 0, 5),
                    child: AdaptableText(
                        phoneNumberFormatByCountry(transaction.senderPhoneCode! + transaction.senderPhoneNumber!, transaction.senderPhoneCode!),
                        style: paymentSuccessDetailsTextStyle,
                        textMaxLines: 1),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget actions(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 10, 20, 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Flexible(
            child: Material(
              elevation: 5,
              borderRadius: payBorderRadius,
              child: ClipRRect(
                borderRadius: payBorderRadius,
                child: Container(
                  decoration: const BoxDecoration(color: transferredIconBackground, borderRadius: payBorderRadius),
                  child: TextButton(
                    onPressed: () {
                      Navigator.pushNamedAndRemoveUntil(context, AppRoutes.home, (routes) => false);
                    },
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                      child: AdaptableText(home, style: paymentButtonsTextStyle, textMaxLines: 1),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Flexible(
            child: Material(
              elevation: 5,
              borderRadius: payBorderRadius,
              child: ClipRRect(
                borderRadius: payBorderRadius,
                child: Container(
                  decoration: const BoxDecoration(color: shareBackground, borderRadius: payBorderRadius),
                  child: TextButton(
                    onPressed: () {},
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                      child: AdaptableText(share, style: paymentButtonsTextStyle, textMaxLines: 1),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Flexible(
            child: Material(
              elevation: 5,
              borderRadius: payBorderRadius,
              child: ClipRRect(
                borderRadius: payBorderRadius,
                child: Container(
                  decoration: const BoxDecoration(color: receivedIconBackground, borderRadius: payBorderRadius),
                  child: TextButton(
                    onPressed: () {},
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                      child: AdaptableText(needHelp, style: paymentButtonsTextStyle, textMaxLines: 1),
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  _perCalculation() {
    setState(() {
      double val = double.parse(_transactionDetails!.amount!) * double.parse(_transactionDetails!.transactionFeePercentage!);
      transactionFees = val / 100.0;
    });
  }
}
