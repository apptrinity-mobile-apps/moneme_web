import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:mone_me/models/base_response_model.dart';
import 'package:mone_me/models/countries_response_model.dart';
import 'package:mone_me/models/languages_response_model.dart';
import 'package:mone_me/models/login_response_model.dart';
import 'package:mone_me/models/social_medialinks_response_model.dart';
import 'package:mone_me/models/user_login_model.dart';
import 'package:mone_me/models/user_sign_up_model.dart';
import 'package:mone_me/services/repositories.dart';
import 'package:mone_me/utils/session_manager.dart';
import 'package:mone_me/utils/strings.dart';

class LoginNotifier with ChangeNotifier {
  SessionManager sessionManager = SessionManager();
  bool loginStatus = false, _isFetching = false, _isHavingData = false, _showOtpPage = false;
  CountriesResponseModel? _countriesResponseModel;
  LanguagesResponseModel? _languagesResponseModel;
  BaseResponseModel? _verifyMobileResponseModel, _createUserResponseModel;
  LoginResponseModel? _loginResponseModel;
  SocialMediaLinksResponseModel? _socialmedialinksResponseModel;
  UserData? _userData;

  bool get isLogged => loginStatus;

  bool get isOtpPageShowing => _showOtpPage;

  String? get userId {
    String? id = "";
    if (_userData != null) {
      id = _userData!.id!;
    }
    return id;
  }

  CountriesResponseModel? get countriesResponseModel => _countriesResponseModel;

  LanguagesResponseModel? get languagesResponseModel => _languagesResponseModel;

  BaseResponseModel? get verifyMobileResponseModel => _verifyMobileResponseModel;

  BaseResponseModel? get createUserResponseModel => _createUserResponseModel;

  LoginResponseModel? get loginResponseModel => _loginResponseModel;

  SocialMediaLinksResponseModel? get socilamedialinksResponseModel => _socialmedialinksResponseModel;

  List<SocialMediasList>? get getSocialMediasList {
    List<SocialMediasList> list = [];
    if (socilamedialinksResponseModel != null) {
      if (socilamedialinksResponseModel!.responseStatus == 1) {
        if (socilamedialinksResponseModel!.socialMediasList!.isNotEmpty) {
          list = socilamedialinksResponseModel!.socialMediasList!;
        }
      }
    }
    return list;
  }

  String? otp() {
    return verifyMobileResponseModel!.verificationOtp!;
  }

  void showOtpPage(bool show) {
    _showOtpPage = show;
    notifyListeners();
  }

  void isUserLogin(bool login) {
    sessionManager.userLogin(login);
  }

  void saveUserDetails() {
    if (_loginResponseModel != null) {
      if (_loginResponseModel!.responseStatus ==1){
        sessionManager.saveLoginDetails(jsonEncode(_loginResponseModel!.userData!));
      }
    }
  }

  Future<void> getUserDetails() async {
    await sessionManager.isUserLoggedIn().then((value) {
      if (value == null) {
        loginStatus = false;
      } else {
        loginStatus = value;
      }
    });
    await sessionManager.getLoginDetails().then((value) {
      if (value != null) {
        _userData = UserData.fromJson(jsonDecode(value));
      }
    });
  }

  List<CountriesList> getCountriesList() {
    List<CountriesList> list = [];
    if (countriesResponseModel!.responseStatus == 1) {
      list = countriesResponseModel!.countriesList!;
    }
    return list;
  }

  List<LanguagesList> getLanguagesList() {
    List<LanguagesList> list = [];
    if (languagesResponseModel!.responseStatus == 1) {
      list = languagesResponseModel!.languagesList!;
    }
    return list;
  }

  Future<CountriesResponseModel?> getAllCountriesAPI() async {
    _isFetching = true;
    _isHavingData = false;
    _countriesResponseModel = CountriesResponseModel();
    try {
      dynamic response = await Repository().getAllCountries();
      if (response != null) {
        _countriesResponseModel = CountriesResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("getAllCountriesAPI $_isFetching $_isHavingData $_countriesResponseModel");
    notifyListeners();
    return _countriesResponseModel;
  }

  Future<LanguagesResponseModel?> getAllLanguagesAPI() async {
    _isFetching = true;
    _isHavingData = false;
    _languagesResponseModel = LanguagesResponseModel();
    try {
      dynamic response = await Repository().getAllLanguages();
      if (response != null) {
        _languagesResponseModel = LanguagesResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("getAllLanguagesAPI $_isFetching $_isHavingData $_languagesResponseModel");
    notifyListeners();
    return _languagesResponseModel;
  }

  Future<BaseResponseModel?> verifyMobileNumberAPI(String phoneNumber, String email) async {
    _isFetching = true;
    _isHavingData = false;
    _verifyMobileResponseModel = BaseResponseModel();
    try {
      dynamic response = await Repository().verifyMobileNumber(phoneNumber, email);
      if (response != null) {
        _verifyMobileResponseModel = BaseResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("verifyMobileNumberAPI $_isFetching $_isHavingData $_verifyMobileResponseModel");
    notifyListeners();
    return _verifyMobileResponseModel;
  }

  Future<BaseResponseModel?> createUserAPI(UserSignUpModel signUpModel) async {
    _isFetching = true;
    _isHavingData = false;
    _createUserResponseModel = BaseResponseModel();
    try {
      dynamic response = await Repository().createUser(signUpModel);
      if (response != null) {
        _createUserResponseModel = BaseResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("createUserAPI $_isFetching $_isHavingData $_createUserResponseModel");
    notifyListeners();
    return _createUserResponseModel;
  }

  Future<LoginResponseModel?> loginAPI(UserLoginModel loginModel) async {
    _isFetching = true;
    _isHavingData = false;
    _loginResponseModel = LoginResponseModel();
    try {
      dynamic response = await Repository().login(loginModel);
      if (response != null) {
        _loginResponseModel = LoginResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("loginAPI $_isFetching $_isHavingData $_loginResponseModel");
    notifyListeners();
    return _loginResponseModel;
  }

  BaseResponseModel? _logoutResponseModel;

  BaseResponseModel? get logoutResponseModel => _logoutResponseModel;

  Future<BaseResponseModel?> logoutAPI(String userId) async {
    _isFetching = true;
    _isHavingData = false;
    _logoutResponseModel = BaseResponseModel();
    try {
      dynamic response = await Repository().logout(userId);
      if (response != null) {
        _logoutResponseModel = BaseResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("loginAPI $_isFetching $_isHavingData $_logoutResponseModel");
    notifyListeners();
    return _logoutResponseModel;
  }

  Future<SocialMediaLinksResponseModel?> getSocialMediaLinksAPI() async {
    _isFetching = true;
    _isHavingData = false;
    _socialmedialinksResponseModel = SocialMediaLinksResponseModel();
    try {
      dynamic response = await Repository().getSocialMediaLinks();
      if (response != null) {
        _socialmedialinksResponseModel = SocialMediaLinksResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e, st) {
      _isHavingData = false;
      debugPrint(e.toString());
      debugPrint("err--- " + st.toString());
    }
    _isFetching = false;
    debugPrint("getSocialMediaLinksAPI $_isFetching $_isHavingData $_socialmedialinksResponseModel");
    notifyListeners();
    return _socialmedialinksResponseModel;
  }

  BaseResponseModel? _passwordResetResponse;

  BaseResponseModel? get passwordResetResponse => _passwordResetResponse;

  Future<BaseResponseModel?> passwordResetAPI(String phoneCode, String phoneNumber, String email) async {
    _isFetching = true;
    _isHavingData = false;
    _passwordResetResponse = BaseResponseModel();
    try {
      dynamic response = await Repository().passwordReset(phoneCode, phoneNumber, email);
      if (response != null) {
        _passwordResetResponse = BaseResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e, st) {
      _isHavingData = false;
      debugPrint(e.toString());
      debugPrint("err--- " + st.toString());
    }
    _isFetching = false;
    debugPrint("passwordResetAPI $_isFetching $_isHavingData $_passwordResetResponse!");
    notifyListeners();
    return _passwordResetResponse;
  }

  BaseResponseModel? _passwordForgotResponse;

  BaseResponseModel? get passwordForgotResponse => _passwordForgotResponse;

  Future<BaseResponseModel?> forgotPasswordResetAPI(
      String phoneCode, String phoneNumber, String email, String forgotPasswordOTP, String newPassword, String confirmNewPassword) async {
    _isFetching = true;
    _isHavingData = false;
    _passwordForgotResponse = BaseResponseModel();
    try {
      dynamic response = await Repository().forgotPasswordReset(phoneCode, phoneNumber, email, forgotPasswordOTP, newPassword, confirmNewPassword);
      if (response != null) {
        _passwordForgotResponse = BaseResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e, st) {
      _isHavingData = false;
      debugPrint(e.toString());
      debugPrint("err--- " + st.toString());
    }
    _isFetching = false;
    debugPrint("forgotPasswordResetAPI $_isFetching $_isHavingData $_passwordForgotResponse!");
    notifyListeners();
    return _passwordForgotResponse;
  }
}
