import 'dart:convert';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:mone_me/models/countries_response_model.dart';
import 'package:mone_me/models/social_medialinks_response_model.dart';
import 'package:mone_me/models/user_login_model.dart';
import 'package:mone_me/screens/login/login_notifier/login_notifier.dart';
import 'package:mone_me/utils/routes.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/inputphone_number_formats.dart';
import 'package:mone_me/utils/screen_config.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/utils/validators.dart';
import 'package:mone_me/widgets/adaptable_text.dart';
import 'package:mone_me/widgets/country_picker_dropdown.dart';
import 'package:mone_me/widgets/custom_toast.dart';
import 'package:mone_me/widgets/loader.dart';
import 'package:mone_me/widgets/phone_dropdown.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  late LoginNotifier viewModel;
  final _formKey = GlobalKey<FormState>();
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool isShowPassword = false;
  String initialCountry = "IN", phoneCode = "", input_phonenumber_hint = "";
  var platformName = '';
  List<SocialMediasList> socialMediaLinksList = [];

  @override
  void initState() {
    super.initState();
    viewModel = Provider.of<LoginNotifier>(context, listen: false);
    phoneCode = '+1';
    input_phonenumber_hint = inputPhoneNumberFormatByCountry('9999999999', phoneCode);
    if (kIsWeb) {
      platformName = "Web";
    } else {
      if (Platform.isAndroid) {
        platformName = "Android";
      } else if (Platform.isIOS) {
        platformName = "IOS";
      } else if (Platform.isFuchsia) {
        platformName = "Fuchsia";
      } else if (Platform.isLinux) {
        platformName = "Linux";
      } else if (Platform.isMacOS) {
        platformName = "MacOS";
      } else if (Platform.isWindows) {
        platformName = "Windows";
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final platform = Theme.of(context).platform;
    final vModel = Provider.of<LoginNotifier>(context, listen: true);
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          backgroundColor: CupertinoColors.white,
          body: (platform.name == "iOS" || platformName == "IOS") || (platform.name == "android" && platformName == "Web")
              ? Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  decoration: const BoxDecoration(
                    image: DecorationImage(image: AssetImage("assets/images/login_background_logo.png"), fit: BoxFit.fill),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(
                        child: Image.asset("assets/images/app_logo_vertical.png",
                            height: ScreenConfig.height(context) / 2, width: ScreenConfig.width(context) / 2),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                        child: AdaptableText("Mobile Apps", style: signInContinueTextStyle, textMaxLines: 1),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                        child: AdaptableText("Coming Soon", style: signInContinueTextStyle, textMaxLines: 1),
                      )
                    ],
                  ),
                )
              : Row(
                  children: [
                    Expanded(
                      child: Container(
                        height: ScreenConfig.height(context),
                        width: ScreenConfig.width(context),
                        decoration: const BoxDecoration(
                          image: DecorationImage(image: AssetImage("assets/images/login_background_logo.png"), fit: BoxFit.fill),
                        ),
                        child: Center(
                          child: SingleChildScrollView(
                            controller: ScrollController(),
                            child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: [
                              Center(
                                child: Image.asset("assets/images/app_logo_vertical.png",
                                    height: ScreenConfig.height(context) / 2, width: ScreenConfig.width(context) / 2),
                              ),
                              const SizedBox(
                                height: 200,
                              ),
                              SizedBox(
                                height: 100,
                                child: ListView.builder(
                                    controller: ScrollController(),
                                    scrollDirection: Axis.horizontal,
                                    shrinkWrap: true,
                                    itemCount: vModel.getSocialMediasList!.length,
                                    itemBuilder: (context, index) {
                                      return Container(
                                        margin: const EdgeInsets.only(right: 10),
                                        child: InkWell(
                                          onTap: () {
                                            final Uri uri = Uri.parse(vModel.getSocialMediasList![index].urlLink!);
                                            launchUrl(uri);
                                          },
                                          child: Image.network(
                                            vModel.getSocialMediasList![index].image!,
                                            width: 40,
                                            height: 40,
                                          ),
                                        ),
                                      );
                                    }),
                              ),
                            ]),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        height: ScreenConfig.height(context),
                        width: ScreenConfig.width(context),
                        decoration: const BoxDecoration(
                          image: DecorationImage(image: AssetImage("assets/images/login_background_details.png"), fit: BoxFit.fill),
                        ),
                        child: Center(
                          child: SizedBox(
                            width: ScreenConfig.width(context) / 3,
                            child: SingleChildScrollView(
                              controller: ScrollController(),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [welcomeHeaders(), loginFields(), submitOrSignUp()],
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
        ),
      ),
    );
  }

  Widget welcomeHeaders() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
            child: AdaptableText(welcome, style: welcomeTextStyle, textMaxLines: 1),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 5),
            child: AdaptableText(signInToContinue, style: signInContinueTextStyle, textMaxLines: 1),
          ),
        ],
      ),
    );
  }

  Widget loginFields() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
      child: Column(
        children: [
          Form(
            key: _formKey,
            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: TextFormField(
                      autofocus: true,
                      controller: phoneController,
                      textInputAction: TextInputAction.next,
                      maxLines: 1,
                      keyboardType: TextInputType.number,
                      onChanged: (_) {
                        phoneController.text = inputPhoneNumberFormatByCountry(phoneController.text, phoneCode);
                        phoneController.value = TextEditingValue(
                          text: phoneController.text.toString(),
                          selection: TextSelection.collapsed(offset: phoneController.text.length),
                        );
                      },
                      inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))],
                      decoration: InputDecoration(
                        fillColor: CupertinoColors.white,
                        filled: true,
                        border: border,
                        isDense: true,
                        enabledBorder: border,
                        focusedBorder: border,
                        errorBorder: errorBorder,
                        focusedErrorBorder: errorBorder,
                        contentPadding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                        hintStyle: textFormFieldHintStyle,
                        hintText: input_phonenumber_hint,
                        prefixIcon: Container(
                          width: 90,
                          alignment: Alignment.center,
                          margin: const EdgeInsets.fromLTRB(15, 0, 0, 0),
                          child: IntrinsicHeight(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Flexible(
                                  child: Provider.of<LoginNotifier>(context, listen: true).getCountriesList().isNotEmpty
                                      ? countriesDropDown()
                                      : const SizedBox(),
                                ),
                                // const VerticalDivider(thickness: 1.5, color: spinnerDropDown)
                              ],
                            ),
                          ),
                        ),
                      ),
                      style: textFormFieldStyle,
                      validator: phoneValidator,
                      autovalidateMode: AutovalidateMode.onUserInteraction),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: TextFormField(
                      maxLines: 1,
                      controller: passwordController,
                      textInputAction: TextInputAction.next,
                      obscureText: isShowPassword ? false : true,
                      obscuringCharacter: '*',
                      decoration: InputDecoration(
                        fillColor: CupertinoColors.white,
                        filled: true,
                        border: border,
                        isDense: true,
                        enabledBorder: border,
                        focusedBorder: border,
                        errorBorder: errorBorder,
                        focusedErrorBorder: errorBorder,
                        contentPadding: textFormFieldPadding,
                        hintStyle: textFormFieldHintStyle,
                        hintText: password,
                        prefixIcon: Container(
                          width: 90,
                          alignment: Alignment.center,
                          margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                          child: const Icon(Icons.lock_outline, size: 28, color: defaultText),
                        ),
                        suffixIcon: Container(
                          margin: const EdgeInsets.fromLTRB(10, 0, 20, 0),
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                isShowPassword = !isShowPassword;
                              });
                            },
                            child: isShowPassword
                                ? const Icon(Icons.visibility_off_outlined, size: 25)
                                : const Icon(Icons.visibility_outlined, size: 25),
                          ),
                        ),
                      ),
                      style: textFormFieldStyle,
                      validator: emptyTextValidator,
                      autovalidateMode: AutovalidateMode.onUserInteraction),
                )
              ],
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: TextButton(
              onPressed: () {
                Navigator.pushNamed(context, AppRoutes.forgotPassword);
              },
              child: AdaptableText(forgotPassword, style: forgotPasswordTextStyle, textMaxLines: 1),
            ),
          )
        ],
      ),
    );
  }

  Widget submitOrSignUp() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
      child: Column(
        children: [
          Material(
            elevation: 5,
            borderRadius: payBorderRadius,
            child: ClipRRect(
              borderRadius: payBorderRadius,
              child: Container(
                width: ScreenConfig.width(context),
                decoration: const BoxDecoration(gradient: buttonGradient),
                child: TextButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      showLoadingDialog(context);
                      var loginModel =
                          UserLoginModel(phoneNumber: phoneController.text.trim(), password: passwordController.text.trim(), phoneCode: phoneCode);
                      viewModel.loginAPI(loginModel).then((value) {
                        Navigator.pop(context);
                        if (viewModel.loginResponseModel != null) {
                          if (viewModel.loginResponseModel!.responseStatus == 1) {
                            showToast(viewModel.loginResponseModel!.result!);
                            viewModel.saveUserDetails();
                            viewModel.isUserLogin(true);
                            viewModel.getUserDetails();
                            Navigator.pushNamedAndRemoveUntil(context, AppRoutes.home, (routes) => false);
                          } else {
                            showToast(viewModel.loginResponseModel!.result!);
                          }
                        } else {
                          showToast(tryAgain);
                        }
                      });
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                    child: AdaptableText(next, style: submitButtonTextStyle, textMaxLines: 1),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
            child: RichText(
              text: TextSpan(
                style: signInContinueTextStyle,
                children: <TextSpan>[
                  TextSpan(text: newUser, style: signInContinueTextStyle),
                  TextSpan(text: ' ', style: signInContinueTextStyle),
                  TextSpan(
                      text: signUp,
                      style: signUpContinueTextStyle,
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          Navigator.pushNamed(context, AppRoutes.signUp);
                        }),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget countriesDropDown() {
    return CountryPickerDropdown(
      isDense: true,
      isExpanded: true,
      itemBuilder: buildDropdownItem,
      onValuePicked: (CountriesList country) {
        setState(() {
          phoneCode = "";
          phoneCode = country.phoneCode!;
          input_phonenumber_hint = inputPhoneNumberFormatByCountry('9999999999', phoneCode);
          phoneController.text = inputPhoneNumberFormatByCountry(phoneController.text, phoneCode);
        });
      },
      countryList: Provider.of<LoginNotifier>(context, listen: true).getCountriesList(),
    );
  }
}
