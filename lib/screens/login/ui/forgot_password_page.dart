import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:mone_me/models/countries_response_model.dart';
import 'package:mone_me/screens/login/login_notifier/login_notifier.dart';
import 'package:mone_me/utils/routes.dart';
import 'package:mone_me/utils/colors.dart';
import 'package:mone_me/utils/constants.dart';
import 'package:mone_me/utils/inputphone_number_formats.dart';
import 'package:mone_me/utils/screen_config.dart';
import 'package:mone_me/utils/strings.dart';
import 'package:mone_me/utils/validators.dart';
import 'package:mone_me/widgets/adaptable_text.dart';
import 'package:mone_me/widgets/country_picker_dropdown.dart';
import 'package:mone_me/widgets/custom_toast.dart';
import 'package:mone_me/widgets/loader.dart';
import 'package:mone_me/widgets/phone_dropdown.dart';
import 'package:provider/provider.dart';
import 'package:pinput/pinput.dart';
import 'package:url_launcher/url_launcher.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({Key? key}) : super(key: key);

  @override
  State<ForgotPasswordPage> createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  late LoginNotifier viewModel;
  final _formKey = GlobalKey<FormState>();
  final _phoneFormKey = GlobalKey<FormFieldState>();
  final _emailFormKey = GlobalKey<FormFieldState>();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController otpController = TextEditingController();
  bool isShowPassword = false, isShowNewPassword = false, showOtpFields = false, otpSent = false;
  final _pinPutFocusNode = FocusNode();
  final defaultPinTheme = PinTheme(
    width: 56,
    height: 60,
    margin: const EdgeInsets.fromLTRB(5, 0, 5, 0),
    textStyle: pinPutTextStyle,
    decoration: BoxDecoration(color: spinnerDropDown.withOpacity(0.5), borderRadius: payBorderRadius),
  );
  String phoneCode = "", input_phonenumber_hint = "";

  @override
  void initState() {
    phoneCode = '+1';
    input_phonenumber_hint = inputPhoneNumberFormatByCountry('9999999999', phoneCode);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    viewModel = Provider.of<LoginNotifier>(context, listen: true);
    return SafeArea(
      child: WillPopScope(
        onWillPop: onBackPressed,
        child: Scaffold(
          backgroundColor: CupertinoColors.white,
          body: Row(
            children: [
              Expanded(
                child: Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  decoration: const BoxDecoration(
                    image: DecorationImage(image: AssetImage("assets/images/login_background_logo.png"), fit: BoxFit.fill),
                  ),
                  child: Center(
                    child: SingleChildScrollView(
                      controller: ScrollController(),
                      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                        Center(
                          child: Image.asset("assets/images/app_logo_vertical.png",
                              height: ScreenConfig.height(context) / 2, width: ScreenConfig.width(context) / 2),
                        ),
                        const SizedBox(
                          height: 200,
                        ),
                        SizedBox(
                          height: 100,
                          child: ListView.builder(
                              controller: ScrollController(),
                              scrollDirection: Axis.horizontal,
                              shrinkWrap: true,
                              itemCount: viewModel.getSocialMediasList!.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  margin: const EdgeInsets.only(right: 10),
                                  child: InkWell(
                                    onTap: () {
                                      final Uri uri = Uri.parse(viewModel.getSocialMediasList![index].urlLink!);
                                      launchUrl(uri);
                                    },
                                    child: Image.network(
                                      viewModel.getSocialMediasList![index].image!,
                                      width: 40,
                                      height: 40,
                                    ),
                                  ),
                                );
                              }),
                        ),
                      ]),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/images/login_background_details.png"),
                      fit: BoxFit.fill,
                    ),
                  ),
                  child: Center(
                    child: SingleChildScrollView(
                      controller: ScrollController(),
                      child: SizedBox(
                        width: ScreenConfig.width(context) / 3,
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [welcomeHeaders(), inputFields(), backLogin()],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget welcomeHeaders() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
            child: AdaptableText(forgotPassword, style: welcomeTextStyle, textMaxLines: 1),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 5),
            child: AdaptableText(enterMobileNumberOrEmail, style: signInContinueTextStyle, textMaxLines: 1),
          ),
        ],
      ),
    );
  }

  Widget inputFields() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
      child: Column(
        children: [
          Column(
            children: [
              phoneNumberField(),
              AdaptableText(or.toUpperCase(), style: textFormFieldStyle),
              emailField(),
              Visibility(
                visible: otpSent,
                child: Form(
                  key: _formKey,
                  child: Column(children: [passwordFields(), otpField()]),
                ),
              ),
              submitOrResend()
            ],
          ),
        ],
      ),
    );
  }

  Widget phoneNumberField() {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: TextFormField(
          key: _phoneFormKey,
          autofocus: true,
          controller: phoneController,
          textInputAction: TextInputAction.next,
          maxLines: 1,
          keyboardType: TextInputType.number,
          onChanged: (number) {
            phoneController.text = inputPhoneNumberFormatByCountry(phoneController.text, phoneCode);
            phoneController.value = TextEditingValue(
              text: phoneController.text.toString(),
              selection: TextSelection.collapsed(offset: phoneController.text.length),
            );
          },
          onTap: () {
            _emailFormKey.currentState!.reset();
          },
          decoration: InputDecoration(
            fillColor: CupertinoColors.white,
            filled: true,
            border: border,
            isDense: true,
            enabledBorder: border,
            focusedBorder: border,
            errorBorder: errorBorder,
            focusedErrorBorder: errorBorder,
            contentPadding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
            hintStyle: textFormFieldHintStyle,
            hintText: input_phonenumber_hint,
            prefixIcon: Container(
              width: 90,
              alignment: Alignment.center,
              margin: const EdgeInsets.fromLTRB(15, 0, 0, 0),
              child: IntrinsicHeight(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Provider.of<LoginNotifier>(context, listen: true).getCountriesList().isNotEmpty ? countriesDropDown() : const SizedBox(),
                    ),
                    // const VerticalDivider(thickness: 1.5, color: spinnerDropDown)
                  ],
                ),
              ),
            ),
          ),
          style: textFormFieldStyle,
          validator: phoneValidator,
          autovalidateMode: AutovalidateMode.onUserInteraction),
    );
  }

  Widget emailField() {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: TextFormField(
          key: _emailFormKey,
          controller: emailController,
          textInputAction: TextInputAction.next,
          maxLines: 1,
          keyboardType: TextInputType.emailAddress,
          decoration: emailDecoration,
          style: textFormFieldStyle,
          validator: emailValidator,
          onTap: () {
            _phoneFormKey.currentState!.reset();
          },
          autovalidateMode: AutovalidateMode.onUserInteraction),
    );
  }

  Widget passwordFields() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: TextFormField(
              maxLines: 1,
              controller: passwordController,
              textInputAction: TextInputAction.next,
              obscureText: isShowPassword ? false : true,
              obscuringCharacter: '*',
              decoration: InputDecoration(
                fillColor: CupertinoColors.white,
                filled: true,
                border: border,
                isDense: true,
                enabledBorder: border,
                focusedBorder: border,
                errorBorder: errorBorder,
                focusedErrorBorder: errorBorder,
                contentPadding: textFormFieldPadding,
                hintStyle: textFormFieldHintStyle,
                hintText: newPassword,
                prefixIcon: Container(
                  width: 90,
                  alignment: Alignment.center,
                  margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                  child: const Icon(Icons.lock_outline, size: 28, color: defaultText),
                ),
                suffixIcon: Container(
                  margin: const EdgeInsets.fromLTRB(10, 0, 20, 0),
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        isShowPassword = !isShowPassword;
                      });
                    },
                    child: isShowPassword ? const Icon(Icons.visibility_off_outlined, size: 25) : const Icon(Icons.visibility_outlined, size: 25),
                  ),
                ),
              ),
              style: textFormFieldStyle,
              validator: passwordValidator,
              autovalidateMode: AutovalidateMode.onUserInteraction),
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: TextFormField(
              maxLines: 1,
              controller: newPasswordController,
              textInputAction: TextInputAction.next,
              obscureText: isShowNewPassword ? false : true,
              obscuringCharacter: '*',
              decoration: InputDecoration(
                fillColor: CupertinoColors.white,
                filled: true,
                border: border,
                isDense: true,
                enabledBorder: border,
                focusedBorder: border,
                errorBorder: errorBorder,
                focusedErrorBorder: errorBorder,
                contentPadding: textFormFieldPadding,
                hintStyle: textFormFieldHintStyle,
                hintText: confirmPassword,
                prefixIcon: Container(
                  width: 90,
                  alignment: Alignment.center,
                  margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                  child: const Icon(Icons.lock_outline, size: 28, color: defaultText),
                ),
                suffixIcon: Container(
                  margin: const EdgeInsets.fromLTRB(10, 0, 20, 0),
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        isShowNewPassword = !isShowNewPassword;
                      });
                    },
                    child: isShowNewPassword ? const Icon(Icons.visibility_off_outlined, size: 25) : const Icon(Icons.visibility_outlined, size: 25),
                  ),
                ),
              ),
              style: textFormFieldStyle,
              validator: passwordValidator,
              autovalidateMode: AutovalidateMode.onUserInteraction),
        ),
      ],
    );
  }

  Widget otpField() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Column(
        children: [
          Align(
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
              child: AdaptableText(enterOtp, style: signInContinueTextStyle, textMaxLines: 1),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Pinput(
              animationCurve: Curves.linear,
              animationDuration: const Duration(milliseconds: 160),
              pinAnimationType: PinAnimationType.slide,
              separator: const SizedBox(width: 1),
              showCursor: false,
              length: 6,
              closeKeyboardWhenCompleted: true,
              focusNode: _pinPutFocusNode,
              controller: otpController,
              defaultPinTheme: defaultPinTheme,
              focusedPinTheme: defaultPinTheme.copyWith(height: 68, width: 64),
              errorPinTheme: defaultPinTheme.copyWith(
                decoration: BoxDecoration(
                  color: spinnerDropDown.withOpacity(0.5),
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.allow(RegExp(r'[0-9]')), LengthLimitingTextInputFormatter(6)],
              validator: pinPutValidator,
              pinputAutovalidateMode: PinputAutovalidateMode.onSubmit,
              obscuringCharacter: '*',
              obscureText: true,
              onChanged: (value) {},
            ),
          ),
        ],
      ),
    );
  }

  Widget submitOrResend() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 20, 0, 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          otpSent
              ? Padding(
                  padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                  child: TextButton(
                    onPressed: () {
                      sendOtp();
                    },
                    child: AdaptableText(resendCode, style: resendTextStyle, textMaxLines: 1),
                  ),
                )
              : const SizedBox(),
          Material(
            elevation: 5,
            borderRadius: payBorderRadius,
            child: ClipRRect(
              borderRadius: payBorderRadius,
              child: Container(
                width: ScreenConfig.width(context),
                decoration: const BoxDecoration(gradient: buttonGradient),
                child: TextButton(
                  onPressed: () async {
                    if (otpSent) {
                      resetPassword();
                    } else {
                      sendOtp();
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                    child: AdaptableText(submit, style: submitButtonTextStyle, textMaxLines: 1),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget backLogin() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        child: AdaptableText(backToLogin, style: forgotPasswordTextStyle, textMaxLines: 1),
      ),
    );
  }

  Widget countriesDropDown() {
    return CountryPickerDropdown(
      isDense: true,
      isExpanded: true,
      itemBuilder: buildDropdownItem,
      onValuePicked: (CountriesList country) {
        setState(() {
          phoneCode = "";
          phoneCode = country.phoneCode!;
          input_phonenumber_hint = inputPhoneNumberFormatByCountry('9999999999', phoneCode);
          phoneController.text = inputPhoneNumberFormatByCountry(phoneController.text, phoneCode);
        });
      },
      countryList: Provider.of<LoginNotifier>(context, listen: true).getCountriesList(),
    );
  }

  Future<bool> onBackPressed() async {
    Navigator.of(context).pop();
    return true;
  }

  sendOtp() async {
    if (phoneController.text.trim().toString().isNotEmpty || emailController.text.trim().toString().isNotEmpty) {
      setState(() {
        otpSent = false;
        passwordController.clear();
        newPasswordController.clear();
      });
      showLoadingDialog(context);
      await viewModel.passwordResetAPI(phoneCode, phoneController.text.trim().toString(), emailController.text.trim().toString()).then((value) {
        Navigator.of(context).pop();
        if (viewModel.passwordResetResponse != null) {
          if (viewModel.passwordResetResponse!.responseStatus == 1) {
            showToast(viewModel.passwordResetResponse!.result!);
            setState(() {
              otpSent = true;
            });
          } else {
            showToast(viewModel.passwordResetResponse!.result!);
          }
        } else {
          showToast(tryAgain);
        }
      });
    } else {
      showToast(enterMobileNumberOrEmail);
    }
  }

  resetPassword() async {
    if (_formKey.currentState!.validate()) {
      if (passwordController.text.trim().toString() == newPasswordController.text.trim().toString()) {
        showLoadingDialog(context);
        await viewModel
            .forgotPasswordResetAPI(phoneCode, phoneController.text.trim().toString(), emailController.text.trim().toString(),
                otpController.text.trim().toString(), passwordController.text.trim().toString(), newPasswordController.text.trim().toString())
            .then((value) {
          Navigator.of(context).pop();
          if (viewModel.passwordForgotResponse != null) {
            if (viewModel.passwordForgotResponse!.responseStatus == 1) {
              showToast(viewModel.passwordForgotResponse!.result!);
              Navigator.pushNamedAndRemoveUntil(context, AppRoutes.login, (route) => false);
            } else {
              showToast(viewModel.passwordForgotResponse!.result!);
            }
          } else {
            showToast(tryAgain);
          }
        });
      } else {
        showToast(passwordsNotMatch);
      }
    }
  }
}
